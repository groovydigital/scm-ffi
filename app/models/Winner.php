<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Winner extends Eloquent implements UserInterface, RemindableInterface {


    use UserTrait, RemindableTrait;

    protected $table = 'ffi_winners';
    protected $primaryKey = 'winner_id';
    protected $fillable = [];

    public function scopesaveData($query, $data = array())
    {
        try {
            DB::beginTransaction();
            foreach($data as $key => $val):
                $this->$key = $val;
            endforeach;
            $this->is_deleted  = 0;
            $this->created_at  = date('Y-m-d H:i:s');
            $this->updated_at  = date('Y-m-d H:i:s');
            $this->save();
            DB::commit();
            return $this->id;
        }
        catch (\Exception $e) {
            DB::rollback();
            return FALSE;
        }

    }

    public function scopeupdateData($query, $id = null, $params = array())
    {
        try {
            DB::beginTransaction();
            $data                  	= $this->find($id);
            foreach($params as $key => $val):
                $data->$key = $val;
            endforeach;
            $data->updated_at  		= date('Y-m-d H:i:s');
            $data->save();
            DB::commit();
            return TRUE;
        }
        catch (\Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

    public function scopeupdateDataWhere($query, $params = array(), $where = array())
    {
        $data = $this->where(array($params));

        try {
            DB::beginTransaction();
            foreach($params as $key => $val):
                $data->$key = $val;
            endforeach;
            $data->updated_at  = date('Y-m-d H:i:s');
            $data->save();
            DB::commit();
            return TRUE;
        }
        catch (\Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

    public function scopedeleteData($query, $id = null)
    {
        try {
            DB::beginTransaction();
            $data             = $this->find($id);
            $data->is_deleted = 1;
            $data->updated_at = date('Y-m-d H:i:s');
            $data->save();
            DB::commit();
            return TRUE;
        }
        catch (\Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

    public function scopedeleteDataWhere($query, $params = array(), $where = array())
    {
        $data = $this->where(array($params));

        try {
            DB::beginTransaction();
            $data->is_deleted = 1;
            $data->updated_at = date('Y-m-d H:i:s');
            $data->save();
            DB::commit();
            return TRUE;
        }
        catch (\Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

    public function scopedropData($query, $id = null)
    {
        try {
            DB::beginTransaction();
            $data = $this->find($id);
            $data->delete();
            DB::commit();
            return TRUE;
        }
        catch (\Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

    public function scopedropDataWhere($query, $params = array(), $where = array())
    {
        $data = $this->where(array($params));

        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            return TRUE;
        }
        catch (\Exception $e) {
            DB::rollback();
            return FALSE;
        }
    }

}

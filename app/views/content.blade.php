<?php
$carbon = new \Carbon\Carbon();
?>

@foreach($data as $key => $val)
    <a class="link-post" href="{{route('detail.post',array($val->id_post))}}">
    @if($val->type_post=="photo")

            <div class="post-photo">
                <img src="/frontend/img/icon-photo.png" class="icon-photo">
                <img src="/images/upload/post/mobile/{{$val->post_image}}" class="post-photo-thumbnail">

                <div class="post-photo-desc">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">
                            <img src="{{$val->image}}" class="img-circle" style="max-width:40px; max-height: 40px; width: 40px; height:40px;">
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <p><strong>{{$val->username}}:</strong> {{html_entity_decode($val->caption)}} <br>
                                <i>{{$carbon::createFromFormat('Y-m-d H:i:s', $val->date)->subMinutes(2)->diffForHumans();}}</i></p>
                        </div>
                    </div>
                </div>
            </div>

    @endif

    @if($val->type_post=="text")
        <div class="post-status">
            <div class="post-status-desc">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">
                        <img src="/frontend/img/icon-status.png" class="icon-status">
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <p><strong>{{$val->username}}:</strong> {{html_entity_decode($val->caption)}}  <br>
                            <i>{{$carbon::createFromFormat('Y-m-d H:i:s', $val->date)->subMinutes(2)->diffForHumans();}}</i></p>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($val->type_post=="location")
        <?php $location=explode(",",$val->location)?>
        <div class="post-location">
            <div class="post-location-desc">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">
                        &nbsp;&nbsp;&nbsp;<img src="/frontend/img/icon-location.png" class="icon-location">
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <p><strong>{{$val->username}}:</strong> {{html_entity_decode($val->caption)}}</p>
                        <br>
                        <p>in {{$location[0].','.$location[1],$location[2]}}<br>
                            <i>{{$carbon::createFromFormat('Y-m-d H:i:s', $val->date)->subMinutes(2)->diffForHumans();}}</i></p>
                    </div>
                </div>
            </div>
        </div>
    @endif
        </a>
@endforeach
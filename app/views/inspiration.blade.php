@include('head-meta')
<body>
@include('header')

<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="inspiration-headline-title">
                    <h4 class="text-blue">Inspirasi Sarapan Sempurna</h4>
                </div>
            </div>
            <div class="col-lg-6 text-right">
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div class="row">
            <?php
            $carbon = new \Carbon\Carbon();
            ?>

            @foreach($data as $key => $val)
            <div class="col-lg-4">
                <div class="inspiration">
                    <div class="inspiration-thumbnail">
                        <img src="/images/upload/blog/mobile/{{$val->image}}">
                        <div class="caption">
                            <h4 class="text-blue"><a href="{{route("inspiration.detail",$val->slug)}}"> {{$val->title}}</a></h4>
                            <p>{{$val->header}}</p>
                            <div class="caption-footer">
                                <div style="float:left;"><i class="date">{{$carbon::createFromFormat('Y-m-d H:i:s', $val->created_at)->subMinutes(2)->diffForHumans();}}</i></div>
                                <div style="float:right;"><a href="{{route("inspiration.detail",$val->slug)}}" class="read-more">Read More</a></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-lg-12">
                <div class=" paging_bootstrap">
                    {{$data->links()}}
                </div>
            </div>

        </div>

    </div>
    <div class="clear"></div>
</div>



<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
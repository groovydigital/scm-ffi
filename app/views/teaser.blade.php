<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bulan Sarapan Sempurna</title>
    <link rel="icon" href="/frontend/img/favicon.png" type="image/png" sizes="32x32">
    <!-- for Google -->
    <meta name="description" content="Yuk mulai kebiasaan sarapan sempurna bersama keluarga dan menangkan Voucher Belanja, 30 Set Meja Makan, dan Grand Prize Sarapan Sempurna Fantastis!">
    <meta name="author" content="Frisian Flag Indonesia">
    <meta name="keywords" content="Bulan Sarapan Sempurna, bulan sarapan sempurna, bulan sarapan, Bulan Sarapan, Frisian Flag Indonesia">
    <!-- Bootstrap -->
    <link href="frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="frontend/css/style.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div id="intro-teaser">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4 col-xs-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10 text-right">
                <div class="logo-ffi">
                    <img src="/frontend/img/logo-frisian-flag.png">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
                <div class="logo-scm">
                    <img src="/frontend/img/logo-bulan-sarapan-sempurna.png">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-lg-6 col-lg-offset-3 text-center">
                <div class="headline-desktop">
                    Nantikan serunya bulan istimewa keluarga<br>1 Maret 2016
                </div>
                <div class="headline-mobile">
                    Nantikan serunya<br>bulan istimewa keluarga<br>1 Maret 2016
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="wave-up-blue"></div>
    <div class="container-fluid" id="bg-blue">
        <div class="row">
            <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-5 col-lg-2 col-lg-offset-5 text-center">
                <div class="scm-product-teaser">
                    <img src="/frontend/img/frisian-flag-product.png">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="short-description-mobile-teaser">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
                    Yuk mulai kebiasaan sarapan sempurna bersama keluarga dan menangkan Voucher Belanja, 30 Set Meja Makan, dan Grand Prize Sarapan Sempurna Fantastis!<br><br>
                    <a href="{{route("teaser.save")}}" class="btn btn-blue" >Facebook Connect</a>

                    <br><br>
                </div>
            </div>
        </div>
        <div class="short-description-desktop-teaser">

            <div class="col-lg-4 col-lg-offset-2 text-right">
                <div class="short-description-box-teaser">
                    <br>Yuk mulai kebiasaan sarapan sempurna bersama keluarga dan menangkan Voucher Belanja, 30 Set Meja Makan, dan Grand Prize Sarapan Sempurna Fantastis!<br><br>
                </div>
            </div>
            <div class="col-lg-4 text-left">
                <br><br>

                <a href="{{route("teaser.save")}}" class="btn btn-blue" >Facebook Connect</a>
                
                </div>
                <br><br>
            </div>
            <br>
        </div>
        
    </div>
</div>


<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>
</div>

<!-- Modal Teaser -->
<div class="modal fade" id="modalTeaser" tabindex="-1" role="dialog" aria-labelledby="modalTeaser" onLoad="ga('send', 'pageview', 'vp-teaser-greeting');">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel">TERIMA KASIH TELAH MENDAFTAR!</h5>
            </div>
            <div class="modal-body text-center">
                <div class="modal-teaser">Nantikan kemeriahan Bulan Sarapan<br>Sempurna mulai <span>tanggal 1 Maret 2016</span></div>
            </div>
            
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/frontend/js/bootstrap.min.js"></script>
<script type="text/javascript">

    <?php if(isset(\Session::get('users')['provider'])){ ?>
        $('#modalTeaser').modal({
            show: 'true'
        });

    <?php
        \Session::flush();
        }
    ?>
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74187777-1', 'auto');
  ga('send', 'pageview');
</script>
</body>
</html>
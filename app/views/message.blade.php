@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12">
                <div class="modal-message-body">
                    @if(isset($message))
                        @foreach($message as $key =>$val)
                            <div class="modal-message-body">
                                <i class="date">{{$val->created_at}}</i>
                                <h5 class="text-blue"><strong>"{{\User::find($val->user_id)["name"]}}"</strong></h5>
                                {{html_entity_decode($val->message)}}
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
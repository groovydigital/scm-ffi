@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 text-center">
                <div class="modal-post-status-body">
                    <br><br>
                    <form>
                        <div class="form-group"><textarea rows="10" name="caption" id="text-status" class="form-control" placeholder="Tulis Status Disini"></textarea></div>
                        <div class="form-group text-center"><button data-dismiss="modal" type="button" id="btn-text" class="btn btn-yellow">Update Status</button></div>
                    </form>
                    <br>
                    <div class="clear"></div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
<script type="text/javascript">
    $('#btn-text').click(function(){
        var data={
            caption : $('#text-status').val()
        }
        insertText(data);
    });

    function insertText(data){
        $.ajax({
            type: "POST",
            url: "/post/text",
            data: {
                caption : data.caption
            },
            beforeSend: function(){
            },
            error: function(data){
                alert("cannot post text");
            },
            success: function( data )
            {
                $(location).attr('href', "{{url('success-status')}}/"+data.status.id);
            }
        });
    }
</script>
@include('footer')
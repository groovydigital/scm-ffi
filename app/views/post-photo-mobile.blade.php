@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 text-center">
                <div class="modal-post-photo-body">
                    <form id="form-photo" >
                        <div class="form-group">
                            <div class="content-mobile" style="margin-top: 10px;">
                                <div class="form-group text-right" id="btn-rotate"><img src="/frontend/img/icon-rotate.png" width="30">&nbsp;Rotate</div>
                                <div style="width: 100%; height: 250px">
                                    <img style="display: none" id="img-crop" src="/images/upload/post/mobile/{{$image}}" class="image-placeholder">
                                </div>
                            </div>
                            <input type="hidden" value="{{$image}}" name="image" />
                            <input type="hidden"  name="w" id="w" />
                            <input type="hidden"  name="h" id="h" />
                            <input type="hidden"  name="y" id="y" />
                            <input type="hidden"  name="x" id="x" />
                            <input type="hidden"  name="rotate" id="rotate" />
                        </div>
                        

                        <div class="form-group"><textarea name="caption" class="form-control" placeholder="Tulis Status Disini"></textarea></div>
                        <div class="form-group text-center"><button type="button" id="btn-photo" class="btn btn-yellow">Upload</button></div>
                        <div class="loading" id="loading" style="display: none;"><img src="/frontend/img/loader.gif"></div>
                    </form>
                    <br><br>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('js-footer')
<link  href="/plugins/cropper/cropper.min.css" rel="stylesheet">
<script src="/plugins/cropper/cropper.min.js"></script>
<script type="text/javascript">
    $('#img-crop').cropper({
        crop: function(e) {
            // Output the result data for cropping image.
//            console.log(e.x);
//            console.log(e.y);
//            console.log(e.width);
//            console.log(e.height);
//            console.log(e.rotate);
//            console.log(e.scaleX);
//            console.log(e.scaleY);
            $('#w').val(e.width);
            $('#h').val(e.height);
            $('#rotate').val(e.rotate);
            $('#x').val(e.x);
            $('#y').val(e.y);
            $('#img-crop').show();
        }
    });
    $('#btn-rotate').click(function(){
        $('#img-crop').cropper('rotate', 45);
    });
    $('#btn-photo').click(function(){
        $('#loading').show();
        insertPhoto($('#form-photo'));
    });

    function insertPhoto(form_id){
        var formData = new FormData(form_id[0]);
        $.ajax({
            type: "POST",
            url: "/post/photo/mobile",
            data: formData,
            beforeSend: function(){
            },
            contentType: false,
            processData: false,
            error: function(data){
                alert("cannot upload photo");
            },
            success: function( data )
            {

                $(location).attr('href', "{{url('success-photo')}}/"+data.status.id);
                $('#loading').hide();
            }
        });
    }
</script>
@include('footer')
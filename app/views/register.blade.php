@include('head-meta')
<body>
@include('header')
<div class="container" id="bg">
    <div class="col-xs-10 col-xs-offset-1 col-lg-12 text-center">
        <div class="logo-scm">
            <img src="/frontend/img/logo-bulan-sarapan-sempurna.png" width="100%">
        </div>
    </div>
</div>

<div class="home-wave-up-mobile"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <h3 class="text-blue">Profil</h3>
                <form id="form-register" name="form-register">

                    <input type="hidden" id="provider" name="provider" value="{{\Session::get('register')['provider']}}">
                    <input type="hidden" id="name" name="name" value="{{\Session::get('register')['name']}}">
                    <input type="hidden" id="gender" name="provider" value="{{\Session::get('register')['gender']}}">
                    <input type="hidden" id="birthdate" name="birthdate" value="{{\Session::get('register')['birthdate']}}">
                    <input type="hidden" id="img_fb" name="img_fb" value="{{\Session::get('register')['img_fb']}}">
                    <input type="hidden" id="friendlist" name="friendlist" value="{{htmlspecialchars(\Session::get('register')['friendlist'])}}">

                    <div class="form-group"><input type="text" name="username" id="username" value="{{\Session::get('register')['username']}}" class="form-control" placeholder="Nama * " required></div>
                    <div class="form-group"><input type="email" name="email" id="email" value="{{\Session::get('register')['email']}}" class="form-control" placeholder="Email * " required></div>
                    <div class="form-group"><input type="text" name="phone" id="phone" class="form-control" placeholder="No. Hp * " required></div>
                    <div class="form-group"><input type="text" name="shop" id="shop" class="form-control" placeholder="Biasa Berbelanja Susu Frisian Flag di... (supermarket, mini market, warung, dll..)"></div>
                    <div class="form-group"><input type="text" name="spouse" id="spouse" class="form-control" placeholder="Nama Pasangan"></div>
                    <div class="form-group"><input type="text" name="child[]" class="form-control child" placeholder="Nama Anak"></div>
                    <div id="divAddChild"></div>
                    <div class="form-group"><button id="btnAddChild" type="button" class="btn btn-light-blue"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Anak</button></div>

                    <div class="form-group"><button type="button" class="btn btn-yellow" id="btn-register">Lanjutkan</button></div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>
@include('js-footer')
<script type="text/javascript" src="<?php echo asset('plugins/validate/jquery-validate.min.js');?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#form-register").validate({
            rules: {
                username:{
                    required: true
                },
                phone:{
                    required: true,
                    number: true,
                    rangelength: [7, 14]
                },
                email:{
                    required: true,
                    email: true,
                }
            },
        });
        $('#btn-register').click(function() {
            if($("#form-register").valid()){
                var anak="";
                var count=0;
                $('.child').each(function(e){
                    if($(this).val()!=""){
                        if(count==0){
                            anak=$(this).val();
                        }else{
                            anak+=','+$(this).val();
                        }
                    }else{
                        count++;
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "/register/save",
                    data: {
                        provider 	: $('#provider').val(),
                        username	: $('#username').val(),
                        name		: $('#name').val(),
                        email		: $('#email').val(),
                        gender		: $('#gender').val(),
                        birthdate	: $('#birthdate').val(),
                        img_fb		: $('#img_fb').val(),
                        friendlist	: $('#friendlist').val(),
                        spouse		: $('#spouse').val(),
                        phone		: $('#phone').val(),
                        shop		: $('#shop').val(),
                        child		: anak
                    },
                    beforeSend: function(){
                    },
                    error: function(data){
                        $('.error-word').text(data.responseJSON.error.message);
                    },
                    success: function( data )
                    {
                        $(location).attr('href', "{{route('welcome')}}");
                    }
                });
            }
        });
        $("#btnAddChild").click(function(){
            $("#divAddChild").append('<div class="form-group"><input type="text" name="child[]" class="child form-control" placeholder="Nama Anak"></div>');
        });
    });
    // $(function() {
        // $('#phone').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    // })
</script>
@include('footer')
@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3 class="text-blue">Diary Bulan Sarapan Sempurna</h3>
                <h4>Cara Ikutan:</h4>
                <div class="how-to-detail">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                            <p class="number">1</p>
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <p>Daftarkan diri anda ke website Bulan Sarapan Sempurna di www.bulansarapansempurna.com</p><br>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                            <p class="number">2</p>
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <p>Upload post atau foto sarapan keluarga anda di website Bulan Sarapan Sempurna, sebanyak dan sekreatif mungkin</p><br>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                            <p class="number">3</p>
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <p>Share foto Sarapan Sempurna Keluarga Anda ke Facebook untuk membuka kesempatan memenangkan hadiah set meja makan dan Grand Prize Sarapan Sempurna Fantastis.</p><br>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    
    
    <div class="clear"></div>

</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
@include('head-meta')
<body>
@include('header')
<div class="container-fluid btn-posting">
    <!-- <div class="row text-center">
        <a href="{{route('post-photo')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-photo');"><img src="/frontend/img/icon-photo.png" class="icon-photo-posting">Photo</div></a>
        <a href="{{route('post-status')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-status');"><img src="/frontend/img/icon-status.png" class="icon-status-posting" >Status</div></a>
        <a href="{{route('check-in')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-location');"><img src="/frontend/img/icon-location.png" class="icon-location-posting" >Check In</div></a>
    </div> -->
    <div class="row text-center">
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-photo');"><img src="/frontend/img/icon-photo.png" class="icon-photo-posting">Photo</div></a>
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-status');"><img src="/frontend/img/icon-status.png" class="icon-status-posting" >Status</div></a>
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-location');"><img src="/frontend/img/icon-location.png" class="icon-location-posting" >Check In</div></a>
    </div>
</div>

<div class="container" id="bg">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-lg-12 text-center">
            <div class="profile-mydiary">
                <img src="{{\Session::get('users')['image']}}" class="img-circle">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a href="{{route('messages')}}">
            <div class="message-mydiary">
                <?php if($total_message>0){?><div class="notification">{{$total_message}}</div><?php } ?>
                <a href="{{route('messages')}}"><img id="message" src="/frontend/img/icon-message.png"></a>
            </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            <div class="username-mydiary">{{\Session::get('users')['username']}}</div>
        </div>
    </div>
</div>

<div class="home-wave-up-mobile"></div>
<div class="container-fluid" id="home-mydiary">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-xs-12 col-md-12 col-lg-10 col-lg-offset-1 text-center">
                <div class="filter-mobile">
                    <a class="col-xs-4 col-lg-4 filter-mobile-block" href="{{route('home')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'all-diary');">Semua Diary</a>
                    <a class="col-xs-4 col-lg-4 filter-mobile-block" href="{{route('friends')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'friends-diary');">Diary Teman</a>
                    <a class="col-xs-4 col-lg-4 filter-mobile-block active" href="{{route('mydiary')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'my-diary');">Diary Saya</a>
                </div>
            </div>
        </div>

        <div class="content-mobile no-gutter">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="/frontend/img/goodbye.png" width="100%">
                    <div class="content-home" id="my"/>
                    <div class="loading-timeline text-center" style="display: none;">
                        <img src="/frontend/img/loader.gif">
                    </div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

{{--<div class="container-fluid" id="bg-blue-2">--}}
    {{--<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">--}}
        {{--<div class="footer">--}}
            {{--&copy; Frisian Flag 2016--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@include('modal')
@include('js-footer')
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({url: "{{url('/post/me?page=')}}"+page, success: function(result){
            $("#my").html(result);
        }});
    });
    var page=1;


    $(window).on("scroll", function() {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            $('.loading-timeline').show();
            page++;
            $.ajax({url: "{{url('/post/me?page=')}}"+page, success: function(result){
                $('.loading-timeline').hide();
                $("#my").append(result);

            }});
        }
    });
</script>
@include('footer')
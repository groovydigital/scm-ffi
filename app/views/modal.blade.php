<!-- Modal Good Bye -->
<div class="modal fade" id="modalGoodbye" tabindex="-1" role="dialog" aria-labelledby="modalGoodbye">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-message-body">
                    <img src="/frontend/img/goodbye.png" width="100%">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Message -->
<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="modalMessage">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-message.png" class="icon-message">&nbsp;&nbsp;INBOX</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                @if(isset($message))
                    @foreach($message as $key =>$val)
                        <div class="modal-message-body">
                            <i class="date">{{$val->created_at}}</i>
                            <h5 class="text-blue"><strong>"{{\User::find($val->user_id)["name"]}}"</strong></h5>
                            {{html_entity_decode($val->message)}}
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

<!-- Modal Post Photo -->
<div class="modal fade" id="modalPostPhoto" tabindex="-1" role="dialog" aria-labelledby="modalPostPhoto">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-photo.png" class="icon-photo">&nbsp;&nbsp;UPLOAD DIARI HARI INI</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-photo-body">
                    <form id="form-photo" >
                        <div class="form-group">
                            <img src="/frontend/img/image-placeholder.png" class="image-placeholder">
                            <br><input accept="image/*" name="file" type="file" class="btn btn-light-blue">
                        </div>
                        <div class="form-group"><textarea name="caption" class="form-control" placeholder="Tulis Status Disini"></textarea></div>
                        <div class="form-group text-center"><button data-dismiss="modal" type="button" id="btn-photo" class="btn btn-submit-pop-up" onclick="ga('send', 'pageview', '/vp-post-photo-success');">Upload</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Post Status -->
<div class="modal fade" id="modalPostStatus" tabindex="-1" role="dialog" aria-labelledby="modalPostStatus">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-status.png" class="icon-photo">&nbsp;&nbsp;UPLOAD DIARI HARI INI</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-status-body">
                    <form>
                        <div class="form-group"><textarea name="caption" id="text-status" class="form-control" placeholder="Tulis Status Disini"></textarea></div>
                        <div class="form-group text-center"><button data-dismiss="modal" type="button" id="btn-text" class="btn btn-submit-pop-up" onclick="ga('send', 'pageview', '/vp-post-status-success');">Post Status</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Post Location -->
<div class="modal fade" id="modalPostLocation" tabindex="-1" role="dialog" aria-labelledby="modalPostLocation">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-location.png" class="icon-photo">&nbsp;&nbsp;UPLOAD DIARI HARI INI</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-location-body">
                    <form method="post" id="geocoding_form">
                        <div class="input">
                            <input type="text" id="address" name="address" class="form-control form-control-location"/>
                            <!-- <input type="submit" class="btn-light-blue" value="Search" /> -->
                        </div>
                    </form>
                    <form>
                        <div class="form-group"><div id="map"></div></div>
                        <input type="hidden" name="long" id="long" value="">
                        <input type="hidden" name="lat" id="lat" value="">
                        <input type="hidden" name="caption-location" id="caption-location" value="">
                        <div class="padding">
                            <div class="form-group"><textarea class="form-control" id="text-location" placeholder="Update Status Disini"></textarea></div>
                            <div class="form-group text-center"><button data-dismiss="modal" type="button" id="btn-location" class="btn btn-submit-pop-up" onclick="ga('send', 'pageview', '/vp-post-location-success');">Check In</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal How To -->
<div class="modal fade" id="modalHowTo" tabindex="-1" role="dialog" aria-labelledby="modalHowTo">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-question.png" class="icon-photo">&nbsp;&nbsp;HOW TO</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-status-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h3 class="text-blue">Diary Bulan Sarapan Sempurna</h3>
                            <h4>Cara Ikutan:</h4>
                            <div class="how-to-detail">
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                                        <p class="number">1</p>
                                    </div>
                                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                        <p>Daftarkan diri anda ke website Bulan Sarapan Sempurna di www.bulansarapansempurna.com</p><br>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                                        <p class="number">2</p>
                                    </div>
                                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                        <p>Upload post atau foto sarapan keluarga anda di website Bulan Sarapan Sempurna, sebanyak dan sekreatif mungkin</p><br>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                                        <p class="number">3</p>
                                    </div>
                                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                        <p>Share foto Sarapan Sempurna Keluarga Anda ke Facebook untuk membuka kesempatan memenangkan hadiah set meja makan dan Grand Prize Sarapan Sempurna Fantastis.</p><br>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal TNC -->
<div class="modal fade" id="modaltnc" tabindex="-1" role="dialog" aria-labelledby="modaltnc">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-question.png" class="icon-photo">&nbsp;&nbsp;TERMS & CONDITIONS</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-status-body">
                    <h3>{{$tnc->title}}</h3>
                    {{html_entity_decode($tnc->text)}}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal PVP -->
<div class="modal fade" id="modalpvp" tabindex="-1" role="dialog" aria-labelledby="modalpvp">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-question.png" class="icon-photo">&nbsp;&nbsp;Privacy Policy</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-status-body">
                    <h3>{{$privacy->title}}</h3>
                    {{html_entity_decode($privacy->text)}}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Success Post Photo -->
<div class="modal fade" id="modalPostPhotoSuccess" tabindex="-1" role="dialog" aria-labelledby="modalPostPhotoSuccess">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-photo.png" class="icon-photo">&nbsp;&nbsp;UPLOAD BERHASIL!</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-photo-success">
                    <br><p>Sarapan Keluarga <span id="photo-name"></span> Hari ini:</p>
                    <div id="photo-image"></div>
                </div>
                <div class="body-share text-center">
                    <br>
                    <span>Foto Berhasil diupload</span>
                    <p>Ayo share ke Facebook untuk memiliki kesempatan memenangkan set meja makan dan grand prize!</p>
                    <button class="btn btn-blue" id="btn-share-fb-photo" data-url="" >Share ke Facebook</button>
                    <br>
                    <a href="{{route('home')}}">back to home</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Success Post Status -->
<div class="modal fade" id="modalPostStatusSuccess" tabindex="-1" role="dialog" aria-labelledby="modalPostStatusSuccess">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-status.png" class="icon-status">&nbsp;&nbsp;UPLOAD BERHASIL!</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-status-success">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="/frontend/img/icon-status.png" class="icon-status"></div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <p><strong>Sarapan Keluarga <span id="status-name"></span> Hari ini:</strong></p>
                            <p id="status-text"></p>
                        </div>
                    </div>
                </div>
                <div class="body-share text-center">
                    <span>Status Berhasil diupload</span>
                    <p>Ayo share ke Facebook untuk memiliki kesempatan memenangkan set meja makan dan grand prize!</p>
                    <button class="btn btn-blue" id="btn-share-fb-status" data-url="" >Share ke Facebook</button>
                    <br>
                    <a href="{{route('home')}}">back to home</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Success Location Status -->
<div class="modal fade" id="modalPostLocationSuccess" tabindex="-1" role="dialog" aria-labelledby="modalPostLocationSuccess">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/icon-location.png" class="icon-status">&nbsp;&nbsp;UPLOAD BERHASIL!</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <div class="modal-post-location-success">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="/frontend/img/icon-location.png" class="icon-status"></div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <p> <strong><span id="location-name"></span></strong></p>
                            <p id="location-status"></p>
                        </div>
                    </div>
                </div>
                <div class="body-share text-center">
                    <span>Check-In Berhasil diupload</span>
                    <p>Ayo share ke Facebook untuk memiliki kesempatan memenangkan set meja makan dan grand prize!</p>
                    <button class="btn btn-blue" id="btn-share-fb-loc" data-url="" >Share ke Facebook</button>
                    <br>
                    <a href="{{route('home')}}">back to home</a>
                </div>
            </div>
        </div>
    </div>
</div>
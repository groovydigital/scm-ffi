@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 text-center">
                <div class="modal-post-photo-body">
                    <br><br>
                    <form id="form-photo" >
                        <div class="form-group">
                            <img src="/frontend/img/image-placeholder.png" class="image-placeholder">
                            <br><br><input accept="image/*" name="file" type="file" class="btn btn-light-blue">
                        </div>
                        <div class="form-group"><textarea name="caption" class="form-control" placeholder="Tulis Status Disini"></textarea></div>
                        <div class="form-group text-center"><button type="button" id="btn-photo" class="btn btn-yellow">Upload</button></div>
                        <div class="loading" id="loading" style="display: none;"><img src="/frontend/img/loader.gif"></div>
                    </form>
                    <br><br>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('js-footer')
<script type="text/javascript">
    $('#btn-photo').click(function(){
        $('#loading').show();
        insertPhoto($('#form-photo'));
    });

    function insertPhoto(form_id){
        var formData = new FormData(form_id[0]);
        $.ajax({
            type: "POST",
            url: "/post/photo",
            data: formData,
            beforeSend: function(){
            },
            contentType: false,
            processData: false,
            error: function(data){
                alert("cannot upload photo");
            },
            success: function( data )
            {
                $(location).attr('href', "{{url('success-photo')}}/"+data.status.id);
                $('#loading').hide();
            }
        });
    }
</script>
@include('footer')
@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="bg-post-photo">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-lg-12 text-center">
                <div class="choose-post-photo">

                    <button class="btn btn-yellow" id="take-photo" >Ambil Gambar</button>
                    <form id="form-photo" >
                    <input type="file" name="file" style="display: none;" capture="camera" accept="image/*" id="takePictureField">
                    </form>
                    <br><br>
                    <a href="{{route('upload-photo')}}"><button class="btn btn-yellow">Pilih Dari Galeri</button></a>
                    <div class="loading" id="loading" style="display: none;"><img src="/frontend/img/loader.gif"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
<script language="JavaScript">
    $('#take-photo').click(function(){
        $('#takePictureField').trigger('click');
    });

    $('#takePictureField').change(function() {
        $('#loading').show();
        var formData = new FormData($('#form-photo')[0]);
        $.ajax({
            type: "POST",
            url: "/upload-api",
            data: formData,
            beforeSend: function(){
            },
            contentType: false,
            processData: false,
            error: function(data){
                alert("cannot upload photo");
                $('#loading').hide();
            },
            success: function( data )
            {
                
                $(location).attr('href', "{{url('uploading-photo')}}/"+data.image);
                $('#loading').hide();
            }
        });
    });

</script>
@include('footer')
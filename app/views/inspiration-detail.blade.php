
@include('head-meta')
<body>
@include('header')

<div id="inspiration-detail-mobile">
    <div class="container" style="background: url('/images/upload/blog/mobile/{{$data["image"]}}'); width:100%; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                <div class="video">
                    <img src="/frontend/img/overlay-play-btn.png" class="overlay-play-btn" data-toggle="modal" data-target="#modalVideo" onclick="ga('send', 'pageview', '/vp-video');">
                </div>
                <div class="inspiration-detail-title">{{$data["title"]}}</div>
            </div>
        </div>
    </div>
    <div class="home-wave-up-mobile"></div>
    <div class="container-fluid" id="home">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-12 text-center">
                    <button class="btn btn-yellow btn-portion">{{$data["porsi"]}}</button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <br>
                    <div class="inspiration-box-header">
                        <div class="inspiration-header-top text-center">
                            <h4>Bahan</h4>
                        </div>
                        <div class="inspiration-header-bottom">
                            {{html_entity_decode($data["bahan"])}}
                        </div>
                    </div>
                    <br>
                    <div class="inspiration-box-desc">
                        <div class="inspiration-desc-top text-center">
                            <h4>Cara Membuat</h4>
                        </div>
                        <div class="inspiration-desc-bottom">
                            {{html_entity_decode($data["text"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="clear"></div>
</div>

<div id="inspiration-detail-desktop">
    <div class="home-wave-up"></div>
    <div class="container-fluid" id="home">
        <div class="container">
            <br><br>
            <div class="inspiration-top-header">
                <div class="row">
                    <div class="col-lg-6">
                        <br><a href="/inspirasi"><button class="btn btn-yellows btn-filter">Kembali</button></a>
                    </div>
                    <div class="col-lg-6 text-right">
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <br>
            <div class="row no-gutter">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-3 ">
                    <div class="inspiration-box-title">
                        <div class="inspiration-title-top text-center">
                            <h4>{{$data["title"]}}</h4>
                        </div>
                        <div class="inspiration-title-bottom">
                            <div class="video">
                                <img src="/frontend/img/overlay-play-btn.png" class="overlay-play-btn" data-toggle="modal" data-target="#modalVideo" onclick="ga('send', 'pageview', '/vp-video');">
                            </div>
                            <img src="/images/upload/blog/mobile/{{$data["image"]}}" width="100%">
                            <br>
                            <button class="btn btn-yellow btn-portion">{{$data["porsi"]}}</button>
                        </div>
                    </div>
                    <div class="inspiration-cover">
                        <img src="/images/upload/blog/mobile/{{$data["image"]}}" width="100%">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inspiration-box-header">
                        <div class="inspiration-header-top text-center">
                            <h4>Bahan</h4>
                        </div>
                        <div class="inspiration-header-bottom">
                            {{html_entity_decode($data["bahan"])}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="inspiration-box-desc">
                        <div class="inspiration-desc-top text-center">
                            <h4>Cara Membuat</h4>
                        </div>
                        <div class="inspiration-desc-bottom">
                            {{html_entity_decode($data["text"])}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="clear"></div>
    </div>
</div>

<!-- Modal Video -->
<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="modalVideo">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><img src="/frontend/img/overlay-play-btn.png" class="icon-message">&nbsp;&nbsp;VIDEO INSPIRASI SEMPURNA</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">

                <iframe width="100%" height="300" src="{{$data['url']}}" frameborder="0" allowfullscreen></iframe>

            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
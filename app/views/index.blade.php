@include('head-meta')
<body>

<div id="intro-teaser">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4 col-xs-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10 text-right">
				<div class="logo-ffi">
					<img src="/frontend/img/logo-frisian-flag.png">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
				<div class="logo-scm">
					<img src="/frontend/img/logo-bulan-sarapan-sempurna.png">
				</div>
			</div>
		</div>
	</div>

	<div class="clear"></div>

	<div class="wave-up-blue"></div>
	<div class="container-fluid" id="bg-blue">

		<div class="short-description-mobile">
			
			<div class="row">
				<div class="col-xs-6 col-xs-offset-3 text-center">
					<div class="logo-scm-2">
						<img src="/frontend/img/logo-bulan-sarapan-sempurna.png">
					</div>
				</div>
				<div class="col-xs-10 col-xs-offset-1 text-center">
					<div class="short-description-box">
						<br>Ayo bangun kebiasaan sarapan sempurna bersama keluarga, dan menangkan hadiah menarik setiap harinya.</strong>
					</div>
					<br>
					<a href="{{route("login.mobile")}}"><button class="btn btn-blue" data-toggle="modal" data-target="#modalLogin" onclick="ga('send', 'pageview', '/vp-register');">Masuk / Daftar</button></a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 text-center"><hr></div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<ul class="icon-mechanism">
						<li><img src="/frontend/img/icon-1.png"><br><br>Hadiah Harian<br><strong>300 Voucher Belanja</strong></li>
						<li><img src="/frontend/img/icon-2.png"><br><br>Hadiah Mingguan<br><strong>30 Set Meja Makan</strong></li>
						<li><img src="/frontend/img/icon-3.png"><br><br>Hadiah Utama<br><strong>3 Paket Sarapan Sempurna Fantastis</strong></li>
					</ul>
					<br>
					<!-- <div class="text-yellow">Daftar</div> -->
					<br>
					
					<br><br><br>
				</div>

			</div>
			<div class="row">
				<div class="col-xs-6 col-xs-offset-3 text-center">
					<div class="scm-product">
						<img src="/frontend/img/frisian-flag-product.png">
					</div>
				</div>
			</div>
		</div>

		<div class="short-description-desktop">
			<br>
			<div class="row">
				<div class="col-lg-2 col-lg-offset-3 text-right">
					<div class="scm-product">
						<img src="/frontend/img/frisian-flag-product.png">
					</div>
				</div>
				<div class="col-lg-4">
					<div class="short-description-box">
						<br>Ayo bangun kebiasaan sarapan sempurna bersama keluarga, dan menangkan hadiah menarik setiap harinya.</strong>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center"><hr></div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<ul class="icon-mechanism">
						<li><img src="/frontend/img/icon-1.png"><br><br>Hadiah Harian<br><strong>300 Voucher Belanja</strong></li>
						<li><img src="/frontend/img/icon-2.png"><br><br>Hadiah Mingguan<br><strong>30 Set Meja Makan</strong></li>
						<li><img src="/frontend/img/icon-3.png"><br><br>Hadiah Utama<br><strong>3 Paket Sarapan Sempurna Fantastis</strong></li>
					</ul>
					<br>
					<!-- <div class="text-yellow">Daftar</div> -->
					<br>
						<a href="{{route('login.save')}}" class="btn btn-blue" onclick="ga('send', 'pageview', '/vp-register');">Masuk / Daftar</a>

					<br><br><br>
				</div>

			</div>
		</div>
	</div>


	<div class="wave-up-blue-2"></div>
	<div class="container-fluid" id="bg-blue-2">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
			<div class="footer">
				&copy; Frisian Flag 2016
			</div>
		</div>
	</div>
</div>

<!-- Modal Register -->
<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="modalRegister">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-md">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="myModalLabel">PROFIL</h5>
			</div>
			<div class="wave-up-blue-bottom"></div>
			<div class="modal-body">
				<br>
				
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1">
						<form id="form-register" name="form-register">

							<input type="hidden" id="provider" name="provider" value="{{\Session::get('register')['provider']}}">
							<input type="hidden" id="name" name="name" value="{{\Session::get('register')['name']}}">
							<input type="hidden" id="gender" name="provider" value="{{\Session::get('register')['gender']}}">
							<input type="hidden" id="birthdate" name="birthdate" value="{{\Session::get('register')['birthdate']}}">
							<input type="hidden" id="img_fb" name="img_fb" value="{{\Session::get('register')['img_fb']}}">
							<input type="hidden" id="friendlist" name="friendlist" value="{{htmlspecialchars(\Session::get('register')['friendlist'])}}">

							<div class="form-group"><input type="text" name="username" id="username" value="{{\Session::get('register')['username']}}" class="form-control" placeholder="Nama * " ></div>
							<div class="form-group"><input type="email" name="email" id="email" value="{{\Session::get('register')['email']}}" class="form-control" placeholder="Email * " ></div>
							<div class="form-group"><input type="text" name="phone" id="phone" class="form-control" placeholder="No. Hp * " ></div>
							<div class="form-group"><input type="text" name="shop" id="shop" class="form-control" placeholder="Biasa Berbelanja Susu Frisian Flag di... (supermarket, mini market, warung, dll..)"></div>
							<div class="form-group"><input type="text" name="spouse" id="spouse" class="form-control" placeholder="Nama Pasangan"></div>
							<div class="form-group"><input type="text" name="child[]" class="form-control child" placeholder="Nama Anak"></div>
							<div id="divAddChild"></div>
							<div class="form-group"><button id="btnAddChild" type="button" class="btn btn-light-blue"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Anak</button></div>

							<div class="form-group"><button type="button" class="btn btn-yellow" id="btn-register" onclick="ga('send', 'pageview', '/vp-welcome');">Lanjutkan</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Join -->
<div class="modal fade" id="modalJoin" tabindex="-1" role="dialog" aria-labelledby="modalJoin">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-md">
			<div class="modal-header text-center">
				<div class="row">
					<div class="col-xs-3 col-lg-3">
						<div class="profile">
							<img src="{{\Session::get('register')['img_fb']}}" class="img-circle">
						</div>
					</div>
					<div class="col-xs-9 col-lg-9">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h5 class="modal-title" id="myModalLabel">Selamat bergabung,</h5><h3>{{\Session::get('register')['username']}}</h3>
					</div>
				</div>
			</div>
			<div class="wave-up-blue-bottom"></div>
			<div class="modal-body">
				<br>
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1">
						<div class="join">
							<br><br>
							Yuk mulai kebiasaan Sarapan Sempurna Keluarga bersama Frisian Flag dan dapatkan kesempatan memenangkan set meja makan cantik dan pengalaman Sarapan Sempurna Fantastis!<br><br>
						</div>
						<div class="form-group"><a href="{{route('home')}}"><button type="button" class="btn btn-yellow">Update Diary Hari Ini</button></a></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

@include('js-footer')
<script type="text/javascript" src="<?php echo asset('plugins/validate/jquery-validate.min.js');?>"></script>
<script>
	$(document).ready(function(){
		$("#btnAddChild").click(function(){
			$("#divAddChild").append('<div class="form-group"><input type="text" name="child[]" class="child form-control" placeholder="Nama Anak"></div>');
		});

		$("#form-register").validate({
			rules: {
				username:{
					required: true
				},
				phone:{
					required: true,
					number: true,
					rangelength: [7, 14]
				},
				email:{
					required: true,
					email: true,
				}
			},
		});

		$('#btn-register').click(function() {
			if($("#form-register").valid()){
				var anak="";
				var count=0;
				$('.child').each(function(e){
					if($(this).val()!=""){
						if(count==0){
							anak=$(this).val();
						}else{
							anak+=','+$(this).val();
						}
					}else{
						count++;
					}
				});

				$.ajax({
					type: "POST",
					url: "/login/register/user",
					data: {
						provider 	: $('#provider').val(),
						username	: $('#username').val(),
						name		: $('#name').val(),
						email		: $('#email').val(),
						gender		: $('#gender').val(),
						birthdate	: $('#birthdate').val(),
						img_fb		: $('#img_fb').val(),
						friendlist	: $('#friendlist').val(),
						spouse		: $('#spouse').val(),
						phone		: $('#phone').val(),
						shop		: $('#shop').val(),
						child		: anak
					},
					beforeSend: function(){
					},
					error: function(data){
						$('.error-word').text(data.responseJSON.error.message);
					},
					success: function( data )
					{
						$('#modalRegister').modal('toggle');
						$('#modalJoin').modal({
							show: 'true'
						});
					}
				});
			}
		});

	});

</script>
<script type="text/javascript">
	$('#modalJoin').on('hidden.bs.modal', function (e) {
		$(location).attr('href', '{{route('home')}}');
	})

	<?php
		if(isset(\Session::get('register')['provider'])){ ?>
        $('#modalRegister').modal({
		show: 'true'
	});

	<?php
	\Session::flush();
	}
	?>

	// $(function() {
	// 	$('#phone').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	// })
</script>
@include('footer')
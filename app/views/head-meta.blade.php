<!DOCTYPE html>
<html lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$page_title}}</title>
    <link rel="icon" href="/frontend/img/favicon.png" type="image/png" sizes="32x32">
    <!-- for Google -->
    <meta name="description" content="Yuk mulai kebiasaan sarapan sempurna bersama keluarga dan menangkan Voucher Belanja, 30 Set Meja Makan, dan Grand Prize Sarapan Sempurna Fantastis!">
    {{--<meta name="author" content="Frisian Flag Indonesia">--}}
    <meta name="keywords" content="Bulan Sarapan Sempurna, bulan sarapan sempurna, bulan sarapan, Bulan Sarapan, Frisian Flag Indonesia">

    <meta property="fb:app_id" content="355740294608174" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{url()}}" />
    <meta property="og:title" content="Bulan Sarapan Sempurna" />
    <meta property="og:image" content="http://bulansarapansempurna.com/frontend/img/bulan-sarapan-sempurna.jpg" />
    <meta property="og:description" content="Yuk mulai kebiasaan sarapan sempurna bersama keluarga dan menangkan Voucher Belanja, 30 Set Meja Makan, dan Grand Prize Sarapan Sempurna Fantastis!" />

    <link rel="canonical" href="{{url()}}" />

    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <script src="/frontend/js/html5shiv.min.js"></script>
    <script src="/frontend/js/respond.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-74187777-1', 'auto');
      ga('send', 'pageview');

    </script>
</head>
@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h3 class="text-blue">{{$data->title}}</h3>
                <div class="how-to-detail">
                    {{html_entity_decode($data->text) }}
                </div>
            </div>
        </div>
    </div>
    <br>
    
    <div class="clear"></div>
    <div class="clear"></div>

</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
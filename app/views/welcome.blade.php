@include('head-meta')
<body>
@include('header')
<div class="container" id="bg">
    <div class="col-xs-10 col-xs-offset-1 col-lg-12 text-center">
        <div class="logo-scm">
            <img src="/frontend/img/logo-bulan-sarapan-sempurna.png" width="100%">
        </div>
        <br>
    </div>
</div>

<div class="home-wave-up-mobile"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 text-center">
                <div class="profile-welcome">
                    <img src="{{\Session::get('users')['image']}}" class="img-circle">
                </div>
                <h5 class="modal-title">Selamat bergabung,</h5><h3>{{\Session::get('users')['username']}}</h3>
                <div class="join">
                    <br>
                    Yuk mulai kebiasaan Sarapan Sempurna Keluarga bersama Frisian Flag dan dapatkan kesempatan memenangkan set meja makan cantik dan pengalaman Sarapan Sempurna Fantastis!
                    <br><br>
                </div>
                <div class="form-group"><a href="{{route('home')}}"><button type="button" class="btn btn-yellow">Update Diary Hari Ini</button></a></div>
            </div>
        </div>
        <br><br>
    </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
@include('head-meta')
@section('og')
    <meta property="og:type" content="article">
    <meta property="og:title" content="Bulan Sarapan Sempurna">
    <meta property="og:description" content="Bulan Sarapan Sempurna, bulan sarapan sempurna, bulan sarapan, Bulan Sarapan, Frisian Flag Indonesia">
    <meta property="og:image" content="http://www.bulansarapansempurna.com/frontend/img/logo-bulan-sarapan-sempurna.png">
@stop
<body>
@include('header')
<div class="container-fluid btn-posting">
    <!-- <div class="row text-center">
        <a href="{{route('post-photo')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-photo');"><img src="/frontend/img/icon-photo.png" class="icon-photo-posting">Photo</div></a>
        <a href="{{route('post-status')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-status');"><img src="/frontend/img/icon-status.png" class="icon-status-posting" >Status</div></a>
        <a href="{{route('check-in')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-location');"><img src="/frontend/img/icon-location.png" class="icon-location-posting" >Check In</div></a>
    </div> -->
    <div class="row text-center">
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-photo');"><img src="/frontend/img/icon-photo.png" class="icon-photo-posting">Photo</div></a>
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-status');"><img src="/frontend/img/icon-status.png" class="icon-status-posting" >Status</div></a>
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-location');"><img src="/frontend/img/icon-location.png" class="icon-location-posting" >Check In</div></a>
    </div>
</div>

<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-xs-12 col-md-12 col-lg-10 col-lg-offset-1 text-center">
                <div class="filter">
                    <button type="button" class="btn btn-yellows btn-filter btn-all" onClick="ga('send', 'event', 'filter-diary', 'Click', 'all-diary');">Semua Diary</button>
                    <button type="button" class="btn btn-light-blues btn-filter btn-friend" onClick="ga('send', 'event', 'filter-diary', 'Click', 'friends-diary');">Diary Teman</button>
                    <button type="button" class="btn btn-light-blues btn-filter btn-me" onClick="ga('send', 'event', 'filter-diary', 'Click', 'my-diary');">Diary Saya</button>
                </div>
                <div class="filter-mobile">
                    <a class="col-xs-4 col-lg-4 filter-mobile-block  active" href="{{route('home')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'all-diary');">Semua Diary</a>
                    <a class="col-xs-4 col-lg-4 filter-mobile-block" href="{{route('friends')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'friends-diary');">Diary Teman</a>
                    <a class="col-xs-4 col-lg-4 filter-mobile-block" href="{{route('mydiary')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'my-diary');">Diary Saya</a>
                </div>
            </div>
        </div>
        
        <div class="content-mobile no-gutter">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="/frontend/img/goodbye.png" width="100%">
                    <div class="content-home">
                    </div>
                    <div class="loading-timeline text-center" style="display: none;">
                        <img src="/frontend/img/loader.gif">
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-lg-3 col-lg-offset-1">
                <div id="sidebar" class="sidebar">
                    <div class="sidebar-header">
                        <img src="{{\Session::get('users')['image']}}" class="img-circle">
                        <p>Diary Sarapan</p>
                        <h5>{{\Session::get('users')['username'];}}</h5>
                    </div>
                    <div class="wave-sidebar"></div>
                    <div class="sidebar-body">
                        <ul class="icon-sidebar">
                            <!-- <li ><?php if($total_message>0){?><div class="notification">{{$total_message}}</div><?php } ?><img id="message" src="/frontend/img/icon-message.png" onclick="ga('send', 'pageview', '/vp-message'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-message');"></li>
                            <li data-toggle="modal" data-target="#modalPostPhoto" onclick="ga('send', 'pageview', '/vp-post-photo');"><img src="/frontend/img/icon-photo.png" onclick="ga('send', 'pageview', '/vp-post-photo'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-photo');"></li>
                            <li data-toggle="modal" data-target="#modalPostStatus" onclick="ga('send', 'pageview', '/vp-post-status');"><img src="/frontend/img/icon-status.png" onclick="ga('send', 'pageview', '/vp-post-status'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-status');"></li>
                            <li data-toggle="modal" data-target="#modalPostLocation" onclick="ga('send', 'pageview', '/vp-post-location');"><img src="/frontend/img/icon-location.png" onclick="ga('send', 'pageview', '/vp-post-location'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-location');"></li> -->
                            <li ><?php if($total_message>0){?><div class="notification">{{$total_message}}</div><?php } ?><img id="message" src="/frontend/img/icon-message.png" onclick="ga('send', 'pageview', '/vp-message'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-message');"></li>
                            <li data-toggle="modal" data-target="#modalGoodbye" onclick="ga('send', 'pageview', '/vp-post-photo');"><img src="/frontend/img/icon-photo.png" onclick="ga('send', 'pageview', '/vp-post-photo'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-photo');"></li>
                            <li data-toggle="modal" data-target="#modalGoodbye" onclick="ga('send', 'pageview', '/vp-post-status');"><img src="/frontend/img/icon-status.png" onclick="ga('send', 'pageview', '/vp-post-status'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-status');"></li>
                            <li data-toggle="modal" data-target="#modalGoodbye" onclick="ga('send', 'pageview', '/vp-post-location');"><img src="/frontend/img/icon-location.png" onclick="ga('send', 'pageview', '/vp-post-location'); ga('send', 'event', 'sidebar-icon', 'Click', 'icon-location');"></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-lg-7">
                <div class="content">
                    <img src="/frontend/img/goodbye.png" width="100%">
                    <!-- <div class="get-started">
                        <div class="get-started-header">

                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="#tabPostPhoto" aria-controls="tabPostPhoto" role="tab" data-toggle="tab" onClick="ga('send', 'event', 'get-started', 'Click', 'post-photo');"><img src="/frontend/img/icon-photo.png">&nbsp;&nbsp;Post Photo</a></li>
                                <li role="presentation" class="active"><a href="#tabPostStatus" aria-controls="tabPostStatus" role="tab" data-toggle="tab" onClick="ga('send', 'event', 'get-started', 'Click', 'post-status');"><img src="/frontend/img/icon-status.png">&nbsp;&nbsp;Update Status</a></li>
                                <li role="presentation"><a href="#tabPostLocation" aria-controls="tabPostLocation" role="tab" data-toggle="tab" onClick="ga('send', 'event', 'get-started', 'Click', 'post-location');"><img src="/frontend/img/icon-location.png">&nbsp;&nbsp;Check In</a></li>
                            </ul>
                        </div>

                        <div class="get-started-body text-right">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane" id="tabPostPhoto">
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <img src="{{\Session::get('users')['image']}}" class="img-circle">
                                        </div>
                                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                            <form id="form-photo-2" >
                                                <div class="form-groups">
                                                    <img src="/frontend/img/image-placeholder.png" class="image-placeholder">
                                                    <br><input accept="image/*" name="file" type="file" id="file-photo" class="btn-light-blue-2">
                                                </div>
                                                <div class="form-group"><textarea name="caption" id="caption-photo" class="form-control" placeholder="Tulis Status Disini"></textarea></div>
                                                <button type="button" id="btn-photo-2" class="btn btn-light-blues btn-filter">Get Started</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane active" id="tabPostStatus">
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <img src="{{\Session::get('users')['image']}}" class="img-circle">
                                        </div>
                                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                            <form>
                                                <textarea class="form-control" name="text-main" id="text-main" placeholder="What's on your mind?"></textarea><br>
                                                <button type="button" id="btn-text-main" class="btn btn-light-blues btn-filter">Get Started</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tabPostLocation">
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <img src="{{\Session::get('users')['image']}}" class="img-circle">
                                        </div>
                                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                            
                                            <form method="post" id="geocoding_form2">
                                                <div class="input">
                                                    <input type="text" id="address2" name="address2" class="form-control form-control-location"/>
                                                </div>
                                            </form>
                                            <form>
                                                <div id="map2"></div>
                                                <input type="hidden" name="long2" id="long2" value="">
                                                <input type="hidden" name="lat2" id="lat2" value="">
                                                <input type="hidden" name="caption-location2" id="caption-location2" value="">
                                                <div class="padding">
                                                    <br>
                                                    <div class="form-group"><textarea class="form-control" id="text-location2" placeholder="Update Status Disini"></textarea></div>
                                                    <button type="button"  id="btn-location-main" class="btn btn-light-blues btn-filter">Get Started</button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div> -->
                    <div class="content-home">
                    </div>
                    <div class="loading-timeline text-center" style="display: none;">
                        <img src="/frontend/img/loader.gif">
                    </div>
                  
                </div>

            </div>
        </div>

        <div class="clear"></div>

    </div>
</div>

<div class="home-footer-desktop">
    <div class="wave-up-blue-2"></div>
    <div class="container-fluid" id="bg-blue-2">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
            <div class="footer">
                &copy; Frisian Flag 2016
            </div>
        </div>
    </div>
</div>
@include('modal')
@include('js-footer')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>
<script src="/frontend/js/gmap.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({url: "{{url('/post/all')}}", success: function(result){
            $(".content-home").html(result);
        }});
    });
    var status="all";
    var page=2;
    $('.btn-all').click(function() {
        $.ajax({url: "{{url('/post/all')}}", success: function(result){
            $(".content-home").html(result);
            status="all";
            page=2;
        }});
    });
    $('.btn-friend').click(function() {
        $.ajax({url: "{{url('/post/friend')}}", success: function(result){
            $(".content-home").html(result);
            status="friend";
            page=2;
        }});
    });
    $('.btn-me').click(function() {
        $.ajax({url: "{{url('/post/me')}}", success: function(result){
            $(".content-home").html(result);
            status="me";
            page=2;
        }});

    });


    $(window).on("scroll", function() {
        if($(window).scrollTop() == $(document).height() - $(window).height()) {
//        if($(window).offset().top + $(window).outerHeight(true)) {
            // when scroll to bottom of the page
            $('.loading-timeline').show();
            if(status=="all"){

                $.ajax({url: "{{url('/post/all?page=')}}"+page, success: function(result){
                    $('.loading-timeline').hide();
                    $(".content-home").append(result);
//                    page++;
                }});
            }
            if(status=="friend"){
                $.ajax({url: "{{url('/post/friend?page=')}}"+page, success: function(result){
                    $('.loading-timeline').hide();
                    $(".content-home").append(result);
//                    page++;
                }});
            }
            if(status=="me"){
                $.ajax({url: "{{url('/post/me?page=')}}"+page, success: function(result){
                    $('.loading-timeline').hide();
                    $(".content-home").append(result);
//                    page++;
                }});
            }
            page++;
        }
    });

    $('.filter .btn').click(function(){
        $('.filter .btn').removeClass('btn-yellows').addClass('btn-light-blues');
        $(this).removeClass('btn-light-blues').addClass('btn-yellows');
    });

//    $('.filter-mobile .filter-mobile-block').click(function(){
//        $('.filter-mobile .filter-mobile-block').removeClass('active');
//        $(this).addClass('active');
//    });

    $("a[href='#tabPostLocation']").on('shown.bs.tab', function(){
        map2 = new GMaps({
            el: '#map2',
            width: '100%',
            height: '350px',
            zoom: 16,
            lat: -12.043333,
            lng: -77.028333,
            click: function(e) {
                marker(e.latLng.lat(),e.latLng.lng());
            },
        });

        GMaps.geolocate({
            success: function(position){
                map2.setCenter(position.coords.latitude, position.coords.longitude);
                marker(position.coords.latitude,position.coords.longitude);
            },
            error: function(error){
                alert('Geolocation failed: '+error.message);
            },
            not_supported: function(){
                alert("Your browser does not support geolocation");
            },
        });

        $('#geocoding_form2').submit(function(e){
            e.preventDefault();
            GMaps.geocode({
                address:  $('#address2').val(),
                callback: function(results, status){
                    if(status=='OK'){
                        var latlng = results[0].geometry.location;
                        map2.setCenter(latlng.lat(), latlng.lng());
                        marker(latlng.lat(),latlng.lng());
                    }
                }
            });
        });

        function marker(lat,lng){
            map2.removeMarkers();
            map2.addMarker({
                lat: lat,
                lng: lng,
                callback : function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        console.log(results[1].formatted_address);
                    }
                }
            });
            $('#long2').val(lng);
            $('#lat2').val(lat);
            getAddress(lat,lng)
        }

        function getAddress(lat,lng){
            var opt = {
                lat : lat,
                lng : lng,
                callback : function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $('#caption-location2').val(results[0].formatted_address);
                    }
                }
            }

            GMaps.geocode(opt);
        }
    });

    $('#modalPostLocation').on('shown.bs.modal', function (e) {
        map = new GMaps({
            el: '#map',
            width: '100%',
            height: '350px',
            zoom: 16,
            lat: -12.043333,
            lng: -77.028333,
            click: function(e) {
                marker(e.latLng.lat(),e.latLng.lng());
            },
        });

        GMaps.geolocate({
            success: function(position){
                map.setCenter(position.coords.latitude, position.coords.longitude);
                marker(position.coords.latitude,position.coords.longitude);
            },
            error: function(error){
                alert('Geolocation failed: '+error.message);
            },
            not_supported: function(){
                alert("Your browser does not support geolocation");
            },
        });

        $('#geocoding_form').submit(function(e){
            e.preventDefault();
            GMaps.geocode({
                address:  $('#address').val(),
                callback: function(results, status){
                    if(status=='OK'){
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        marker(latlng.lat(),latlng.lng());
                    }
                }
            });
        });

        function marker(lat,lng){
            map.removeMarkers();
            map.addMarker({
                lat: lat,
                lng: lng,
                callback : function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        console.log(results[1].formatted_address);
                    }
                }
            });
            $('#long').val(lng);
            $('#lat').val(lat);
            getAddress(lat,lng)
        }

        function getAddress(lat,lng){
            var opt = {
                lat : lat,
                lng : lng,
                callback : function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $('#caption-location').val(results[0].formatted_address);
                    }
                }
            }

            GMaps.geocode(opt);
        }

    });
</script>
<script type="text/javascript">
    $('#message').click(function(){

        $.ajax({
            type: "POST",
            url: "/message/notif/read",
            data: {
                user_id :"{{\Session::get('users')['id']}}"
            },
            beforeSend: function(){
            },
            error: function(data){
                alert("error check message");
            },
            success: function( data )
            {
                $('.notification').remove();
                $('#modalMessage').modal({
                    show: 'true'
                });
            }
        });
    });

    $('#btn-photo').click(function(){
        insertPhoto($('#form-photo'));
    });

    $('#btn-photo-2').click(function(){
        insertPhoto($('#form-photo-2'));
        clear();
    });

    function insertPhoto(form_id){
        var formData = new FormData(form_id[0]);
        $.ajax({
            type: "POST",
            url: "/post/photo",
            data: formData,
            beforeSend: function(){
            },
            contentType: false,
            processData: false,
            error: function(data){
                alert("cannot upload photo");
            },
            success: function( data )
            {
                var html= '<div class="post-photo">'+
                        '<img src="/frontend/img/icon-photo.png" class="icon-photo">'+
                        '<img src="/images/upload/post/mobile/'+data.status.image+'">'+

                        '<div class="post-photo-desc">'+
                        '<div class="row">'+
                        '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">'+
                        '<img src="{{\Session::get('users')['image']}}" class="img-circle">'+
                        '</div>'+
                        '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">'+
                        '<p><strong>{{\Session::get('users')['username']}}:</strong> '+data.status.caption+' <br>'+
                        '<i>1 minutes ago</i></p>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>';

                $( ".content-home" ).prepend(html);
                $('#modalPostPhotoSuccess').modal({
                    show: 'true'
                });
                $("#photo-image").html('<img width="100%" src="/images/upload/post/mobile/'+data.status.image+'">');
                $("#photo-name").text("{{\Session::get('users')['username']}}");
                $('#btn-share-fb-photo').attr('data-url','{{url()}}/detail/'+data.status.id);
            }
        });
    }

    $('#btn-text').click(function(){
        var data={
            caption : $('#text-status').val()
        }
        insertText(data);
    });

    $('#btn-text-main').click(function(){
        var data={
            caption : $('#text-main').val()
        }
        insertText(data);
        clear();
    });

    function insertText(data){
        $.ajax({
            type: "POST",
            url: "/post/text",
            data: {
                caption : data.caption
            },
            beforeSend: function(){
            },
            error: function(data){
                alert("cannot post text");
            },
            success: function( data )
            {
                var html='<div class="post-status">'+
                        '<div class="post-status-desc">'+
                        '<div class="row">'+
                        '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">'+
                        '<img src="/frontend/img/icon-status.png" class="icon-status">'+
                        '</div>'+
                        '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">'+
                        '<p><strong>{{\Session::get('users')['username']}}:</strong> '+data.status.caption+'  <br>'+
                        '<i>1 minutes ago</i></p>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                $( ".content-home" ).prepend(html);
                $('#modalPostStatusSuccess').modal({
                    show: 'true'
                });
                $('#status-text').text('"'+data.status.caption+'"');
                $('#status-name').text("{{\Session::get('users')['username']}}");
                $('#btn-share-fb-status').attr('data-url','{{url()}}/detail/'+data.status.id);
            }
        });
    }

    $('#btn-location').click(function(){
        var data={
                    caption :$('#text-location').val(),
                    location:$('#caption-location').val(),
                    latitude:$('#lat').val(),
                    longitude:$('#long').val()
                };
        insertLocation(data);
    });

    $('#btn-location-main').click(function(){
        var data={
            caption :$('#text-location2').val(),
            location:$('#caption-location2').val(),
            latitude:$('#lat2').val(),
            longitude:$('#long2').val()
        };
        insertLocation(data);
        clear();
    });

    function insertLocation(data){
        $.ajax({
            type: "POST",
            url: "/post/location",
            data: {
                caption :data.caption,
                location:data.location,
                latitude:data.latitude,
                longitude:data.longitude
            },
            beforeSend: function(){
            },
            error: function(data){
                alert("cannot post location");
            },
            success: function( data )
            {
                var split =data.status.location.split(",");
                var location= split[0]+', '+split[1]+split[2]

                var html ='<div class="post-location">'+
                        '<div class="post-location-desc">'+
                        '<div class="row">'+
                        '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">'+
                        '&nbsp;&nbsp;&nbsp;<img src="/frontend/img/icon-location.png" class="icon-location">'+
                        '</div>'+
                        '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">'+
                        '<p><strong>{{\Session::get('users')['username'];}}:</strong> '+data.status.caption+'</p>'+
                        '<br>'+
                        '<p>in '+location+'<br>'+
                        '<i>1 minutes ago</i></p>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</div>';

                $( ".content-home" ).prepend(html);
                $('#modalPostLocationSuccess').modal({
                    show: 'true'
                });
                $('#location-status').text('"'+data.status.caption+'"');
                $('#location-name').text(location);
                $('#btn-share-fb-loc').attr('data-url','{{url()}}/detail/'+data.status.id);
            }
        });
    }

    function tab(){
        $('.nav-tabs a[href="#tabPostStatus"]').tab('show');
    }

    function clear(){
        $('#caption-location2').val("");
        $('#caption-photo').val("");
        $('#file-photo').val("");
        $('#text-main').val("");
        tab();
    }

    $('#btn-share-fb-loc').click(function(){
        window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent($(this).attr('data-url')),'facebook-share-dialog','width=626,height=436');return false;
    });
    $('#btn-share-fb-status').click(function(){
        window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent($(this).attr('data-url')),'facebook-share-dialog','width=626,height=436');return false;
    });
    $('#btn-share-fb-photo').click(function(){
        window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent($(this).attr('data-url')),'facebook-share-dialog','width=626,height=436');return false;
    });
</script>
<script>
    $(function(){ // document ready
 
      if (!!$('.sidebar').offset()) { // make sure ".sticky" element exists
     
        var stickyTop = $('.sidebar').offset().top; // returns number 
     
        $(window).scroll(function(){ // scroll event
     
          var windowTop = $(window).scrollTop(); // returns number 
     
          if (stickyTop < windowTop){
            $('.sidebar').css({ position: 'fixed', top: 20});
          }
          else {
            $('.sidebar').css('position','static');
          }
     
        });
     
      }
     
    });
</script>
@include('footer')
@include('head-meta')
<body>
@include('header')
<div class="container-fluid btn-posting">
    <!-- <div class="row text-center">
        <a href="{{route('post-photo')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-photo');"><img src="/frontend/img/icon-photo.png" class="icon-photo-posting">Photo</div></a>
        <a href="{{route('post-status')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-status');"><img src="/frontend/img/icon-status.png" class="icon-status-posting" >Status</div></a>
        <a href="{{route('check-in')}}"><div class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-location');"><img src="/frontend/img/icon-location.png" class="icon-location-posting" >Check In</div></a>
    </div> -->
    <div class="row text-center">
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-photo');"><img src="/frontend/img/icon-photo.png" class="icon-photo-posting">Photo</div></a>
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-status');"><img src="/frontend/img/icon-status.png" class="icon-status-posting" >Status</div></a>
        <a href="#"><div data-toggle="modal" data-target="#modalGoodbye" class="col-xs-4 col-lg-4 btn-posting-block" onClick="ga('send', 'event', 'icon-posting-mobile', 'Click', 'post-location');"><img src="/frontend/img/icon-location.png" class="icon-location-posting" >Check In</div></a>
    </div>
</div>

<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-xs-12 col-md-12 col-lg-10 col-lg-offset-1 text-center">
                <div class="filter-mobile">
                    <a class="col-xs-4 col-lg-4 filter-mobile-block" href="{{route('home')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'all-diary');">Semua Diary</a>
                    <a class="col-xs-4 col-lg-4 filter-mobile-block active" href="{{route('friends')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'friends-diary');">Diary Teman</a>
                    <a class="col-xs-4 col-lg-4 filter-mobile-block" href="{{route('mydiary')}}" onClick="ga('send', 'event', 'filter-diary', 'Click', 'my-diary');">Diary Saya</a>
                </div>
            </div>
        </div>

        <div class="content-mobile no-gutter">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="/frontend/img/goodbye.png" width="100%">
                    <div class="content-home">
                    </div>
                    <div class="loading-timeline text-center" style="display: none;">
                        <img src="/frontend/img/loader.gif">
                    </div>
                </div>
            </div>
        </div>

        <div class="clear"></div>
        
    </div>
</div>

<!-- <div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div> -->

@include('modal')
@include('js-footer')
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({url: "{{url('/post/friend')}}", success: function(result){
            $(".content-home").html(result);

        }});
    });
    var page=1;


    $(window).on("scroll", function() {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            $('.loading-timeline').show();
            page++;
            $.ajax({url: "{{url('/post/friend?page=')}}"+page, success: function(result){
                $('.loading-timeline').hide();
                $(".content-home").append(result);

            }});
        }
    });
</script>
@include('footer')
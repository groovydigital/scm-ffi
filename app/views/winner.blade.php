@include('head-meta')
<body>
@include('header')

<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-6 text-center">
                <div class="tab-winner">
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($category as $key => $val)
                            <li role="presentation" {{($key==0)?"class='active'":""}} ><a href="#{{$val->winner_category_id}}" aria-controls="{{$val->winner_category_id}}" role="tab" data-toggle="tab">{{$val->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-content">
            @foreach($category as $key => $val)
                    <!-- Winner Daily -->
            <div role="tabpanel" class="tab-pane {{($key==0)?'active':''}}"  id="{{$val->winner_category_id}}">
                <br>
                <div class="row ">

                    <?php

                    $stream=\Winner::leftjoin('ffi_users', 'ffi_winners.user_id', '=', 'ffi_users.id')
                            ->where('ffi_winners.is_deleted','=','0')
                            ->where('ffi_winners.winner_category_id','=',$val->winner_category_id)
                            ->orderBy('ffi_winners.winner_id','DESC')
                            ->paginate(100);
                    ?>
                    @if($stream->toArray()["total"]>0)
                        @foreach($stream as $ky => $value)
                            <?php
                                $post=\Post::find($value->id_post);
                             ?>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="winner-header">
                                    <div class="winner-name"><img src="{{$value->image}}" class="img-circle"> {{$value->username}}</div>
                                    <img src="/images/upload/post/mobile/{{$post['image']}}" class="img-cover">
                                </div>
                                <div class="winner-bottom">
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                                            <img src="/frontend/img/trophy.png">
                                        </div>
                                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-center">
                                            <p><strong>{{$val->name}}</strong><br>{{$value->ket_date}}</p>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>

                        @endforeach
                    @else
                            <div class="clear"></div>
                            <div class="clear"></div>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="clear"></div>
    <div class="clear"></div>

</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>
@include('js-footer')
@include('modal')
@include('footer')
@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12">
                <div class="success-mobile">
                    <br>
                    <div class="modal-post-location-success">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="/frontend/img/icon-location.png" class="icon-status"></div>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                <?php $location=explode(",",$data['location'])?>
                                <p><strong>{{$location[0].','.$location[1],$location[2]}}</strong></p>
                                <p>"{{$data['caption']}}"</p>
                            </div>
                        </div>
                    </div>
                    <div class="body-share text-center">
                        <br><span><strong>Check In Berhasil diupload</strong></span><br><br>
                        <p>Ayo share ke Facebook untuk memiliki kesempatan memenangkan set meja makan dan grand prize!</p><br>
                        <button class="btn btn-blue"onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{route("detail.post",array($data["id"]))}}','facebook-share-dialog','width=auto,height=auto');return false;" >Share ke Facebook</button>
                        <br><br>
                        <a href="{{route('home')}}">back to home</a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
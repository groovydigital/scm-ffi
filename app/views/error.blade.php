<!DOCTYPE html>
<html lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error</title>
    <link rel="icon" href="/frontend/img/favicon.png" type="image/png" sizes="32x32">
    <!-- for Google -->
    <meta name="description" content="Yuk mulai kebiasaan sarapan sempurna bersama keluarga dan menangkan Voucher Belanja, 30 Set Meja Makan, dan Grand Prize Sarapan Sempurna Fantastis!">
    <meta name="author" content="Frisian Flag Indonesia">
    <meta name="keywords" content="Bulan Sarapan Sempurna, bulan sarapan sempurna, bulan sarapan, Bulan Sarapan, Frisian Flag Indonesia">


    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <script src="/frontend/js/html5shiv.min.js"></script>
    <script src="/frontend/js/respond.min.js"></script>
</head>
<body>

<div id="intro-teaser">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4 col-xs-offset-8 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10 text-right">
                <div class="logo-ffi">
                    <img src="/frontend/img/logo-frisian-flag.png">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
                <div class="logo-scm">
                    <img src="/frontend/img/logo-bulan-sarapan-sempurna.png">
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="wave-up-blue"></div>
    <div class="container-fluid" id="bg-blue">
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 text-center">
                <div class="error">
                    <h3>Whoops looks like we lost one!<br>404 - Page can not be found</h3>
                    <a href="{{route('home')}}"><button class="btn btn-blue">Back to Home</button></a>
                </div>
            </div>
        </div>
    </div>


    <div class="wave-up-blue-2"></div>
    <div class="container-fluid" id="bg-blue-2">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
            <div class="footer">
                &copy; Frisian Flag 2016
            </div>
        </div>
    </div>
</div>

<!-- Modal Register -->
<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="modalRegister">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel">PROFIL</h5>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <br>
                
<div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <form id="form-register" name="form-register">

                            <input type="hidden" id="provider" name="provider" value="{{\Session::get('register')['provider']}}">
                            <input type="hidden" id="name" name="name" value="{{\Session::get('register')['name']}}">
                            <input type="hidden" id="gender" name="provider" value="{{\Session::get('register')['provider']}}">
                            <input type="hidden" id="birthdate" name="birthdate" value="{{\Session::get('register')['birthdate']}}">
                            <input type="hidden" id="img_fb" name="img_fb" value="{{\Session::get('register')['img_fb']}}">
                            <input type="hidden" id="friendlist" name="friendlist" value="{{htmlspecialchars(\Session::get('register')['friendlist'])}}">

                            <div class="form-group"><input type="text" name="username" id="username" value="{{\Session::get('register')['username']}}" class="form-control" placeholder="Nama * " ></div>
                            <div class="form-group"><input type="email" name="email" id="email" value="{{\Session::get('register')['email']}}" class="form-control" placeholder="Email * " ></div>
                            <div class="form-group"><input type="text" name="phone" id="phone" class="form-control" placeholder="No. Hp * " ></div>
                            <div class="form-group"><input type="text" name="shop" id="shop" class="form-control" placeholder="Biasa Berbelanja Susu Frisian Flag di... (supermarket, mini market, warung, dll..)"></div>
                            <div class="form-group"><input type="text" name="spouse" id="spouse" class="form-control" placeholder="Nama Pasangan"></div>
                            <div class="form-group"><input type="text" name="child[]" class="form-control child" placeholder="Nama Anak"></div>
                            <div id="divAddChild"></div>
                            <div class="form-group"><button id="btnAddChild" type="button" class="btn btn-light-blue"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Anak</button></div>

                            <div class="form-group"><button type="button" class="btn btn-yellow" id="btn-register">Lanjutkan</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Join -->
<div class="modal fade" id="modalJoin" tabindex="-1" role="dialog" aria-labelledby="modalJoin">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header text-center">
                <div class="row">
                    <div class="col-xs-3 col-lg-3">
                        <div class="profile">
                            <img src="{{\Session::get('register')['img_fb']}}" class="img-circle">
                        </div>
                    </div>
                    <div class="col-xs-9 col-lg-9">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="myModalLabel">Selamat bergabung,</h5><h3>{{\Session::get('register')['username']}}</h3>
                    </div>
                </div>
            </div>
            <div class="wave-up-blue-bottom"></div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="join">
                            <br><br>
                            Welcome copy and call to action to share today’s breakfast activity. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt nisl ut euismod mattis. Maecenas sollicitudin ex vitae eros. <br>
                            Welcome copy and call to action to share today’s breakfast activity. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt nisl ut euismod mattis. Maecenas sollicitudin ex vitae eros
                            <br><br>

                        </div>
                        <div class="form-group"><a href="{{route('home')}}"><button type="button" class="btn btn-yellow">Update Diary Hari Ini</button></a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@include('js-footer')
@include('footer')
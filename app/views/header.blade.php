<div class="nav-mobile">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('home')}}"><img src="/frontend/img/logo-bulan-sarapan-sempurna.png"></a>
                <a class="navbar-brand-mobile" href="{{route('home')}}"><img src="/frontend/img/logo-frisian-flag.png"></a>
                <div class="navbar-brand-title" href="#">{{@$title}}</div>
            </div>
        
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li {{($menu=='home')?'class="active"':""}}><a href="{{route('home')}}">Home</a></li>
                    <li {{($menu=='about')?'class="active"':""}}><a href="{{route('about-us')}}">Hadiah</a></li>
                    <li {{($menu=='howto')?'class="active"':""}}><a href="{{route('how-to')}}">Cara Ikutan</a></li>
                    <li {{($menu=='inspiration')?'class="active"':""}}><a href="{{route('inspiration')}}">Inspirasi Sarapan Sempurna</a></li>
                    <li {{($menu=='winner')?'class="active"':""}}><a href="{{route('winners')}}">Pemenang</a></li>
                    <li {{($menu=='tnc')?'class="active"':""}}><a href="{{route('tnc')}}">Syarat & Ketentuan</a></li>
                    <li {{($menu=='policy')?'class="active"':""}}><a href="{{route('policy')}}">Kebijakan Privasi</a></li>
                    <li {{($menu=='logout')?'class="active"':""}}><a href="{{route('log-out')}}">Keluar</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <a href="{{route('home')}}"><img src="/frontend/img/logo-frisian-flag.png" class="logoff"></a>
                </ul>
            </div>
        </div>
    </nav>
</div>

<div class="nav-desktop">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('home')}}"><img src="/frontend/img/logo-bulan-sarapan-sempurna.png"></a>
            </div>
        
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li {{($menu=='home')?'class="active"':""}}><a href="{{route('home')}}">Home</a></li>
                    <li {{($menu=='about')?'class="active"':""}}><a href="{{route('about-us')}}">Hadiah</a></li>
                    <li {{($menu=='howto')?'class="active"':""}}><a href="#" data-toggle="modal" data-target="#modalHowTo" onclick="ga('send', 'pageview', '/vp-how-to');">Cara Ikutan</a></li>
                    <li {{($menu=='inspiration')?'class="active"':""}}><a href="{{route('inspiration')}}">Inspirasi Sarapan Sempurna</a></li>
                    <li {{($menu=='winner')?'class="active"':""}}><a href="{{route('winners')}}">Pemenang</a></li>
                    <li {{($menu=='tnc')?'class="active"':""}}><a href="#" data-toggle="modal" data-target="#modaltnc" onclick="ga('send', 'pageview', '/vp-tnc');">Syarat & Ketentuan</a></li>
                    <li {{($menu=='policy')?'class="active"':""}}><a href="#" data-toggle="modal" data-target="#modalpvp" onclick="ga('send', 'pageview', '/vp-pvp');">Kebijakan Privasi</a></li>
                    <li {{($menu=='logout')?'class="active"':""}}><a href="{{route('log-out')}}">Keluar</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <a href="{{route('home')}}"><img src="/frontend/img/logo-frisian-flag.png" class="logoff"></a>
                </ul>
            </div>
        </div>
    </nav>
</div>

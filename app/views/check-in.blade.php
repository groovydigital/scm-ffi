@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 text-center">
                <div class="modal-post-location-body">
                    <br><br>
                    <form method="post" id="geocoding_form">
                        <div class="input">
                            <input type="text" id="address" name="address" class="form-control form-control-location" placeholder="Cari Lokasi..."/>
                            <!-- <input type="submit" class="btn-light-blue" value="Search" /> -->
                        </div>
                    </form>
                    <form>
                        <div class="form-group"><div id="map"></div></div>
                        <input type="hidden" name="long" id="long" value="">
                        <input type="hidden" name="lat" id="lat" value="">
                        <input type="hidden" name="caption-location" id="caption-location" value="">
                        <div class="padding">
                            @if(count($history)>0)
                                <div class="history">
                                    <div class="history-header">
                                        <h5>History</h5>
                                    </div>
                                    <div class="history-body">
                                        @foreach($history as $key => $val)
                                            <?php $detail=\Post::where('is_deleted','=','0')
                                                    ->where('location','=',$val->location)
                                                    ->select('latitude','longitude')
                                                    ->first();
                                            $location=explode(",",$val->location);
                                                ?>
                                            <div class="row">
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="/frontend/img/icon-location.png" class="icon-history"></div>
                                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 detail-history" data-lat="{{$detail['latitude']}}" data-long="{{$detail['longitude']}}">{{$location[0].','.$location[1],$location[2]}}</div>
                                                <br>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            <div class="form-group"><textarea class="form-control" id="text-location" placeholder="Update Status Disini"></textarea></div>
                            <div class="form-group text-center"><button data-dismiss="modal" type="button" id="btn-location" class="btn btn-yellow" >Check In</button></div>
                        </div>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>
<script src="/frontend/js/gmap.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        map = new GMaps({
            el: '#map',
            width: '100%',
            height: '200px',
            zoom: 16,
            lat: -12.043333,
            lng: -77.028333,
            click: function(e) {
                marker(e.latLng.lat(),e.latLng.lng());
            },
        });

        GMaps.geolocate({
            success: function(position){
                map.setCenter(position.coords.latitude, position.coords.longitude);
                marker(position.coords.latitude,position.coords.longitude);
            },
            error: function(error){
                alert('Geolocation failed: '+error.message);
            },
            not_supported: function(){
                alert("Your browser does not support geolocation");
            },
        });

        $('#geocoding_form').submit(function(e){
            e.preventDefault();
            GMaps.geocode({
                address:  $('#address').val(),
                callback: function(results, status){
                    if(status=='OK'){
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        marker(latlng.lat(),latlng.lng());
                    }
                }
            });
        });

        function marker(lat,lng){
            map.removeMarkers();
            map.addMarker({
                lat: lat,
                lng: lng,
                callback : function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        console.log(results[1].formatted_address);
                    }
                }
            });
            $('#long').val(lng);
            $('#lat').val(lat);
            getAddress(lat,lng)
        }

        function getAddress(lat,lng){
            var opt = {
                lat : lat,
                lng : lng,
                callback : function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $('#caption-location').val(results[0].formatted_address);
                    }
                }
            }

            GMaps.geocode(opt);
        }

        $('.detail-history').click(function(){
           var lat=$(this).data('lat');
           var lng=$(this).data('long');
            map.setCenter(lat,lng);
            marker(lat,lng);
        });
    });

    $('#btn-location').click(function(){
        var data={
            caption :$('#text-location').val(),
            location:$('#caption-location').val(),
            latitude:$('#lat').val(),
            longitude:$('#long').val()
        };
        insertLocation(data);
    });

    function insertLocation(data){
        $.ajax({
            type: "POST",
            url: "/post/location",
            data: {
                caption :data.caption,
                location:data.location,
                latitude:data.latitude,
                longitude:data.longitude
            },
            beforeSend: function(){
            },
            error: function(data){
                alert("cannot post location");
            },
            success: function( data )
            {
                $(location).attr('href', "{{url('success-checkin')}}/"+data.status.id);
            }
        });
    }

</script>
@include('footer')
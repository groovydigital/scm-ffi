<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#  article: http://ogp.me/ns/article#">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$page_title}}</title>
    <link rel="icon" href="/frontend/img/favicon.png" type="image/png" sizes="32x32">
    <!-- for Google -->
    <meta name="description" content="Yuk mulai kebiasaan sarapan sempurna bersama keluarga dan menangkan Voucher Belanja, 30 Set Meja Makan, dan Grand Prize Sarapan Sempurna Fantastis!">
    {{--<meta name="author" content="Frisian Flag Indonesia">--}}
    <meta name="keywords" content="Bulan Sarapan Sempurna, bulan sarapan sempurna, bulan sarapan, Bulan Sarapan, Frisian Flag Indonesia">

    <meta property="fb:app_id" content="355740294608174" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route("detail.post",array($data["id"]))}}" />
    <meta property="og:title" content="{{(!empty($data['location']))?$data["location"]:$data["caption"]}}" />
    <meta property="og:image:height"  content="315" />
    <meta property="og:image:width"  content="600" />
    <meta property="og:description" content="Upload #SarapanSempurna keluarga Anda dan menangkan hadiah-hadiah menarik!" />
    <meta property="og:image" content="{{(!empty($data["image"]))?url("images/upload/post/mobile/".$data["image"]):'http://bulansarapansempurna.com/frontend/img/bulan-sarapan-sempurna.jpg'}}" />
    <meta property="og:site_name" content="Bulan Sarapan Sempurna" />
    <meta property="article:author" content="{{url()}}" />
    <meta property="profile:first_name" content="Frisian Flag Indonesia" />

    <link rel="canonical" href="{{route("detail.post",array($data["id"]))}}" />

    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <script src="/frontend/js/html5shiv.min.js"></script>
    <script src="/frontend/js/respond.min.js"></script>
    <style>
        .post-photo-desc { font-size: 0.8em; }
        .post-status-desc { font-size: 0.8em; }
        .post-location-desc { font-size: 0.8em; }
    </style>
</head>

<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home-detail">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                <?php
                $carbon = new \Carbon\Carbon();
                    $stream=\User::find($data['user_id']);
                    $user=$stream['username'];
                ?>

                <br>

                @if($data['type_post']=="photo")
                <div class="post-photo">
                    <img src="/frontend/img/icon-photo.png" class="icon-photo">
                    <img src="/images/upload/post/mobile/{{$data['image']}}" width="100%">

                    <div class="post-photo-desc">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">
                                <img src="{{$stream['image']}}" class="img-circle" style="max-width:40px; max-height: 40px; width: 40px; height:40px;">
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                <p><strong>{{$user}}:</strong> {{$data['caption']}}<br>
                                <i>{{$carbon::createFromFormat('Y-m-d H:i:s', $data['created_at'])->subMinutes(2)->diffForHumans();}}</i></p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                    @if($data['type_post']=="text")
                <div class="post-status">
                    <div class="post-status-desc">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">
                                <img src="/frontend/img/icon-status.png" class="icon-status">
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                <p><strong>{{$user}}:</strong> {{$data['caption']}}  <br>
                                <i>{{$carbon::createFromFormat('Y-m-d H:i:s', $data['created_at'])->subMinutes(2)->diffForHumans();}}</i></p>
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
                    @if($data['type_post']=="location")
                    <?php $location=explode(",",$data['location'])?>
                <div class="post-location">
                    <div class="post-location-desc">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 padding">
                                &nbsp;&nbsp;&nbsp;<img src="/frontend/img/icon-location.png" class="icon-location">
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                <p><strong>{{$user}}:</strong> {{$data['caption']}}</p>
                                <br>
                                <p>in {{$location[0].','.$location[1],$location[2]}}<br>
                                <i>{{$carbon::createFromFormat('Y-m-d H:i:s', $data['created_at'])->subMinutes(2)->diffForHumans();}}</i></p>
                            </div>
                        </div>
                    </div>
                </div>
                    @endif

                <div class="body-share text-center">
                    <br>
                    <p>Ayo share ke Facebook untuk memiliki kesempatan memenangkan set meja makan dan grand prize!</p><br>
                    
                    <button class="btn btn-blue"onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{route("detail.post",array($data["id"]))}}','facebook-share-dialog','width=auto,height=auto');return false;" >Share ke Facebook</button>

                    <br><br>
                    <a href="{{route('home')}}">back to home</a>
                    <br><br>
                    @if(\Session::get('users')['id']==$data["user_id"])
                    <a href="{{route('post-delete',array($data["id"]))}}" onclick="return confirm('Are you sure you want to delete this post?');">Delete</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
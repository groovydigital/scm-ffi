@include('head-meta')
<body>
@include('header')
<div class="home-wave-up"></div>
<div class="container-fluid" id="home">
    <div class="container">
        <div class="row no-gutter">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="prize-header">
                    <h4>Hadiah Utama</h4>
                    <span><h3>SARAPAN DI DARAT, LAUT DAN UDARA</h3></span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="prize-body">
                    <p>Serunya sarapan keluarga ditemani satwa liar, serunya sarapan keluarga di dalam air, atau serunya sarapan di udara? Jadikan pengalaman sarapan keluarga anda fantastis hanya dengan upload foto sarapan keluarga setiap hari. Untuk 3 keluarga terpilih!</p>
                    <div class="prize-img">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <img src="/frontend/img/main-prize-1.png">
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="/frontend/img/main-prize-2.png">
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="/frontend/img/main-prize-3.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row no-gutter">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="prize-header">
                    <h4>Hadiah Mingguan</h4>
                    <span><h3>SATU SET MEJA MAKAN</h3></span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="prize-body">
                    <p>Ingin punya meja makan keren? Bisa! Upload foto sarapan keluarga anda sebanyak-banyaknya dan dapatkan kesempatan memenangkan meja makan keren ini. Ada 30 set meja makan untuk Anda menangkan selama periode kontes!</p>
                    <div class="prize-img">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <img src="/frontend/img/weekly-prize-1.png">
                                <br>
                                <h5>OHIO</h5>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="/frontend/img/weekly-prize-2.png">
                                <br>
                                <h5>PORTLAND</h5>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="/frontend/img/weekly-prize-3.png">
                                <br>
                                <h5>FREMONT</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row no-gutter">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="prize-header">
                    <div class="prize-img">
                        <img src="/frontend/img/daily-prize.png">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="prize-body">
                    <h4>Hadiah Harian</h4>
                    <span><h3>VOUCHER BELANJA</h3></span>
                    <br><p>Upload foto sarapan keluarga Anda dan dapatkan kesempatan memenangkan voucher belanja harian sebesar Rp100.000 untuk 10 orang pemenang harian.</p><br><br>

                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<div class="wave-up-blue-2"></div>
<div class="container-fluid" id="bg-blue-2">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 text-center">
        <div class="footer">
            &copy; Frisian Flag 2016
        </div>
    </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')
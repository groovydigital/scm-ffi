<?php

use Illuminate\Support\Facades\View;

class AboutController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){

        $data=\About::find(1);

        return View::make("about", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Hadiah',
            'menu'          => 'about',
            'data'          => $data,
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }
}
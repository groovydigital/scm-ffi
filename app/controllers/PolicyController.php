<?php

use Illuminate\Support\Facades\View;

class PolicyController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){

        $data=\Privacy::find(1);

        return View::make("policy", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Kebijakan Privasi',
            'menu'          => 'policy',
            'data'          => $data,
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }
}
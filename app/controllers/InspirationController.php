<?php

use Illuminate\Support\Facades\View;

class InspirationController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){
        $page=\Input::get('search');
        $data=\Blog::whereIsDeleted(0)->where('title','like','%'.$page.'%')->orderBy('blog_id','DESC')->paginate(6);

        return View::make("inspiration", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Inspirasi Sarapan Sempurna',
            'menu'          => 'inspiration',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'data'          => $data
        ));
    }
    public function detail($slug){

        $data=\Blog::whereIsDeleted(0)->where('slug','=',$slug)->first();

        return View::make("inspiration-detail", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Inspirasi Sarapan Sempurna',
            'menu'          => 'Inspiration detail',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'data'          => $data
        ));
    }
}
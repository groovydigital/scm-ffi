<?php

use Illuminate\Support\Facades\View;

class HomeController extends \BaseController {

    public function __construct()
    {
        //facebook app
        session_start();
        $this->app_id ='1697230303899844'; // APP ID TEST
        $this->app_secret='dcd450c085eeb91ead96d3e781cfa5b4';
        //$this->app_id ='355740294608174'; // APP ID ASLI
        //$this->app_secret='34989ca87613896a8a1de767b077f891';

        $this->default_graph_version = 'v2.5';
        $this->setupData();
    }

    public function index(){

        $data=\Post::whereIsDeleted(0)->where('user_id','=',\Session::get('users')['id'])->orderBy('id','DESC')->paginate(10);
        $message=\Message::whereIsDeleted(0)->where('user_id','=',\Session::get('users')['id'])->orderBy('inbox_id','DESC')->paginate(10);
        $message_send=\Message::whereIsDeleted(0)->where('user_id','=',\Session::get('users')['id'])->where('status','=','send')->orderBy('inbox_id','DESC')->paginate(10);
        $total=$message_send->toArray()["total"];

        return View::make("home", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Diary Keluarga',
            'menu'          => 'home',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'message'       => $message,
            'total_message' => $total,
            'data'          => $data
        ));
    }

    public function allPost(){

        $data=\Post::join('ffi_users', 'ffi_post.user_id', '=', 'ffi_users.id')
            ->where('ffi_post.is_deleted','=','0')
            ->where('ffi_users.is_deleted','=','0')
            ->orderBy('ffi_post.id','DESC')
            ->select('ffi_post.id as id_post','ffi_post.image as post_image','ffi_post.created_at as date','ffi_post.*','ffi_users.*')
            ->paginate(5);

        return View::make("content", array(
            'data'          => $data
        ));
    }
    public function friendPost(){
        $data_friend=[];
       $friend=json_decode(\Session::get('users')['friendlist']);
       foreach($friend as $key => $val){
           $data_friend[]=$val->id;
       }

        $data=\Post::join('ffi_users', 'ffi_post.user_id', '=', 'ffi_users.id')
            ->where('ffi_post.is_deleted','=','0')
            ->where('ffi_users.is_deleted','=','0')
            ->whereIn('ffi_users.provider', $data_friend)->orderBy('ffi_post.id','DESC')
            ->select('ffi_post.id as id_post','ffi_post.image as post_image','ffi_post.created_at as date','ffi_post.*','ffi_users.*')
            ->paginate(5);

        return View::make("content", array(
            'data'          => $data
        ));
    }
    public function myPost(){
        $data=\Post::join('ffi_users', 'ffi_post.user_id', '=', 'ffi_users.id')
            ->where('ffi_post.is_deleted','=','0')
            ->where('ffi_users.is_deleted','=','0')
            ->where('ffi_post.user_id', \Session::get('users')['id'])->orderBy('ffi_post.id','DESC')
            ->select('ffi_post.id as id_post','ffi_post.image as post_image','ffi_post.created_at as date','ffi_post.*','ffi_users.*')
            ->paginate(5);

        return View::make("content", array(
            'data'          => $data
        ));
    }
    public function detail($id){

        $data=\Post::find($id);

        return View::make("detail", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Detail Post',
            'menu'          => 'detail',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'data'          => $data
        ));
    }
    public function savePhoto()
    {
        if (\File::exists(\Input::file('file'))) {

            try {
                $destinationPath = public_path('/images/upload/post');
                $extension = \Input::file('file')->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . date("Ymd") . 'post.' . $extension;
                \Input::file("file")->move($destinationPath, $fileName);
                $image = \Image::make($destinationPath . '/' . $fileName);
                $image->widen(600);
                $image->save($destinationPath . '/mobile/' . $fileName);

                $params=array(
                    "image"=>$fileName,
                    "type_post"=>"photo",
                    "caption"=>htmlspecialchars(\Input::get("caption")),
                    "user_id"=>\Session::get('users')['id']
                );

                $data["status"]=\Post::saveData($params);
                $data["message"]="Upload successful";
                return \Response::json($data, 200);
            } catch (\Exception $e) {
                return \Response::json($e->getMessage(), 400);
            }
        }else{
            $data["message"]="file doesn't exist";
            return \Response::json($data, 400);
        }
    }

    public function savePhotoMobile()
    {
        try {
            $w=intval(\Input::get('w'));
            $h=intval(\Input::get('h'));
            $x=intval(\Input::get('x'));
            $y=intval(\Input::get('y'));
            $destinationPath = public_path('/images/upload/post');
            $img = Image::make($destinationPath. '/mobile/' .\Input::get("image"));
            $img->rotate(\Input::get('rotate'));
            $img->crop($w, $h, $x, $y);
            $img->save($destinationPath . '/mobile/' . \Input::get("image"));

            $params=array(
                "image"=>\Input::get("image"),
                "type_post"=>"photo",
                "caption"=>htmlspecialchars(\Input::get("caption")),
                "user_id"=>\Session::get('users')['id']
            );

            $data["status"]=\Post::saveData($params);
            $data["message"]="Upload successful";
            return \Response::json($data, 200);
        } catch (\Exception $e) {
            return \Response::json($e->getMessage(), 400);
        }
    }

    public function saveText()
    {
        if (strlen(\Input::get('caption'))) {

            try {
                $params=array(
                    "type_post"=>"text",
                    "caption"=> htmlspecialchars(\Input::get("caption")),
                    "user_id"=>\Session::get('users')['id']
                );

                $data["status"]=\Post::saveData($params);
                $data["message"]="Upload successful";
                return \Response::json($data, 200);
            } catch (\Exception $e) {
                return \Response::json($e->getMessage(), 400);
            }
        }else{
            $data["message"]="Fill the caption";
            return \Response::json($data, 400);
        }
    }

    public function saveLocation()
    {
        if (strlen(\Input::get('location'))) {

            try {
                $params=array(
                    "type_post"=>"location",
                    "caption"=> htmlspecialchars(\Input::get("caption")),
                    "location"=> \Input::get("location"),
                    "latitude"=> \Input::get("latitude"),
                    "longitude"=> \Input::get("longitude"),
                    "user_id"=>\Session::get('users')['id']
                );

                $data["status"]=\Post::saveData($params);
                $data["message"]="Upload successful";
                return \Response::json($data, 200);
            } catch (\Exception $e) {
                return \Response::json($e->getMessage(), 400);
            }
        }else{
            $data["message"]="Fill the caption";
            return \Response::json($data, 400);
        }
    }

    public function login(){
        try{
            $fb = new Facebook\Facebook([
                'app_id' => $this->app_id, // Replace {app-id} with your app id
                'app_secret' => $this->app_secret,
                'default_graph_version' => $this->default_graph_version,

            ]);

            $helper = $fb->getRedirectLoginHelper();

            //$permissions = ['email','user_birthday','user_about_me','public_profile','user_friends','user_relationships','user_education_history','user_hometown','user_relationship_details']; // Optional permissions
            $permissions = ['email','user_about_me','public_profile','user_friends']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(route('login.callback'), $permissions);

            return \Redirect::to($loginUrl);

        }catch (\Exception $e){

            return \Redirect::back()->withErrors("Login failed");
        }
    }

    public function login_callback(){

        $fb = new Facebook\Facebook([
            'app_id' => $this->app_id, // Replace {app-id} with your app id
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->default_graph_version,

        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($this->app_id); // Replace {app-id} with your app id

        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }
        }
        \Session::put('fb_access_token', (string) $accessToken);

        $fb->setDefaultAccessToken($accessToken);

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,education,family,relationship_status,hometown,interested_in,birthday,email,picture.type(normal),gender,first_name,last_name', $accessToken);
            $friends = $fb->get('/me/friends', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();

        $user['friends'] = $friends->getGraphEdge()->asArray();

        $data= \User::where('provider','=',$user['id'])->where('login_enabled','=','0')->where('is_deleted','=','0')->get();

        if(count($data)<1) {
            $params['provider'] = @$user['id'];
            $params['username'] = @$user['name'];
            $params['name'] = @$user['first_name'] . ' ' . @$user['last_name'];
            $params['email'] = @$user['email'];
            $params['gender'] = @$user->getGender();
            //$params['birthdate'] = @$user->getBirthday()->format('Y-m-d');
            $params['birthdate'] = '';
            $params['img_fb'] = @$user['picture']['url'];
            $params['friendlist'] = json_encode($user['friends']);

            \Session::put('register', $params);
            return \Redirect::to("/");
        }else{
            $data=$data->toArray()[0];
            $params['gender'] = @$user->getGender();
            //$params['birthdate'] = @$user->getBirthday()->format('Y-m-d');
            $params['birthdate'] = '';
            $params['friendlist'] = json_encode($user['friends']);
            $data['friendlist'] = json_encode($user['friends']);

            \User::updateData($data['id'], $params);
            \Session::put('users', $data);
            return \Redirect::route("home");
        }
    }

    public function saveUser(){
        try{
            $input=\Input::all();

            $fileName =rand(11111,99999).date("Ymd").'provider.jpg';
            $image ='/images/upload/photo/provider/'.$fileName;
           // \Image::make($input['img_fb'])->save(public_path($image));
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            $response = file_get_contents($input['img_fb'], false, stream_context_create($arrContextOptions));

            file_put_contents(public_path($image), $response);
            $params['provider'] = @$input['provider'];
            $params['username'] = @$input['username'];
            $params['name'] = @$input['name'];
            $params['email'] = @$input['email'];
            $params['gender'] = @$input['gender'];
            //$params['birthdate'] = @$input['birthdate'];
            $params['birthdate'] = '';
            $params['friendlist'] = @$input['friendlist'];
            $params['image']            = @$image;
            $params['provider_avatar']  = @$image;
            $params['phone'] = @$input['phone'];
            $params['spouse'] = @$input['spouse'];
            $params['child'] = @$input['child'];
            $params['shop'] = @$input['shop'];

            $data = \User::saveData($params);

             \Session::put('users', $data);
            return \Response::json($data, 200);

        }catch (\Exception $e){
            $data["message"]="Cannot Register";
            return \Response::json($data, 400);
        }
    }

    public function logout()
    {
        \Session::flush();
        return \Redirect::to("/");
    }

    public function readNotif(){
        try{

            $data=\Message::whereIsDeleted(0)->where('user_id',\Input::get('user_id') )->update(array('status' => 'read'));
            return \Response::json($data, 200);

        }catch (\Exception $e){
            $data["message"]="Cannot Register";
            return \Response::json($data, 400);
        }
    }

    public function register(){

        return View::make("register", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Registrasi',
            'menu'          => 'register',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }

    public function welcome(){

        return View::make("welcome", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Selamat Datang',
            'menu'          => 'welcome',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }
    public function mydiary(){
        $message_send=\Message::whereIsDeleted(0)->where('user_id','=',\Session::get('users')['id'])->where('status','=','send')->orderBy('inbox_id','DESC')->paginate(10);
        $total=$message_send->toArray()["total"];

        return View::make("diary-me", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Diary Saya',
            'menu'          => 'mydiary',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'total_message' => $total
        ));
    }
    public function friends(){

        return View::make("diary-friend", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Diary Teman',
            'menu'          => 'friend',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }

    public function postPhoto(){

        return View::make("post-photo", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Upload Foto',
            'menu'          => 'photo',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }

    public function postStatus(){

        return View::make("post-status", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Update Status',
            'menu'          => 'status',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }

    public function uploadPhoto(){

        return View::make("upload-photo", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Upload Foto',
            'menu'          => 'upload-photo',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }

    public function uploadingPhoto($image){

        return View::make("post-photo-mobile", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Upload Foto',
            'menu'          => 'upload-photo',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'image'         => $image
        ));
    }
    public function checkIn(){
        $history=\Post::where('is_deleted','=','0')
            ->where('user_id','=',\Session::get('users')['id'])
            ->where('type_post','=','location')
            ->select('location')
            ->distinct()
            ->orderBy('ffi_post.id','DESC')
            ->paginate(4);
        return View::make("check-in", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Check In',
            'menu'          => 'check-in',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'history'        =>$history
        ));
    }

    public function successStatus($id){
        $data=\Post::find($id);
        return View::make("success-status", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Update Status Berhasil',
            'menu'          => 'success-status',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'data'          => $data
        ));
    }

    public function successPhoto($id){
        $data=\Post::find($id);
        return View::make("success-photo", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Upload Foto Berhasil',
            'menu'          => 'success-photo',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'data'          => $data
        ));
    }

    public function successCheckin($id){
        $data=\Post::find($id);
        return View::make("success-checkin", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Check In Berhasil',
            'menu'          => 'success-checkin',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'data'          => $data
        ));
    }

    public function message(){
        $message=\Message::whereIsDeleted(0)->where('user_id','=',\Session::get('users')['id'])->orderBy('inbox_id','DESC')->paginate(10);

        return View::make("message", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Pesan',
            'menu'          => 'message',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'message'       => $message
        ));
    }


    //mobile login
    public function loginMobile(){
        try{
            $fb = new Facebook\Facebook([
                'app_id' => $this->app_id, // Replace {app-id} with your app id
                'app_secret' => $this->app_secret,
                'default_graph_version' => $this->default_graph_version,

            ]);

            $helper = $fb->getRedirectLoginHelper();

            //$permissions = ['email','user_birthday','user_about_me','public_profile','user_friends','user_relationships','user_education_history','user_hometown','user_relationship_details']; // Optional permissions
            $permissions = ['email','user_about_me','public_profile','user_friends']; // Optional permissions
            
            $loginUrl = $helper->getLoginUrl(route('login.mobile.callback'), $permissions);

            return \Redirect::to($loginUrl);

        }catch (\Exception $e){

            return \Redirect::back()->withErrors("Login failed");
        }
    }

    public function loginMobile_callback(){

        $fb = new Facebook\Facebook([
            'app_id' => $this->app_id, // Replace {app-id} with your app id
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->default_graph_version,

        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($this->app_id); // Replace {app-id} with your app id

        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }
        }
        \Session::put('fb_access_token', (string) $accessToken);

        $fb->setDefaultAccessToken($accessToken);

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,birthday,email,picture.type(normal),gender,first_name,last_name', $accessToken);
            $friends = $fb->get('/me/friends', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();
        $user['friends'] = $friends->getGraphEdge()->asArray();

        $data= \User::where('provider','=',$user['id'])->where('login_enabled','=','0')->where('is_deleted','=','0')->get();

        if(count($data)<1) {
            $params['provider'] = @$user['id'];
            $params['username'] = @$user['name'];
            $params['name'] = @$user['first_name'] . ' ' . @$user['last_name'];
            $params['email'] = @$user['email'];
            $params['gender'] = @$user->getGender();
            //$params['birthdate'] = @$user->getBirthday()->format('Y-m-d');
            $params['birthdate'] = '';
            $params['img_fb'] = @$user['picture']['url'];
            $params['friendlist'] = json_encode($user['friends']);

            \Session::put('register', $params);
            return \Redirect::route("register");
        }else{
            $data=$data->toArray()[0];
            $params['gender'] = @$user->getGender();
            //$params['birthdate'] = @$user->getBirthday()->format('Y-m-d');
            $params['birthdate'] = '';
            $params['friendlist'] = json_encode($user['friends']);
            $data['friendlist'] = json_encode($user['friends']);

            \User::updateData($data['id'], $params);
            \Session::put('users', $data);
            return \Redirect::route("home");
        }
    }

    public function saveUserMobile(){
        try{
            \Session::flush();
            $input=\Input::all();

            $fileName =rand(11111,99999).date("Ymd").'provider.jpg';
            $image ='/images/upload/photo/provider/'.$fileName;
            //\Image::make($input['img_fb'])->save(public_path($image));
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            $response = file_get_contents($input['img_fb'], false, stream_context_create($arrContextOptions));

            file_put_contents(public_path($image), $response);
            //\Image::make('https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xft1/v/t1.0-1/p100x100/1497794_581398908598785_1716899176_n.jpg?oh=d5bece921fc17e60b9ed6e30681b9872&oe=576FC365&__gda__=1466083240_b65220ee70662e3211bd0717c5039867')->save(public_path($image));


            $params['provider'] = @$input['provider'];
            $params['username'] = @$input['username'];
            $params['name'] = @$input['name'];
            $params['email'] = @$input['email'];
            $params['gender'] = @$input['gender'];
            // $params['birthdate'] = @$input['birthdate'];
            $params['birthdate'] = '';
            $params['friendlist'] = @$input['friendlist'];
            $params['image']            = @$image;
            $params['provider_avatar']  = @$image;
            $params['phone'] = @$input['phone'];
            $params['spouse'] = @$input['spouse'];
            $params['child'] = @$input['child'];
            $params['shop'] = @$input['shop'];

            $data = \User::saveData($params);

            \Session::put('users', $data);
            return \Response::json($data, 200);

        }catch (\Exception $e){
            $data["message"]="Cannot Register";
            return \Response::json($data, 400);
        }
    }

    public function uploadApi()
    {
        if (\File::exists(\Input::file('file'))) {

            try {
                $destinationPath = public_path('/images/upload/post');
                $extension = \Input::file('file')->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . date("Ymd") . 'post.' . $extension;
                \Input::file("file")->move($destinationPath, $fileName);
                $image = \Image::make($destinationPath . '/' . $fileName);
                $image->widen(600);
                $image->save($destinationPath . '/mobile/' . $fileName);

                $data=array(
                    "image"=>$fileName,
                    "message"=>"Upload successful"
                );

                return \Response::json($data, 200);
            } catch (\Exception $e) {
                return \Response::json($e->getMessage(), 400);
            }
        }else{
            $data["message"]="file doesn't exist";
            return \Response::json($data, 400);
        }
    }

    public function deletePost($id){

        $delete=\Post::where('id', $id)
            ->where('user_id','=', \Session::get('users')['id'])
            ->update(array('is_deleted'=>1));

        if($delete) {
            return \Redirect::to('/')->with('message', 'Post Deleted');
        }else{
            return \Redirect::to('/')->withError('message', 'Cannot Deleted');
        }
    }
}
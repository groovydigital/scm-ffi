<?php

use Illuminate\Support\Facades\View;

class TeaserController extends \BaseController {

    public function __construct()
    {
        session_start();
        $this->setupData();
    }

    public function teaser(){

        return View::make("teaser", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'menu'          => 'teaser'
        ));
    }

    public function saveFb(){
        try{
            $fb = new Facebook\Facebook([
                'app_id' => '1697230303899844', // Replace {app-id} with your app id
                'app_secret' => 'dcd450c085eeb91ead96d3e781cfa5b4',
                'default_graph_version' => 'v2.5',
            ]);

            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email','public_profile','user_friends']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(route('teaser.callback'), $permissions);

            return \Redirect::to($loginUrl);

        }catch (\Exception $e){

            return \Redirect::back()->withErrors("Login failed");
        }
    }

    public function teaser_callback(){

        $fb = new Facebook\Facebook([
            'app_id' => '1697230303899844', // Replace {app-id} with your app id
            'app_secret' => 'dcd450c085eeb91ead96d3e781cfa5b4',
            'default_graph_version' => 'v2.5',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('355740294608174'); // Replace {app-id} with your app id

        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }
        }
        \Session::put('fb_access_token', (string) $accessToken);

        $fb->setDefaultAccessToken($accessToken);

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,birthday,email,picture.type(normal),gender,first_name,last_name,friends', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();


        try{
            $data= \User::where('provider','=',$user['id'])->get();

            if(count($data)<1){

                $fileName =rand(11111,99999).date("Ymd").'provider.jpg';
                $image ='/images/upload/photo/provider/'.$fileName;
                \Image::make($user['picture']['url'])->save(public_path($image));

                $params['provider']         = @$user['id'];
                $params['username']         = @$user['name'];
                $params['name']             = @$user['first_name'].' '.@$user['last_name'];
                $params['email']            = @$user['email'];
                $params['image']            = @$image;
                $params['provider_avatar']  = @$image;
                $params['gender']           =  @$user['gender'];
                $params['birthdate']        = @$user['birthday'];
                $data = \User::saveData($params);

            }else{
                $data=$data->toArray()[0];
            }
            \Session::put('users', $data);
            return \Redirect::route("teaser");

        }catch (\Exception $e){
            return \Redirect::to("teaser");
        }
    }
}
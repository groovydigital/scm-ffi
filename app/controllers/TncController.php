<?php

use Illuminate\Support\Facades\View;

class TncController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){

        $data=\Tnc::find(1);

        return View::make("tnc", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Syarat & Ketentuan',
            'menu'          => 'tnc',
            'data'          => $data,
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }
}
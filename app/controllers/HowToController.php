<?php

use Illuminate\Support\Facades\View;

class HowToController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){

        $data=\Howto::find(1);

        return View::make("how-to", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Cara Ikutan',
            'menu'          => 'how-to',
            'data'          => $data,
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1)
        ));
    }
}
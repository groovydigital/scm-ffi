<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	var $data = null;
	 public function setupData()
    {
        $this->data['page_title'] = "Untitled";
        $this->data['site_name'] = "";

    }


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

    public function getIndex(){
        if (\Session::has('users'))
        {
            return Redirect::to('/home');
        }

        return View::make("index", array(
            'page_title'        => 'Bulan Sarapan Sempurna',
            'menu'              => "login",
        ));
    }

    public function setPageTitle($pageTitle){
        $this->data['page_title'] = $pageTitle;
    }

    public function setPageMenu($menu){
        if(empty($menu)){
            $this->data['menu'] = 'dashboard';
        }else{
            $this->data['menu'] = $menu;
        }
    }

    public function setSiteName($siteName){
        $this->data['site_name'] = $siteName;
    }

    public function getData(){
        return $this->data;
    }
}

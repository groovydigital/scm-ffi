<?php

use Illuminate\Support\Facades\View;

class WinnerController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){
        $category=\CategoryWinner::whereIsDeleted(0)->orderBy('winner_category_id','DESC')->paginate(10);

        return View::make("winner", array(
            'page_title'    => 'Bulan Sarapan Sempurna',
            'title'         => 'Pemenang',
            'menu'          => 'winner',
            'howto'         => \Howto::find(1),
            'tnc'           => \Tnc::find(1),
            'privacy'       => \Privacy::find(1),
            'category'      => $category
        ));
    }
}
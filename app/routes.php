<?php
Route::get('/', [
    'uses' => 'BaseController@getIndex'
]);
Route::get('/teaser', [
    'as'=>'teaser',
    'uses' => 'TeaserController@teaser'
]);
Route::any('/teaser/save', [
    'as'=>'teaser.save',
    'uses' => 'TeaserController@saveFb'
]);
Route::any('/teaser/callback', [
    'as'=>'teaser.callback',
    'uses' => 'TeaserController@teaser_callback'
]);

Route::any('/login/save', [
    'as'=>'login.save',
    'uses' => 'HomeController@login'
]);
Route::any('/login/callback', [
    'as'=>'login.callback',
    'uses' => 'HomeController@login_callback'
]);
Route::post('/login/register/user', [
    'as'=>'login.register.user',
    'uses' => 'HomeController@saveUser'
]);

Route::any('/login/mobile', [
    'as'=>'login.mobile',
    'uses' => 'HomeController@loginMobile'
]);
Route::any('/login/mobile/callback', [
    'as'=>'login.mobile.callback',
    'uses' => 'HomeController@loginMobile_callback'
]);

Route::group(array('prefix' => '/', 'before' => 'register'), function () {
    Route::any('/register', [
        'as' => 'register',
        'uses' => 'HomeController@register'
    ]);
});
Route::any('/register/save', [
    'as' => 'register.save',
    'uses' => 'HomeController@saveUserMobile'
]);
Route::any('/detail/{id_post}', [
    'as' => 'detail.post',
    'uses' => 'HomeController@detail'
]);
Route::group(array('prefix' => '/', 'before' => 'login'), function () {
    Route::get('/welcome', [
        'as' => 'welcome',
        'uses' => 'HomeController@welcome'
    ]);
    Route::get('/home', [
        'as' => 'home',
        'uses' => 'HomeController@index'
    ]);
    Route::get('/my/diary', [
        'as' => 'mydiary',
        'uses' => 'HomeController@mydiary'
    ]);
    Route::get('/friends/diary', [
        'as' => 'friends',
        'uses' => 'HomeController@friends'
    ]);
    Route::get('/post-photo', [
        'as' => 'post-photo',
        'uses' => 'HomeController@postPhoto'
    ]);
    Route::get('/post-delete/{id}', [
        'as' => 'post-delete',
        'uses' => 'HomeController@deletePost'
    ]);
    Route::get('/upload-photo', [
        'as' => 'upload-photo',
        'uses' => 'HomeController@uploadPhoto'
    ]);
    Route::post('/upload-api', [
        'as' => 'upload-api',
        'uses' => 'HomeController@uploadApi'
    ]);
    Route::get('/uploading-photo/{image}', [
        'as' => 'uploading-photo',
        'uses' => 'HomeController@uploadingPhoto'
    ]);
    Route::get('/check-in', [
        'as' => 'check-in',
        'uses' => 'HomeController@checkIn'
    ]);
    Route::get('/post-status', [
        'as' => 'post-status',
        'uses' => 'HomeController@postStatus'
    ]);
    Route::get('/success-status/{id}', [
        'as' => 'success-status',
        'uses' => 'HomeController@successStatus'
    ]);
    Route::get('/success-photo/{id}', [
        'as' => 'success-photo',
        'uses' => 'HomeController@successPhoto'
    ]);
    Route::get('/success-checkin/{id}', [
        'as' => 'success-checkin',
        'uses' => 'HomeController@successCheckin'
    ]);
    Route::get('/messages', [
        'as' => 'messages',
        'uses' => 'HomeController@message'
    ]);
    Route::get('/share', [
        'as' => 'share',
        'uses' => 'HomeController@share'
    ]);
    Route::get('/hadiah', [
        'as' => 'about-us',
        'uses' => 'AboutController@index'
    ]);
    Route::get('/how-to', [
        'as' => 'how-to',
        'uses' => 'HowToController@index'
    ]);
    Route::get('/inspirasi', [
        'as' => 'inspiration',
        'uses' => 'InspirationController@index'
    ]);
    Route::get('/inspirasi/{title}', [
        'as' => 'inspiration.detail',
        'uses' => 'InspirationController@detail'
    ]);
    Route::get('/winners', [
        'as' => 'winners',
        'uses' => 'WinnerController@index'
    ]);
    Route::get('/tnc', [
        'as' => 'tnc',
        'uses' => 'TncController@index'
    ]);
    Route::get('/policy', [
        'as' => 'policy',
        'uses' => 'PolicyController@index'
    ]);

    Route::post('/post/photo', [
        'as' => 'save.photo',
        'uses' => 'HomeController@savePhoto'
    ]);
    Route::post('/post/photo/mobile', [
        'as' => 'save.photo.mobile',
        'uses' => 'HomeController@savePhotoMobile'
    ]);
    Route::post('/post/text', [
        'as' => 'save.text',
        'uses' => 'HomeController@saveText'
    ]);
    Route::post('/post/location', [
        'as' => 'save.location',
        'uses' => 'HomeController@saveLocation'
    ]);
    Route::post('/message/notif/read', [
        'as' => 'message.notif.read',
        'uses' => 'HomeController@readNotif'
    ]);
    Route::any('/post/all', [
        'as' => 'post.all',
        'uses' => 'HomeController@allPost'
    ]);
    Route::any('/post/friend', [
        'as' => 'post.friend',
        'uses' => 'HomeController@friendPost'
    ]);
    Route::any('/post/me', [
        'as' => 'post.me',
        'uses' => 'HomeController@myPost'
    ]);
    Route::any('/log-out', [
        'as' => 'log-out',
        'uses' => 'HomeController@logout'
    ]);
});
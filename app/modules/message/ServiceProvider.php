<?php
namespace App\Modules\Message;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('message');
    }

    public function boot()
    {
        parent::boot('message');
    }

}
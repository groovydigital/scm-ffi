@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="<?php echo asset('css/font-awesome-4.2.0/css/font-awesome.min.css'); ?>">
@stop

@section("content")
    <div class="row">

        <div class="col-lg-12">
            <div class="panel panel-default col-sm-12">
                {{
                    Form::open(array(
                        'url'=> $status=='add'?route('message.store'):route('message.update',$data->inbox_id),
                        'class' => 'form-horizontal form-bordered',
                        'id'=>'form',
                        'files' => true
                    ))
                }}
                @if($errors->has())

                    <div class="alert alert-danger fade in mt10">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4 class="semibold">Oh snap! You got an error!</h4>
                        <p class="mb10">
                            {{ HTML::ul($errors->all()) }}
                        </p>
                    </div>

                @endif

                <div class="form-group">
                    <label class="control-label col-sm-2">To : </label>
                    <div class="col-sm-8">
                        @if($status=="edit")
                            <input type="text" class="form-control" name="user" readonly value="{{$user->name}}" />
                            <input type="hidden" class="form-control" name="user_id" value="{{$data->user_id}}" />
                        @else
                            <select name="user[]" id="selectize-user" class="form-control" multiple></select>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Message : </label>
                    <div class="col-sm-8">
                        <textarea rows="15" name="message" placeholder="Insert Message" class="form-control" required data-parsley-required> {{@$data->message}} </textarea>
                    </div>
                </div>

                <div class="panel-footer">
                    <a href="{{route('message');}}" class="btn btn-sm btn-default"><icon class="ico-arrow-left"></icon> Back</a>
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>

                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section("javascript_footer")
    <script type="application/javascript">
        $('#selectize-user').selectize({
            delimiter: ',',
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            options: [],
            loadThrottle: 600,
            create: false,
            render: {
                option: function(item, escape) {
                    return '<div>' +escape(item.name)+'</div>';
                }
            },
            load: function(query, callback) {

                var url="/users/search";
                if (!query.length) return callback();
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        name: query
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res);

                    }
                });
            }
        });
    </script>
@stop
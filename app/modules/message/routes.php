<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "message", [
        "as" => "message",
        "uses" => 'App\Modules\Message\Controllers\MessageController@home'
    ]);
    Route::any( "message/add", [
        "as" => "message.add",
        "uses" => 'App\Modules\Message\Controllers\MessageController@add'
    ]);
    Route::any( "message/store", [
        "as" => "message.store",
        "uses" => 'App\Modules\Message\Controllers\MessageController@save'
    ]);
    Route::any( "message/edit/{id}", [
        "as" => "message.edit",
        "uses" => 'App\Modules\Message\Controllers\MessageController@edit'
    ]);
    Route::any( "message/update/{id}", [
        "as" => "message.update",
        "uses" => 'App\Modules\Message\Controllers\MessageController@update'
    ]);
    Route::any( "message/delete/{id}", [
        "as" => "message.delete",
        "uses" => 'App\Modules\Message\Controllers\MessageController@delete'
    ]);
    Route::any( "message/export", [
        "as" => "message.export",
        "uses" => 'App\Modules\Message\Controllers\MessageController@export'
    ]);
});
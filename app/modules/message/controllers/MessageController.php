<?php
namespace App\Modules\Message\Controllers;

use Illuminate\Support\Facades\View;


class MessageController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\Message::whereIsDeleted(0)->orderBy('inbox_id','DESC')->paginate(10);

        return View::make("message::message", array(
            'page_title'    => 'Message Management',
            'menu'          => "message",
            'data'          => $data,
        ));
    }
    public function add(){
        return View::make("message::add", array(
            'page_title'    => 'Add Message',
            'menu'          => "message",
            'status'        => "add"
        ));
    }

    public function save(){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();

                try{

                    foreach($input["user"] as $val){
                        $data=array(
                            "user_id"      =>  $val,
                            "message"      =>  $input["message"],
                            "status"       => "send"
                        );

                        $save=\Message::saveData($data);

                    }
                    return \Redirect::to('message')->withMessage('Message Send');
                }catch (\Exception $e){
                    return \Redirect::to('message')->withError('Cannot Send');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    public function delete($id){

        $delete=\Message::dropData($id);
        if($delete) {
            return \Redirect::to('message')->withMessage('Message Deleted');
        }else{
            return \Redirect::to('message')->withError('Cannot Deleted');
        }
    }

    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "user" => "required",
            "message" => "required",
        ]);
    }

    public function edit($id){
        $data=\Message::find($id);

        return View::make("message::add", array(
            'page_title'    => 'Add Message',
            'menu'          => "message",
            'status'        => "edit",
            'data'          => $data,
            'user'          => \User::find($data->user_id)
        ));
    }

    protected function getEditValidator()
    {
        return \Validator::make(\Input::all(), [
            "message" => "required",
        ]);
    }

    public function update($id){
        if ($this->isPostRequest()) {
            $validator = $this->getEditValidator();
            if ($validator->passes()) {

                $input = \Input::all();

                try{

                        $data=array(
                            "user_id"      =>  $input["user_id"],
                            "message"      =>  $input["message"]
                        );

                        $save=\Message::updateData($id,$data);

                    return \Redirect::to('message')->withMessage('Message Send');
                }catch (\Exception $e){

                    return \Redirect::to('message')->withError('Cannot Send');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    public function export(){
        $data=\Message::whereIsDeleted(0)->orderBy('inbox_id','DESC')->get();

        try{
            \Excel::create('message', function($excel) use($data) {

                $excel->sheet('Data Message', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xls');
        }catch (\Exception $e){
            return \Response::json($e->getMessage(), 400)->header('Access-Control-Allow-Origin','*');
        }
    }
}
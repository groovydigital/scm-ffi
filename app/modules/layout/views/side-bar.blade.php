<aside class="sidebar sidebar-left sidebar-menu">
    <!-- START Sidebar Content -->
    <section class="content slimscroll">
        <h5 class="heading">Main Menu</h5>
        <!-- START Template Navigation/Menu -->
        <ul class="topmenu topmenu-responsive" data-toggle="menu">
            <li class="{{$menu=='home'?'active open':''}}">
                <a href="{{route('dashboard')}}">
                    <span class="figure"><i class="ico-home2"></i></span>
                    <span class="text">Dashboard</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="user">
            <li class="{{$menu=='user'?'active open':''}}">
                <a href="{{route('users')}}">
                    <span class="figure"><i class="ico-people"></i></span>
                    <span class="text">User</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="post">
            <li class="{{$menu=='post'?'active open':''}}">
                <a href="{{route('post')}}">
                    <span class="figure"><i class="ico-megaphone"></i></span>
                    <span class="text">Post Management</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="message">
            <li class="{{$menu=='message'?'active open':''}}">
                <a href="{{route('message')}}">
                    <span class="figure"><i class="ico-mail"></i></span>
                    <span class="text">Message Management</span>
                </a>
            </li>
        </ul>

        <ul class="topmenu topmenu-responsive" data-toggle="about">
            <li class="{{$menu=='about'?'active open':''}}">
                <a href="{{route('about')}}">
                    <span class="figure"><i class="ico-accessibility"></i></span>
                    <span class="text">About Page</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="howto">
            <li class="{{$menu=='howto'?'active open':''}}">
                <a href="{{route('howto')}}">
                    <span class="figure"><i class="ico-info"></i></span>
                    <span class="text">How To Page</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="blog">
            <li class="{{$menu=='blog'?'active open':''}}">
                <a href="{{route('blog')}}">
                    <span class="figure"><i class="ico-star"></i></span>
                    <span class="text">Inspiration Page</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="winner">
            <li class="{{$menu=='winner'?'active open':''}}">
                <a href="{{route('winner')}}">
                    <span class="figure"><i class="ico-medal"></i></span>
                    <span class="text">Winner Page</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="tnc">
            <li class="{{$menu=='tnc'?'active open':''}}">
                <a href="{{route('tnc')}}">
                    <span class="figure"><i class="ico-library"></i></span>
                    <span class="text">Term & Condition Page</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="privacy">
            <li class="{{$menu=='privacy'?'active open':''}}">
                <a href="{{route('privacy')}}">
                    <span class="figure"><i class="ico-lock3"></i></span>
                    <span class="text">Privacy Page</span>
                </a>
            </li>
        </ul>
        <!--/ END Template Navigation/Menu -->
    </section>
    <!--/ END Sidebar Container -->
</aside>
<!-- Plugins stylesheet : optional -->
@yield('stylesheet_header')
<!-- Application stylesheet : mandatory -->

<link rel="stylesheet" href="/stylesheet/layout.min.css">
<link rel="stylesheet" href="/stylesheet/uielement.min.css">
<link rel="stylesheet" href="/plugins/selectize/css/selectize.min.css">
<link rel="stylesheet" href="/plugins/jqueryui/css/jquery-ui.min.css">
<link rel="stylesheet" href="/library/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo asset('css/font-awesome-4.2.0/css/font-awesome.min.css'); ?>">
<!--/ Application stylesheet -->
<style>

    #message-bar{
        background: rgb(222, 255, 222);
        margin-top: -15px;
        padding: 10px;
        text-align: center;
        margin-bottom: 15px;
        margin-left: -15px;
        margin-right: -15px;
        color: green;
        font-size: 16px;
    }
    #message-bar.error_con {
        background: rgb(255, 190, 190);
        color: red;
    }
</style>
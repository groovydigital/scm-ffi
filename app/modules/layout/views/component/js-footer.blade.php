@yield('javascript_footer')
<!-- Library script : mandatory -->
<script type="text/javascript" src="/library/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/library/core/js/core.min.js"></script>
<!--/ Library script -->

<!-- App and page level script -->
<script type="text/javascript" src="/plugins/sparkline/js/jquery.sparkline.min.js"></script><!-- will be use globaly as a summary on sidebar menu -->
<script type="text/javascript" src="/javascript/app.min.js"></script>



<head>
    <!-- START META SECTION -->
    @include("layout::component.meta")
    <!--/ END META SECTION -->

    <!-- START STYLESHEETS -->
    @include("layout::component.stylesheet")
    <!-- END STYLESHEETS -->

    <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
    @include("layout::component.js-header")
    <!--/ END JAVASCRIPT SECTION -->
</head>
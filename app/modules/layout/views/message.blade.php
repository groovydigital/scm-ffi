@if(\Session::get('message'))
    <div id="message-bar">
        {{ \Session::get('message') }}
    </div>
@endif

@if(\Session::get('error_message'))
    <div id="message-bar" class="error_con">
        {{ \Session::get('error_message') }}
    </div>
@endif


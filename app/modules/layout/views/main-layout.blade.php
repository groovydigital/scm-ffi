
<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    @include("layout::head")
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Header -->
        @include("layout::header")
        <!--/ END Template Header -->

        <!-- START Template Sidebar (Left) -->
        @include("layout::side-bar")
        <!--/ END Template Sidebar (Left) -->

        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <div class="container-fluid">
                @include("layout::message")
                <!-- Page Header -->
                <div class="page-header page-header-block">
                    <div class="page-header-section">
                        <h4 class="title semibold">{{$page_title}}</h4>
                    </div>
                    <div class="page-header-section">
                    </div>
                </div>

                <!-- Page Header -->
                @yield('content')
            </div>
            <!--/ END Template Container -->

            <!-- START To Top Scroller -->
            <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
            <!--/ END To Top Scroller -->

        </section>
        <!--/ END Template Main -->

        @include("layout::component.js-footer")

        <!--/ END JAVASCRIPT SECTION -->
    </body>
</body>
@extends("layout::footer")
</html>
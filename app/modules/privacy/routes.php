<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "privacy", [
        "as" => "privacy",
        "uses" => 'App\Modules\Privacy\Controllers\PrivacyController@home'
    ]);
    Route::any( "privacy/save/{id}", [
        "as" => "privacy.save",
        "uses" => 'App\Modules\Privacy\Controllers\PrivacyController@save'
    ]);
});
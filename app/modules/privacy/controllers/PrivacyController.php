<?php
namespace App\Modules\Privacy\Controllers;

use Illuminate\Support\Facades\View;


class PrivacyController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\Privacy::find(1);

        return View::make("privacy::privacy", array(
            'page_title'    => 'Privacy Page',
            'menu'          => "privacy",
            'data'          => $data
        ));
    }

    public function save($id){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();

                $data=array(
                    "title"             =>  $input["title"],
                    "text"              =>  htmlspecialchars($input["text"])
                );

                $save=\Privacy::updateData($id,$data);
                if($save) {
                    return \Redirect::to('privacy')->with('message', 'Privacy page was update');
                }else{
                    return \Redirect::to('privacy')->withError('message', 'Cannot Saved');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "title" => "required",
            "text" => "required",
        ]);
    }
}
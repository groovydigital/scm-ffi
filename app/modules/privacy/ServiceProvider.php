<?php
namespace App\Modules\Privacy;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('privacy');
    }

    public function boot()
    {
        parent::boot('privacy');
    }

}
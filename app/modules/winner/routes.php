<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "winner", [
        "as" => "winner",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@home'
    ]);
    Route::any( "winner/add", [
        "as" => "winner.add",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@add'
    ]);
    Route::any( "winner/save", [
        "as" => "winner.save",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@save'
    ]);
    Route::any( "winner/edit/{id}", [
        "as" => "winner.edit",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@edit'
    ]);
    Route::any( "winner/update/{id}", [
        "as" => "winner.update",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@update'
    ]);
    Route::any( "winner/delete/{id}", [
        "as" => "winner.delete",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@delete'
    ]);
    Route::any( "winner/active/{id}", [
        "as" => "winner.active",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@active'
    ]);
    Route::any( "winner/inactive/{id}", [
        "as" => "winner.inactive",
        "uses" => 'App\Modules\Winner\Controllers\WinnerController@inactive'
    ]);

    //detail
    Route::any( "winner/detail/{id_category}", [
        "as" => "winner.detail",
        "uses" => 'App\Modules\Winner\Controllers\WinnerDetailController@detail'
    ])->where(array('id_category' => '[0-9]+'));
    Route::any( "winner/detail/add/{id_category}", [
        "as" => "winner.detail.add",
        "uses" => 'App\Modules\Winner\Controllers\WinnerDetailController@add'
    ])->where(array('id_category' => '[0-9]+'));
    Route::any( "winner/detail/save/{id_category}", [
        "as" => "winner.detail.save",
        "uses" => 'App\Modules\Winner\Controllers\WinnerDetailController@save'
    ])->where(array('id_category' => '[0-9]+'));
    Route::any( "winner/detail/delete/{id_category}/{id}", [
        "as" => "winner.detail.delete",
        "uses" => 'App\Modules\Winner\Controllers\WinnerDetailController@delete'
    ]);
    Route::any( "winner/detail/export/{id_category}", [
        "as" => "winner.detail.export",
        "uses" => 'App\Modules\Winner\Controllers\WinnerDetailController@export'
    ])->where(array('id_category' => '[0-9]+'));
});
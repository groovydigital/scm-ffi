<?php
namespace App\Modules\Winner;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('winner');
    }

    public function boot()
    {
        parent::boot('winner');
    }

}
<?php
namespace App\Modules\Winner\Controllers;

use Illuminate\Support\Facades\View;


class WinnerDetailController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function detail($id_category_winner){
        $data=\Winner::leftjoin('ffi_users', 'ffi_winners.user_id', '=', 'ffi_users.id')
            ->where('ffi_winners.is_deleted','=','0')
            ->where('ffi_winners.winner_category_id','=',$id_category_winner)
            ->orderBy('ffi_winners.winner_id','DESC')
            ->paginate(10);

        return View::make("winner::winner-detail", array(
            'page_title'    => 'Winner User',
            'menu'          => "winner",
            'data'          => $data,
            "id_category_winner"=>$id_category_winner
        ));
    }
    public function add($id_category_winner){
        return View::make("winner::winner-detail-add", array(
            'page_title'    => 'Add Winner',
            'menu'          => "winner",
            'id_category_winner'        => $id_category_winner
        ));
    }

    public function save($id_category_winner){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();
                $data=array(
                    "user_id"             =>  $input["user"],
                    "ket_date"             =>  $input["ket_date"],
                    "id_post"             =>  $input["id_post"],
                    "winner_category_id" => $id_category_winner
                );

                $save=\Winner::saveData($data);
                if($save) {
                    return \Redirect::route('winner.detail',$id_category_winner)->with('message', 'User Winner added');
                }else{
                    return \Redirect::route('winner.detail',$id_category_winner)->withError('message', 'Cannot added');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    public function delete($id_category_winner,$id){

        $delete=\Winner::deleteData($id);
        if($delete) {
            return \Redirect::route('winner.detail',$id_category_winner)->with('message', 'User Winner Deleted');
        }else{
            return \Redirect::route('winner.detail',$id_category_winner)->withError('message', 'Cannot Deleted');
        }
    }


    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "user" => "required",
            "ket_date" => "required",
            "id_post" =>  "required"
        ]);
    }

    protected function getEditValidator()
    {
        return \Validator::make(\Input::all(), [
            "user" => "required",
            "ket_date" => "required",
            "id_post" =>  "required"
        ]);
    }

    public function export($id_category_winner){
        $data=\Winner::leftjoin('ffi_users', 'ffi_winners.user_id', '=', 'ffi_users.id')
            ->where('ffi_winners.is_deleted','=','0')
            ->where('ffi_winners.winner_category_id','=',$id_category_winner)
            ->orderBy('ffi_winners.winner_id','DESC')
            ->get();

        try{
            \Excel::create('winner', function($excel) use($data) {

                $excel->sheet('Data Winner', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xls');
        }catch (\Exception $e){
            return \Response::json($e->getMessage(), 400)->header('Access-Control-Allow-Origin','*');
        }
    }

}
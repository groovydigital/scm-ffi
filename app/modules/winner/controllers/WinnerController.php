<?php
namespace App\Modules\Winner\Controllers;

use Illuminate\Support\Facades\View;


class WinnerController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\CategoryWinner::whereIsDeleted(0)->orderBy('winner_category_id','DESC')->paginate(10);

        return View::make("winner::winner", array(
            'page_title'    => 'Winner Page',
            'menu'          => "winner",
            'data'          => $data
        ));
    }
    public function add(){
        return View::make("winner::add", array(
            'page_title'    => 'Add Winner Category',
            'menu'          => "winner",
            'status'        => 'add'
        ));
    }

    public function save(){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();
                $data=array(
                    "name"             =>  $input["name"]
                );

                $save=\CategoryWinner::saveData($data);
                if($save) {
                    return \Redirect::to('winner')->with('message', 'Winner Category Saved');
                }else{
                    return \Redirect::to('winner')->withError('message', 'Cannot Saved');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    public function edit($id = null){

        return View::make("winner::add", array(
            'page_title'    => 'Edit Winner Category',
            'status'        => 'edit',
            'menu'          => 'winner',
            'data'          => \CategoryWinner::find($id)
        ));
    }

    public function update($id){
        if ($this->isPostRequest()) {
            $validator = $this->getEditValidator();
            if ($validator->passes()) {

                $input = \Input::all();

                $data=array(
                    "name"             =>  $input["name"]
                );


                $save=\CategoryWinner::updateData($id,$data);
                if($save) {
                    return \Redirect::to('winner')->with('message', 'Winner Category Edited');
                }else{
                    return \Redirect::to('winner')->withError('message', 'Cannot Edited');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    public function delete($id){

        $delete=\CategoryWinner::deleteData($id);
        if($delete) {
            return \Redirect::to('winner')->with('message', 'Category Winner Deleted');
        }else{
            return \Redirect::to('winner')->withError('message', 'Cannot Deleted');
        }
    }

    public function active($id){

        $delete=\CategoryWinner::activeData($id);
        if($delete) {
            return \Redirect::to('winner')->with('message', 'Category Winner Actived');
        }else{
            return \Redirect::to('winner')->withError('message', 'Cannot Deleted');
        }
    }

    public function inactive($id){

        $delete=\CategoryWinner::inactiveData($id);
        if($delete) {
            return \Redirect::to('winner')->with('message', 'Category Winner Inactived');
        }else{
            return \Redirect::to('winner')->withError('message', 'Cannot Deleted');
        }
    }

    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "name" => "required",
        ]);
    }

    protected function getEditValidator()
    {
        return \Validator::make(\Input::all(), [
            "name" => "required",
        ]);
    }

}
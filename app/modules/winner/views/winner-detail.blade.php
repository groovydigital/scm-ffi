@extends("layout::main-layout")
@section("stylesheet_header")
<link rel="stylesheet" href="/plugins/tablesorter/css/tablesorter.css">
@stop

@section("content")

<div class="row">
    <div class="col-md-12">
        <!-- START panel -->
        <div class="panel panel-default" id="toolbar-showcase">
            <!-- panel toolbar wrapper -->
            <div class="panel-toolbar-wrapper pl10 pr10 pt5 pb5">
                <div class="panel-toolbar text-left">
                    <div class="btn-group">
                        <a href="{{route('winner')}}" class="btn btn-sm btn-default" ><i class="ico-arrow-left"></i> Back To Category</a>
                    </div>
                </div>
                <div class="panel-toolbar text-right">
                    <a href="{{route('winner.detail.export',$id_category_winner)}}" class="btn btn-sm" style="background-color: #5cb85c; color: #FFF"><i class="ico-file-excel"></i> Export</a>
                    <a href="{{route('winner.detail.add',$id_category_winner)}}" class="btn btn-sm" style="background-color: #BD3E91; color: #FFF"><i class="ico-user-plus2"></i> New Winner</a>

                </div>
            </div>
            <!--/ panel toolbar wrapper -->

            <!-- panel body with collapse capabale -->

            <table class="table table-bordered table-hover tablesorter">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>User Id</th>
                    <th>Name</th>
                    <th>Account Id</th>
                    <th>Date Winner</th>
                    <th>Id Post</th>
                    <th width="100px">Tools</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach($data as $key => $val)
                <tr>
                    <td>{{$data->toArray()["from"]+$key}}</td>
                    <td>{{$val->user_id}}</td>
                    <td>{{$val->name}}</td>
                    <td>{{$val->provider}}</td>
                    <td>{{$val->ket_date}}</td>
                    <td>{{$val->id_post}}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="{{route("winner.detail.delete",array($id_category_winner,$val->winner_id))}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="ico-trash" title="Delete"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <div class="panel-footer">
                <div class="col-sm-6">
                    <div class=" paging_bootstrap">
                        {{$data->links()}}
                    </div>
                </div>
            </div>
        </div>
        <!--/ panel body with collapse capabale -->
    </div>
</div>
@stop

@section("javascript_header")

@stop

@section("javascript_footer")
<script type="text/javascript" src="/plugins/tablesorter/js/tablesorter.js"></script>
<script type="application/javascript">
    $('table').tablesorter({});
</script>
@stop

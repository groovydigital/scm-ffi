@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="/plugins/tablesorter/css/tablesorter.css">
@stop

@section("content")

    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default" id="toolbar-showcase">
                <!-- panel toolbar wrapper -->
                <div class="panel-toolbar-wrapper pl10 pr10 pt5 pb5">
                    <div class="panel-toolbar text-left">
                        <div class="btn-group">
                        </div>
                    </div>
                    <div class="panel-toolbar text-right">

                        <a href="{{route('winner.add')}}" class="btn btn-sm" style="background-color: #BD3E91; color: #FFF"><i class="ico-cart"></i> New Category</a>

                    </div>
                </div>
                <!--/ panel toolbar wrapper -->

                <!-- panel body with collapse capabale -->

                <table class="table table-bordered table-hover tablesorter">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Category</th>
                        <th>Date</th>
                        <th width="220px">Tools</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($data as $key => $val)
                        <tr>
                            <td>{{$data->toArray()["from"]+$key}}</td>
                            <td>{{$val->name}}</td>
                            <td>{{$val->created_at}}</td>
                            <td class="text-center">
                                <div class="btn-group">

                                    <a href="{{route("winner.edit",$val->winner_category_id)}}" class="btn btn-sm btn-default"><i class="ico-pencil" title="Edit"></i> </a>
                                    @if($val->is_active==0)
                                        <a href="{{route("winner.inactive",$val->winner_category_id)}}" class="btn btn-sm btn-success"><i class="ico-check"></i></a>
                                    @else
                                        <a href="{{route("winner.active",$val->winner_category_id)}}" class="btn btn-sm btn-default"><i class="ico-check"></i></a>
                                    @endif
                                    <a href="{{route("winner.detail",$val->winner_category_id)}}" class="btn btn-sm btn-primary"><i class="ico-list"></i> Winner List</a>
                                    <a href="{{route("winner.delete",$val->winner_category_id)}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="ico-trash" title="Delete"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <div class="col-sm-6">
                        <div class=" paging_bootstrap">
                            {{$data->links()}}
                        </div>
                    </div>
                </div>
            </div>
            <!--/ panel body with collapse capabale -->
        </div>
    </div>
@stop

@section("javascript_header")

@stop

@section("javascript_footer")
    <script type="text/javascript" src="/plugins/tablesorter/js/tablesorter.js"></script>
    <script type="application/javascript">
        $('table').tablesorter({});
    </script>
@stop

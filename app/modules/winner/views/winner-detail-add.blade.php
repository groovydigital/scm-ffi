@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="<?php echo asset('css/font-awesome-4.2.0/css/font-awesome.min.css'); ?>">
@stop

@section("content")
    <div class="row">

        <div class="col-lg-12">
            {{
                Form::open(array(
                    'url'=> route('winner.detail.save',$id_category_winner),
                    'class' => 'form-horizontal form-bordered  panel panel-default',
                    'id'=>'form',
                    'files' => true
                ))
            }}
            @if($errors->has())

                <div class="alert alert-danger fade in mt10">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4 class="semibold">Oh snap! You got an error!</h4>
                    <p class="mb10">
                        {{ HTML::ul($errors->all()) }}
                    </p>
                </div>

            @endif

            <div class="form-group">
                <label class="control-label col-sm-2">User : </label>
                <div class="col-sm-8">
                    <input type="text" id="selectize-user" class="form-control" name="user" value="{{Input::old("user")}}" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Date Winner : </label>
                <div class="col-sm-8">
                    <input type="text"  class="form-control" id="ket_date" name="ket_date" value="{{Input::old("ket_date")}}" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Id post : </label>
                <div class="col-sm-8">
                    <input type="text"  class="form-control" id="id_post" name="id_post" value="{{Input::old("id_post")}}" />
                </div>
            </div>
            <div class="panel-footer">
                <a href="{{route('winner.detail',$id_category_winner);}}" class="btn btn-sm btn-default"><icon class="ico-arrow-left"></icon> Back</a>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>

            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section("javascript_footer")
    <script type="application/javascript">
        $('#selectize-user').selectize({
            delimiter: ',',
            valueField: 'id',
            labelField: 'username',
            searchField: ['username'],
            options: [],
            loadThrottle: 600,
            create: false,
            render: {
                option: function(item, escape) {
                    return '<div>' +escape(item.username)+'</div>';
                }
            },
            load: function(query, callback) {

                var url="/users/search";
                if (!query.length) return callback();
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        name: query
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res);

                    }
                });
            }
        });
    </script>
@stop
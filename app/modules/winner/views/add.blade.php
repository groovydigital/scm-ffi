@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="<?php echo asset('css/font-awesome-4.2.0/css/font-awesome.min.css'); ?>">
@stop

@section("content")
    <div class="row">

        <div class="col-lg-12">
                {{
                    Form::open(array(
                        'url'=> $status=='add'?route('winner.save'):route('winner.update',$data->winner_category_id),
                        'class' => 'form-horizontal form-bordered  panel panel-default',
                        'id'=>'form',
                        'files' => true
                    ))
                }}
                @if($errors->has())

                    <div class="alert alert-danger fade in mt10">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4 class="semibold">Oh snap! You got an error!</h4>
                        <p class="mb10">
                            {{ HTML::ul($errors->all()) }}
                        </p>
                    </div>

                @endif

                <div class="form-group">
                    <label class="control-label col-sm-2">Category : </label>
                    <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" value="{{isset($data->name)?$data->name:Input::old("name")}}" />
                    </div>
                </div>

                <div class="panel-footer">
                    <a href="{{route('winner');}}" class="btn btn-sm btn-default"><icon class="ico-arrow-left"></icon> Back</a>
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>

                </div>
                {{ Form::close() }}
        </div>
    </div>
@stop

@section("javascript_footer")
@stop
<?php
namespace App\Modules\Post;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('post');
    }

    public function boot()
    {
        parent::boot('post');
    }

}
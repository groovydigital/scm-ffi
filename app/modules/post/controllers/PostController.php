<?php
namespace App\Modules\Post\Controllers;

use Illuminate\Support\Facades\View;


class PostController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\Post::whereIsDeleted(0)->orderBy('id','DESC')->paginate(10);

        return View::make("post::post", array(
            'page_title'    => 'Post Management',
            'menu'          => "post",
            'data'          => $data
        ));
    }
    public function delete($id){

        $delete=\Post::deleteData($id);
        if($delete) {
            return \Redirect::to('post')->with('message', 'Post Deleted');
        }else{
            return \Redirect::to('post')->withError('message', 'Cannot Deleted');
        }
    }

    public function active($id){

        $status=\Post::activeData($id);
        if($status) {
            return \Redirect::to('post')->with('message', 'Post Active');
        }else{
            return \Redirect::to('post')->withError('message', 'Cannot Process');
        }
    }

    public function inactive($id){

        $status=\Post::inactiveData($id);
        if($status) {
            return \Redirect::to('post')->with('message', 'Post Inactive');
        }else{
            return \Redirect::to('post')->withError('message', 'Cannot Process');
        }
    }

    public function export(){
        $data=\Post::whereIsDeleted(0)->orderBy('id','DESC')->get();

        try{
            \Excel::create('post', function($excel) use($data) {

                $excel->sheet('Data Post', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xls');
        }catch (\Exception $e){
            return \Response::json($e->getMessage(), 400)->header('Access-Control-Allow-Origin','*');
        }
    }
}
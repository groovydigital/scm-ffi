<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "post", [
        "as" => "post",
        "uses" => 'App\Modules\Post\Controllers\PostController@home'
    ]);
    Route::any( "post/delete/{id}", [
        "as" => "post.delete",
        "uses" => 'App\Modules\Post\Controllers\PostController@delete'
    ]);
    Route::any( "post/active/{id}", [
        "as" => "post.active",
        "uses" => 'App\Modules\Post\Controllers\PostController@active'
    ]);
    Route::any( "post/inactive/{id}", [
        "as" => "post.inactive",
        "uses" => 'App\Modules\Post\Controllers\PostController@inactive'
    ]);
    Route::any( "post/add", [
        "as" => "post.add",
        "uses" => 'App\Modules\Post\Controllers\PostController@add'
    ]);
    Route::any( "post/export", [
        "as" => "post.export",
        "uses" => 'App\Modules\Post\Controllers\PostController@export'
    ]);
});

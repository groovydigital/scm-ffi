<?php
namespace App\Modules\About\Controllers;

use Illuminate\Support\Facades\View;


class AboutController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\About::find(1);

        return View::make("about::about", array(
            'page_title'    => 'About Page',
            'menu'          => "about",
            'data'          => $data
        ));
    }

    public function save($id){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();

                $data=array(
                    "title"             =>  $input["title"],
                    "text"              =>  htmlspecialchars($input["text"])
                );

                $save=\About::updateData($id,$data);
                if($save) {
                    return \Redirect::to('about')->with('message', 'About page was update');
                }else{
                    return \Redirect::to('about')->withError('message', 'Cannot Saved');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "title" => "required",
            "text" => "required",
        ]);
    }
}
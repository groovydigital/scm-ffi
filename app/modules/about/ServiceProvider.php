<?php
namespace App\Modules\About;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('about');
    }

    public function boot()
    {
        parent::boot('about');
    }

}
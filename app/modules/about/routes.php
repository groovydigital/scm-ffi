<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "about", [
        "as" => "about",
        "uses" => 'App\Modules\About\Controllers\AboutController@home'
    ]);
    Route::any( "about/save/{id}", [
        "as" => "about.save",
        "uses" => 'App\Modules\About\Controllers\AboutController@save'
    ]);
});
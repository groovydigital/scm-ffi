<?php
namespace App\Modules\Tnc\Controllers;

use Illuminate\Support\Facades\View;


class TncController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\Tnc::find(1);

        return View::make("tnc::tnc", array(
            'page_title'    => 'Term & Condition Page',
            'menu'          => "tnc",
            'data'          => $data
        ));
    }

    public function save($id){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();

                $data=array(
                    "title"             =>  $input["title"],
                    "text"              =>  htmlspecialchars($input["text"])
                );

                $save=\Tnc::updateData($id,$data);
                if($save) {
                    return \Redirect::to('tnc')->with('message', 'Term & Condition page was update');
                }else{
                    return \Redirect::to('tnc')->withError('message', 'Cannot Saved');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "title" => "required",
            "text" => "required",
        ]);
    }
}
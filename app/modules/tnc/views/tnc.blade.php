@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="<?php echo asset('css/font-awesome-4.2.0/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo asset('plugins/summernote/css/summernote.min.css'); ?>">
@stop

@section("content")
    <div class="row">
        <div class="col-md-12">
            <!-- Form horizontal layout bordered -->
            {{
                  Form::open(array(
                      'url'=>route('tnc.save',$data->id),
                      'class' => 'form-horizontal form-bordered panel panel-default',
                      'id'=>'form',
                      'files' => true
                  ))
             }}
            <div class="panel-body">
                @if($errors->has())
                    <div class="alert alert-danger fade in mt10">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4 class="semibold">Oh snap! You got an error!</h4>
                        <p class="mb10">
                            {{ HTML::ul($errors->all()) }}
                        </p>
                    </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label">Title Page: </label>
                    <div class="col-sm-10 ">
                        <input  type="text" class="form-control" name="title" value="{{isset($data->title)?$data->title:Input::old("title")}}" placeholder="Insert title here.." required data-parsley-required/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="summernote form-control" name="text"></textarea>
                    </div>
                </div>
                <div class="panel-footer">
                    {{--<div class="col-sm-3"> </div>--}}
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                </div>
                {{ Form::close() }}
                        <!--/ Form horizontal layout bordered -->
            </div>
        </div>
    </div>
@stop

@section("javascript_header")
@stop

@section("javascript_footer")
    <script type="text/javascript" src="<?php echo asset('plugins/summernote/js/summernote.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('javascript/forms/wysiwyg.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('javascript/forms/validation.js');?>"></script>
    <script type="application/javascript">
        $(".summernote").html("{{$data->text}}");
    </script>
@stop
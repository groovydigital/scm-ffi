<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "tnc", [
        "as" => "tnc",
        "uses" => 'App\Modules\Tnc\Controllers\TncController@home'
    ]);
    Route::any( "tnc/save/{id}", [
        "as" => "tnc.save",
        "uses" => 'App\Modules\Tnc\Controllers\TncController@save'
    ]);
});
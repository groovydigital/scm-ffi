<?php
namespace App\Modules\Tnc;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('tnc');
    }

    public function boot()
    {
        parent::boot('tnc');
    }

}
<?php
namespace App\Modules\Blog\Controllers;

use Illuminate\Support\Facades\View;


class BlogController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\Blog::whereIsDeleted(0)->orderBy('blog_id','DESC')->paginate(10);

        return View::make("blog::blog", array(
            'page_title'    => 'Inspiration Page',
            'menu'          => "blog",
            'data'          => $data
        ));
    }
    public function add(){
        return View::make("blog::add", array(
            'page_title'    => 'Add Inspiration Page',
            'status'        => "add",
            'menu'          => "blog"
        ));
    }

    public function addSave(){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();
                $destinationPath = public_path('/images/upload/blog');
                $extension = \Input::file('file')->getClientOriginalExtension();
                $fileName =rand(11111,99999).date("Ymd").'blog.'.$extension;
                $input["file"]->move($destinationPath, $fileName);
                $image = \Image::make($destinationPath.'/'.$fileName);
                $image->widen(350);
                $image->save($destinationPath.'/mobile/'.$fileName);
                $data=array(
                    "title"             =>  $input["title"],
                    "slug"              =>  $this->setSlugAttribute($input["title"]),
                    "header"              =>  htmlspecialchars($input["header"]),
                    "text"              =>  htmlspecialchars($input["text"]),
                    "url"             =>  $input["url"],
                    "bahan"              =>  htmlspecialchars($input["bahan"]),
                    "porsi"              =>  $input["porsi"],
                    "image"             =>  $fileName
                );

                $save=\Blog::saveData($data);
                if($save) {
                    return \Redirect::to('blog')->with('message', 'Article was made');
                }else{
                    return \Redirect::to('blog')->withError('message', 'Cannot Saved');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    public function setSlugAttribute($value) {

        $slug = \Str::slug($value);

        $slugs = \Blog::whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'");

        if ($slugs->count() === 0) {

            return $slug;

        }

        // get reverse order and get first
        $lastSlugNumber = intval(str_replace($slug . '-', '', $slugs->orderBy('slug', 'desc')->first()->slug));

        return $slug . '-' . ($lastSlugNumber + 1);

    }

    public function edit($id = null){

        return View::make("blog::add", array(
            'page_title'    => 'Edit Inspiration Page',
            'status'        => 'edit',
            'menu'          => 'blog',
            'data'          => \Blog::find($id)
        ));
    }

    public function update($id){
        if ($this->isPostRequest()) {
            $validator = $this->getEditValidator();
            if ($validator->passes()) {

                $input = \Input::all();
                $data=array(
                    "title"             =>  $input["title"],
                    "header"              =>  htmlspecialchars($input["header"]),
                    "text"              =>  htmlspecialchars($input["text"]),
                    "url"               =>  $input["url"],
                    "bahan"              =>  htmlspecialchars($input["bahan"]),
                    "porsi"              =>  $input["porsi"]
                );
                if (\File::exists(\Input::file('file')))
                {
                    $destinationPath = public_path('/images/upload/blog');
                    $extension = \Input::file('file')->getClientOriginalExtension();
                    $fileName = rand(11111, 99999) . date("Ymd") . 'blog.' . $extension;
                    $input["file"]->move($destinationPath, $fileName);
                    $image = \Image::make($destinationPath . '/' . $fileName);
                    $image->widen(350);
                    $image->save($destinationPath . '/mobile/' . $fileName);
                    $data["image"]=$fileName;
                }

                $save=\Blog::updateData($id,$data);
                if($save) {
                    return \Redirect::to('blog')->with('message', 'Article was updated');
                }else{
                    return \Redirect::to('blog')->withError('message', 'Cannot Saved');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    public function delete($id = null){
        $status = \Blog::deleteData($id);
        if($status){
            return \Redirect::route('blog')->withMessage("Article was deleted");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't delete data");
        }
    }

    public function publish($id = null){
        $status = \Blog::publishData($id);
        if($status){
            return \Redirect::route('blog')->withMessage("Article was published");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't publish data");
        }
    }

    public function unpublish($id = null){
        $status = \Blog::unpublishData($id);
        if($status){
            return \Redirect::route('blog')->withMessage("Article was unpublished");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't unpublish data");
        }
    }

    public function featured($id = null){
        $status = \Blog::featuredData($id);
        if($status){
            return \Redirect::route('blog')->withMessage("Article was featured");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't featured data");
        }
    }

    public function unfeatured($id = null){
        $status = \Blog::unfeaturedData($id);
        if($status){
            return \Redirect::route('blog')->withMessage("Article was unfeatured");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't unfeatured data");
        }
    }

    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "title" => "required",
            "text" => "required",
            "porsi" => "required",
            "bahan" => "required",
            "header" => "required",
            "file" => "image|required",
        ]);
    }

    protected function getEditValidator()
    {
        return \Validator::make(\Input::all(), [
            "title" => "required",
            "header" => "required",
            "text" => "required",
            "porsi" => "required",
            "bahan" => "required"
        ]);
    }

    public function export(){
        $data=\Blog::whereIsDeleted(0)->orderBy('blog_id','DESC')->get();

        try{
            \Excel::create('blog', function($excel) use($data) {

                $excel->sheet('Data Blog', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xls');
        }catch (\Exception $e){
            return \Response::json($e->getMessage(), 400)->header('Access-Control-Allow-Origin','*');
        }
    }
}
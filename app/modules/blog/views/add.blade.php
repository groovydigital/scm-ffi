@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="<?php echo asset('css/font-awesome-4.2.0/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo asset('plugins/summernote/css/summernote.min.css'); ?>">
@stop

@section("content")
    <div class="row">
        <div class="col-md-12">
            <!-- Form horizontal layout bordered -->
            {{
                  Form::open(array(
                      'url'=> $status=='add'?route('blog.save'):route('blog.update',$data->blog_id),
                      'class' => 'form-horizontal form-bordered panel panel-default',
                      'id'=>'form',
                      'files' => true
                  ))
             }}
            <div class="panel-body">
                @if($errors->has())
                    <div class="alert alert-danger fade in mt10">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4 class="semibold">Oh snap! You got an error!</h4>
                        <p class="mb10">
                            {{ HTML::ul($errors->all()) }}
                        </p>
                    </div>
                @endif
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title : </label>
                        <div class="col-sm-10 ">
                            <input  type="text" class="form-control" name="title" value="{{isset($data->title)?$data->title:Input::old("title")}}" placeholder="Insert title here.." required data-parsley-required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thumbnail : </label>
                        @if(isset($data->image))
                        <div class="col-sm-10 ">
                            <img src="{{asset('images/upload/blog/mobile/'.$data->image)}}" class="thumbnail"/>
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        @endif
                        <div class="col-sm-10 ">
                            <input  type="file" accept="image/x-png, image/jpeg, image/gif" class="form-control" name="file" value="" placeholder="Upload thumbnail here.." />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Porsi : </label>
                        <div class="col-sm-10 ">
                            <input  type="text" class="form-control" name="porsi" value="{{isset($data->porsi)?$data->porsi:Input::old("porsi")}}" placeholder="Insert porsi here.." required data-parsley-required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Url Video : </label>
                        <div class="col-sm-10 ">
                            <input  type="text" class="form-control" name="url" value="{{isset($data->url)?$data->url:Input::old("url")}}" placeholder="Insert video url here.." />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Short Description : </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="header" required data-parsley-required>{{isset($data->header)?$data->header:Input::old("header")}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Bahan : </label>
                        <div class="col-sm-10">
                            <textarea class="summernote form-control" id="bahan" name="bahan"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Cara Membuat : </label>
                        <div class="col-sm-10">
                            <textarea class="summernote form-control" id="text" name="text"></textarea>
                        </div>
                    </div>
                    <div class="panel-footer">
                        {{--<div class="col-sm-3"> </div>--}}
                        <a href="{{route('blog')}}" class="btn btn-sm btn-default"><icon class="ico-arrow-left"></icon> Back</a>
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
            {{ Form::close() }}
                    <!--/ Form horizontal layout bordered -->
        </div>
    </div>
</div>
@stop

@section("javascript_header")
@stop

@section("javascript_footer")
    <script type="text/javascript" src="<?php echo asset('plugins/summernote/js/summernote.min.js'); ?>"></script>
   <script type="text/javascript" src="<?php echo asset('javascript/forms/wysiwyg.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo asset('javascript/forms/validation.js');?>"></script>

    <script type="application/javascript">
        $("#text").html("{{isset($data->text)?$data->text:Input::old("text")}}");
        $("#bahan").html("{{isset($data->bahan)?$data->bahan:Input::old("bahan")}}");
    </script>
@stop
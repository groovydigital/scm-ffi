@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="/plugins/tablesorter/css/tablesorter.css">
@stop

@section("content")

    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default" id="toolbar-showcase">
                <!-- panel toolbar wrapper -->
                <div class="panel-toolbar-wrapper pl10 pr10 pt5 pb5">
                    <div class="panel-toolbar text-left">
                        <div class="btn-group">
                        </div>
                    </div>
                    <div class="panel-toolbar text-right">

                        <a href="{{route('blog.export')}}" class="btn btn-sm" style="background-color: #5cb85c; color: #FFF"><i class="ico-file-excel"></i> Export</a>
                        <a href="{{route('blog.add')}}" class="btn btn-sm" style="background-color: #BD3E91; color: #FFF"><i class="ico-newspaper"></i> New Post</a>

                    </div>
                </div>
                <!--/ panel toolbar wrapper -->

                <!-- panel body with collapse capabale -->

                <table class="table table-bordered table-hover tablesorter">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Header</th>
                        <th>Publish Date</th>
                        <th width="220px">Tools</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($data as $key => $val)
                        <tr>
                            <td>{{$data->toArray()["from"]+$key}}</td>
                            <td>{{$val->title}}</td>
                            <td>{{$val->slug}}</td>
                            <td>{{$val->header}}</td>
                            @if($val->published==0)
                                <td>-</td>
                            @else
                                <td>{{$val->published_at}}</td>
                            @endif
                            <td class="text-center">
                                <div class="btn-group">

                                    <a href="{{route("blog.edit",$val->blog_id)}}" class="btn btn-sm btn-default"><i class="ico-pencil" title="Edit"></i> </a>
                                    @if($val->is_featured==0)
                                        <a href="{{route("blog.featured",$val->blog_id)}}" class="btn btn-sm btn-default"><i class="ico-star" title="Featured"></i> </a>
                                    @else
                                        <a href="{{route("blog.unfeatured",$val->blog_id)}}" class="btn btn-sm btn-success"><i class="ico-star" title="UnFeatured"></i> </a>
                                    @endif
                                    @if($val->published==0)
                                        <a href="{{route("blog.publish",$val->blog_id)}}" class="btn btn-sm btn-primary"><i class="ico-check"></i> Publish</a>
                                    @else
                                        <a href="{{route("blog.unpublish",$val->blog_id)}}" class="btn btn-sm btn-warning"><i class="ico-check"></i> UnPublish</a>
                                    @endif
                                    <a href="{{route("blog.delete",$val->blog_id)}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="ico-trash" title="Delete"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <div class="col-sm-6">
                        <div class=" paging_bootstrap">
                            {{$data->links()}}
                        </div>
                    </div>
                </div>
            </div>
            <!--/ panel body with collapse capabale -->
        </div>
    </div>
@stop

@section("javascript_header")

@stop

@section("javascript_footer")
    <script type="text/javascript" src="/plugins/tablesorter/js/tablesorter.js"></script>
    <script type="application/javascript">
        $('table').tablesorter({});
    </script>
@stop

<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "blog", [
        "as" => "blog",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@home'
    ]);
    Route::any( "blog/add", [
        "as" => "blog.add",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@add'
    ]);
    Route::any( "blog/save", [
        "as" => "blog.save",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@addSave'
    ]);
    Route::any( "blog/edit/{id}", [
        "as" => "blog.edit",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@edit'
    ]);
    Route::any( "blog/update/{id}", [
        "as" => "blog.update",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@update'
    ]);
    Route::any( "blog/delete/{id}", [
        "as" => "blog.delete",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@delete'
    ]);
    Route::any( "blog/publish/{id}", [
        "as" => "blog.publish",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@publish'
    ]);
    Route::any( "blog/unpublish/{id}", [
        "as" => "blog.unpublish",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@unpublish'
    ]);
    Route::any( "blog/featured/{id}", [
        "as" => "blog.featured",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@featured'
    ]);
    Route::any( "blog/unfeatured/{id}", [
        "as" => "blog.unfeatured",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@unfeatured'
    ]);
    Route::any( "blog/export", [
        "as" => "blog.export",
        "uses" => 'App\Modules\Blog\Controllers\BlogController@export'
    ]);
});
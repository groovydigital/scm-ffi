@extends("layout::main-layout")
@section("stylesheet_header")
@stop

@section("content")
    <div class="row panel panel-default">
        <div class="col-md-12">
            <div class="col-md-4">
               <a href="{{route('users')}}"> User Management </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('post')}}"> Post Management </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('message')}}"> Message Management</a>
            </div>
        </div>

        <div class="col-md-12">
            <div class="col-md-4">
                <a href="{{route('about')}}"> About Page </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('howto')}}"> How To Page</a>
            </div>
            <div class="col-md-4">
                <a href="{{route('blog')}}"> Inspiration Page</a>
            </div>
        </div>

        <div class="col-md-12">
            <div class="col-md-4">
                <a href="{{route('winner')}}"> Winner Page</a>
            </div>
            <div class="col-md-4">
                <a href="{{route('tnc')}}">Term & Condition Page</a>
            </div>
            <div class="col-md-4">
                <a href="{{route('privacy')}}">Privacy Page</a>
            </div>
        </div>
    </div>
@stop

@section("javascript_header")
@stop

@section("javascript_footer")

@stop
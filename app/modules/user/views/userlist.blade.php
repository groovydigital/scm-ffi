@extends("layout::main-layout")
@section("stylesheet_header")
    <link rel="stylesheet" href="/plugins/tablesorter/css/tablesorter.css">
@stop

@section("content")

    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default" id="toolbar-showcase">
                <!-- panel toolbar wrapper -->
                <div class="panel-toolbar-wrapper pl10 pr10 pt5 pb5">
                    <div class="panel-toolbar text-left">
                        <div class="btn-group">
                        </div>
                    </div>
                    <div class="panel-toolbar text-right">

                            <a href="{{route('users.export')}}" class="btn btn-sm" style="background-color: #5cb85c; color: #FFF"><i class="ico-file-excel"></i> Export</a>
                            <a href="{{route('users.add')}}" class="btn btn-sm" style="background-color: #BD3E91; color: #FFF"><i class="ico-users"></i> New User</a>

                    </div>
                </div>
                <!--/ panel toolbar wrapper -->

                <!-- panel body with collapse capabale -->

                    <table class="table table-bordered table-hover tablesorter">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Username</th>
                            <th>Fullname</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th width="200px">Tools</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data as $key => $val)
                        <tr>
                            <td>{{$data->toArray()["from"]+$key}}</td>
                            <td>{{$val->username}}</td>
                            <td>{{$val->name}}</td>
                            <td>{{$val->email}}</td>
                            <td>{{$val->phone}}</td>
                            <td class="text-center">
                                <div class="btn-group">

                                    <a href="{{route("users.edit",$val->id)}}" class="btn btn-sm btn-default"><i class="ico-pencil" title="Edit"></i></a>
                                    @if($val->login_enabled==0)
                                        <a href="{{route("users.inactive",$val->id)}}" class="btn btn-sm btn-success"><i class="ico-check"></i> Active</a>
                                    @else
                                        <a href="{{route("users.active",$val->id)}}" class="btn btn-sm btn-warning"><i class="ico-check"></i> Inactive</a>
                                    @endif
                                    <a href="" class="btn btn-sm btn-default"><i class="ico-mail" title="Message"></i></a>
                                    <a href="{{route("users.delete",$val->id)}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="ico-trash" title="Delete"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                <div class="panel-footer">
                    <div class="col-sm-6">
                        <div class=" paging_bootstrap">
                            {{$data->links()}}
                        </div>
                    </div>
                </div>
            </div>
                <!--/ panel body with collapse capabale -->
        </div>
    </div>
@stop

@section("javascript_header")

@stop

@section("javascript_footer")
    <script type="text/javascript" src="/plugins/tablesorter/js/tablesorter.js"></script>
    <script type="application/javascript">
        $('table').tablesorter({});
    </script>
@stop

<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{$page_title}}</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Dasboard login">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->


        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="/library/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/stylesheet/layout.min.css">
        <link rel="stylesheet" href="/stylesheet/uielement.min.css">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script src="/library/modernizr/js/modernizr.min.js"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <section class="container">
                <!-- START row -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <!-- Brand -->
                        <div class="text-center" style="margin-bottom:40px;">
                                    <span class=" inverse"></span>
                                    <span class=" inverse"></span>
                                    <h5 class="semibold text-muted mt-5"></h5>
                                </div>

                        <!--/ Brand -->
                        <!-- Login form -->
                        <form class="panel" name="form-login" action="<?php echo route('login.post');?>" method="post">
                            <div class="panel-body">
                                <div class="text-center" style="margin-bottom:40px;">
                                    <span class=" inverse"></span>
                                    <span class=" inverse"></span>
                                    <h5 class="semibold text-muted mt-5">
                                        <img src="/frontend/img/logo-bulan-sarapan-sempurna.png" alt="logo" style="width:200px;"/>
                                    </h5>
                                </div>
                                <!-- Alert message -->
                                @if($errors->has())
                                <ul class="list-table pa15">
                                    <li>
                                        <!-- Alert message -->
                                        <div class="alert alert-warning nm">
                                            <span class="semibold">Note :</span>&nbsp;&nbsp;{{ HTML::ul($errors->all()) }}
                                        </div>
                                        <!--/ Alert message -->
                                    </li>
                                    <li class="text-right" style="width:20px;"><a href="javascript:void(0);"><i class="ico-question-sign fsize16"></i></a></li>
                                </ul>
                                @endif
                                <!--/ Alert message -->
                                
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input name="username" type="text" class="form-control input-lg" value="{{Input::old("username")}}" placeholder="Username" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your username / email" data-parsley-required>
                                        <i class="ico-user2 form-control-icon"></i>
                                    </div>
                                    <div class="form-stack has-icon pull-left">
                                        <input name="password" type="password" class="form-control input-lg" value="{{Input::old("password")}}" placeholder="Password" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your password" data-parsley-required>
                                        <i class="ico-lock2 form-control-icon"></i>
                                    </div>
                                </div>

                                <!-- Error container -->
                                <div id="error-container"class="mb15"></div>
                                <!--/ Error container -->

                                <div class="form-group nm">
                                    <button type="submit" class="btn btn-block"><span class="semibold">Sign In</span></button>
                                </div>
                            </div>
                        </form>
                        <!-- Login form -->

                        <hr><!-- horizontal line -->

                       </div>
                </div>
                <!--/ END row -->
            </section>
            <!--/ END Template Container -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->
        <script type="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="/library/jquery/js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="/library/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/library/core/js/core.min.js"></script>
        <!--/ Library script -->

        <!-- App and page level script -->
        <script type="text/javascript" src="/plugins/sparkline/js/jquery.sparkline.min.js"></script><!-- will be use globaly as a summary on sidebar menu -->
        <script type="text/javascript" src="/javascript/app.min.js"></script>
        
        
        <script type="text/javascript" src="/plugins/parsley/js/parsley.min.js"></script>
        
        <!-- <script type="text/javascript" src="/javascript/pages/login.js"></script> -->
        
        <!--/ App and page level script -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>
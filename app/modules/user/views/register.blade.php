<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Adminre backend</title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap 3.1.1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/image/touch/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/image/touch/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/image/touch/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/image/touch/apple-touch-icon-57x57-precomposed.png">
        <link rel="shortcut icon" href="/image/favicon.ico">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        
        
        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="/library/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/stylesheet/layout.min.css">
        <link rel="stylesheet" href="/stylesheet/uielement.min.css">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script src="/library/modernizr/js/modernizr.min.js"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <section class="container">
                <!-- START row -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <!-- Brand -->
                        <div class="text-center" style="margin-bottom:20px;">
                            <span class="logo-figure inverse"></span>
                            <span class="logo-text inverse"></span>
                            <h5 class="semibold text-muted mt-5"></h5>
                        </div>
                        <!--/ Brand -->

                        <!-- Register form -->
                        <form class="panel" name="form-register" action="<?php echo route('register.post');?>" method="post">
                            <div class="text-center" style="margin-bottom:20px;">
                            <span class="logo-figure inverse"></span>
                            <span class="logo-text inverse"></span>
                            <h5 class="semibold text-muted mt-5">
                                <img src="/images/logorightkliq.png" alt="logo rigtcliq" style="width:200px;"/>
                            </h5>
                        </div>
                            @if($errors->has())
                            <ul class="list-table pa15">
                                <li>
                                    <!-- Alert message -->
                                    <div class="alert alert-warning nm">
                                        <span class="semibold">Note :</span>&nbsp;&nbsp;{{ HTML::ul($errors->all()) }}
                                    </div>
                                    <!--/ Alert message -->
                                </li>
                                <li class="text-right" style="width:20px;"><a href="javascript:void(0);"><i class="ico-question-sign fsize16"></i></a></li>
                            </ul>
                            @endif
                            <hr class="nm">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label">Username</label>
                                    <div class="has-icon pull-left">
                                        <input type="text" class="form-control" value="{{Input::old("username")}}" name="username" data-parsley-required>
                                        <i class="ico-user2 form-control-icon"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <div class="has-icon pull-left">
                                        <input type="password" class="form-control" value="{{Input::old("password")}}" name="password" data-parsley-required>
                                        <i class="ico-key2 form-control-icon"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Retype Password</label>
                                    <div class="has-icon pull-left">
                                        <input type="password" class="form-control" value="{{Input::old("retype-password")}}" name="retype-password" data-parsley-equalto="input[name=password]">
                                        <i class="ico-asterisk form-control-icon"></i>
                                    </div>
                                </div>
                            </div>
                            <hr class="nm">
                            <div class="panel-body">
                                <p class="semibold text-muted">To confirm and activate your new account, we will need to send the activation code to your e-mail.</p>
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <div class="has-icon pull-left">
                                        <input type="email" class="form-control" value="{{Input::old("email")}}" name="email" placeholder="you@mail.com">
                                        <i class="ico-envelop form-control-icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-block"><span class="semibold">Sign up</span></button>
                            </div>
                        </form>
                        <!-- Register form -->

                        <hr><!-- horizontal line -->

                        <p class="text-center">
                            <span class="text-muted">Already have an account? <a class="semibold" href="<?php echo route('login')?>">Sign in here</a></span>
                        </p>
                    </div>
                </div>
                <!--/ END row -->
            </section>
            <!--/ END Template Container -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->
        <script type="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="/library/jquery/js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="/library/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/library/core/js/core.min.js"></script>
        <!--/ Library script -->

        <!-- App and page level script -->
        <script type="text/javascript" src="/plugins/sparkline/js/jquery.sparkline.min.js"></script><!-- will be use globaly as a summary on sidebar menu -->
        <script type="text/javascript" src="/javascript/app.min.js"></script>
        <script type="text/javascript" src="/plugins/parsley/js/parsley.min.js"></script>
        <!-- <script type="text/javascript" src="/javascript/pages/register.js"></script> -->
        
        <!--/ App and page level script -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
</html>
@extends("layout::main-layout")

@section("content")

<div class="row">
    <div class="col-md-12">
        <!-- Form horizontal layout bordered -->
        {{
              Form::open(array(
                  'url'=> $status=='add'?route('users.store'):route('users.update',$data->id),
                  'class' => 'form-horizontal form-bordered panel panel-default',
                  'id'=>'form',
                  'files' => true
              ))
         }}
            <div class="panel-body">
                @if($errors->has())
                <div class="alert alert-danger fade in mt10">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4 class="semibold">Oh snap! You got an error!</h4>
                    <p class="mb10">
                        {{ HTML::ul($errors->all()) }}
                    </p>
                </div>
                @endif

                <div class="form-group">
                    <label class="control-label col-sm-3">Username</label>
                    <div class="col-sm-8">
                        <input name="username" placeholder="Username" type="text" value="{{isset($data->username)?$data->username:Input::old("username")}}" class="form-control" required data-parsley-required {{($status=="edit")?"readonly":""}}>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Password</label>
                    <div class="col-sm-8">
                        <input name="password" placeholder="Password" type="password" value="{{isset($data->password)?$data->password:Input::old("password")}}" class="form-control" required data-parsley-required>
                        <span class="help-block">Please empty the this field if you don't want to change the password.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Fullname</label>
                    <div class="col-sm-8">
                        <input name="fullname" placeholder="Fullname" type="text" value="{{isset($data->name)?$data->name:Input::old("fullname")}}" class="form-control" required data-parsley-required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Email</label>
                    <div class="col-sm-8">
                        <input name="email" placeholder="Fullname" type="email" value="{{isset($data->email)?$data->email:Input::old("email")}}" class="form-control" required data-parsley-type="email" {{($status=="edit")?"readonly":""}}>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Phone</label>
                    <div class="col-sm-8">
                        <input name="phone" placeholder="Phone" type="text" value="{{isset($data->phone)?$data->phone:Input::old("phone")}}" class="form-control" required data-parsley-type="digits">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Spouse</label>
                    <div class="col-sm-8">
                        <input name="spouse" placeholder="Spouse" type="text" value="{{isset($data->spouse)?$data->spouse:Input::old("spouse")}}" class="form-control" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Child</label>
                    <div class="col-sm-8">
                        <input name="child" placeholder="Child" type="text" value="{{isset($data->child)?$data->child:Input::old("child")}}" class="form-control" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Photo</label>
                    <div class="col-sm-8">
                        <input name="file" accept="image/x-png, image/jpeg, image/gif" type="file" class="form-control" >
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                {{--<div class="col-sm-3"> </div>--}}
                <a href="{{route('users')}}" class="btn btn-sm btn-default"><icon class="ico-arrow-left"></icon> Back</a>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
             </div>
         {{ Form::close() }}
        <!--/ Form horizontal layout bordered -->
    </div>
</div>
<!--/ END row -->
@stop
@section("javascript_footer")
<script type="text/javascript" src="<?php echo asset('plugins/parsley/js/parsley.min.js');?>"></script>
<script type="text/javascript" src="<?php echo asset('javascript/forms/validation.js');?>"></script>
@stop
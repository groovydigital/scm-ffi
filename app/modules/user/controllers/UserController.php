<?php
namespace App\Modules\User\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


class UserController extends \BaseController {
    var $upload_data = array();
    var $baseurl_images;

    public function __construct()
    {
        $this->setupData();
    }

    public function userList(){

        $data=\User::whereIsDeleted(0)->orderBy('id','DESC')->paginate(10);

        return View::make("user::userlist", array(
            'page_title'    => 'User Management',
            'menu'          => "user",
            'data'          => $data
        ));

    }
    public function userListAdd(){

        return View::make("user::userlist_add", array(
            'page_title'    => 'Add User',
            'status'        => 'add',
            'menu'          => "user"
        ));

    }

    public function userListStore(){
        if ($this->isPostRequest()) {
            $validator = $this->getUsersValidator();
            if ($validator->passes()) {
                $params['username']         = \Input::get('username');
                $params['name']         = \Input::get('fullname');
                $params['password']         = \Hash::make(Input::get('password'));
                $params['email']            = \Input::get('email');
                $params['phone']            = \Input::get('phone');
                $params['spouse']          = \Input::get('spouse');
                $params['child']          = \Input::get('child');

                $destinationPath = public_path('/images/upload/photo');
                $extension = \Input::file('file')->getClientOriginalExtension();
                $fileName =rand(11111,99999).date("Ymd").'photo.'.$extension;
                \Input::file('file')->move($destinationPath, $fileName);
                $image = \Image::make($destinationPath.'/'.$fileName);
                $image->widen(200);
                $image->save($destinationPath.'/mobile/'.$fileName);

                $params['image']=$fileName;

                $success = \User::saveData($params);
                if($success){
                    return \Redirect::route('users')->withMessage("Save Successful");
                }else{
                    return \Redirect::back()->withInput()->withErrors("Can't Input Data to Table");
                }
            } else {
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        } else {
            return \Redirect::back()->withInput()->withErrors("Can't Post Data");
        }
    }
    public function userListEdit($id = null){

        return View::make("user::userlist_add", array(
            'page_title'    => 'Add User',
            'status'        => 'edit',
            'menu'          => 'user',
            'data'          => \User::find($id)
        ));
    }
    public function userListUpdate($id){

        if ($this->isPostRequest()) {
            $validator = $this->getUsersEditValidator();
            if ($validator->passes()) {

                $params['username']         = \Input::get('username');
                $params['name']         = \Input::get('fullname');
                $params['password'] = \Hash::make(Input::get('password'));
                $params['email']            = \Input::get('email');
                $params['phone']            = \Input::get('phone');
                $params['spouse']          = \Input::get('spouse');
                $params['child']          = \Input::get('child');

                if (\File::exists(\Input::file('file'))){
                    $destinationPath = public_path('/images/upload/photo');
                    $extension = \Input::file('file')->getClientOriginalExtension();
                    $fileName =rand(11111,99999).date("Ymd").'photo.'.$extension;
                    \Input::file('file')->move($destinationPath, $fileName);
                    $image = \Image::make($destinationPath.'/'.$fileName);
                    $image->widen(200);
                    $image->save($destinationPath.'/mobile/'.$fileName);

                    $params['image']=$fileName;
                }


                $success = \User::updateData($id, $params);
                if($success){
                    return \Redirect::route('users')->withMessage("update data successful");
                }else{
                    return \Redirect::back()->withInput()->withErrors("Can't Input Data to Table");
                }
            } else {
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        } else {
            return \Redirect::back()->withInput()->withErrors("Can't Post data");
        }
    }
    public function userListDelete($id = null){
        $delete = \User::deleteData($id);
        if($delete){
            return \Redirect::route('users')->withMessage("Deleted successful");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't Delete Data to Table");
        }
    }

    public function userActive($id = null){
        $active = \User::activeData($id);
        if($active){
            return \Redirect::route('users')->withMessage("User Active");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't Process data");
        }
    }

    public function userInActive($id = null){
        $inactive = \User::inactiveData($id);
        if($inactive){
            return \Redirect::route('users')->withMessage("User Inactive");
        }else{
            return \Redirect::back()->withInput()->withErrors("Can't Process data");
        }
    }

    protected function getUsersValidator()
    {
        return \Validator::make(\Input::all(), [
            "username"        => "required|unique:ffi_users",
            "fullname"        => "required",
            "password"        => "required|min:5",
            "email"           => "required|email|unique:ffi_users",
            "phone"           => "required",
            "file" => "image|required"
        ]);
    }
    protected function getUsersEditValidator()
    {
        return \Validator::make(\Input::all(), [
            //"username"        => "required|unique:ffi_users",
            "fullname"        => "required",
            //"password"        => "required|min:5",
            //"email"           => "required|email|unique:ffi_users",
            "phone"           => "required",
            //  "file" => "image",
        ]);
    }

    public function search(){
        $name=\Input::get('name');

        try{
            $data= \User::where('username', 'like', '%'.$name.'%')->whereIsDeleted(0)->get();

            return \Response::json($data, 200)->header('Access-Control-Allow-Origin','*');
        }catch (\Exception $e){
            return \Response::json($e->getMessage(), 400)->header('Access-Control-Allow-Origin','*');
        }
    }

    public function export(){
        $data=\User::whereIsDeleted(0)->orderBy('id','DESC')->get();

        try{
            \Excel::create('users', function($excel) use($data) {

                $excel->sheet('Data User', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xls');
        }catch (\Exception $e){
            return \Response::json($e->getMessage(), 400)->header('Access-Control-Allow-Origin','*');
        }
    }
}
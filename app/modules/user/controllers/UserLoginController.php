<?php
namespace App\Modules\User\Controllers;

use Illuminate\Support\Facades\Input;

class UserLoginController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index()
    {
        return \View::make("user::register");
    }

    public function register()
    {
        $this->setPageTitle("Register");
        if ($this->isPostRequest()) {
            $validator = $this->getRegisterValidator();
            if ($validator->passes()) {

                $record                 = new \User;
                $record->username       = \Input::get('username');
                $record->password       = \Hash::make(Input::get('password'));
                $record->email          = \Input::get('email');

                $record->save();

                return \Redirect::to('login');

            } else {
                return \Redirect::back()->withInput()->withErrors($validator);
            }

        } else {
            return \View::make("user::register", $this->data);
        }

    }

    public function login()
    {
        $this->setPageTitle("Login");
        if ($this->isPostRequest()) {
            $validator = $this->getLoginValidator();
            if ($validator->passes()) {
                $credentials = $this->getLoginInput();
                if (\Auth::attempt($credentials)) {
                    if (\Auth::check()) {
                        return \Redirect::route('dashboard');
                    }
                } else {
                    return \Redirect::back()->withInput()->withErrors([
                        "password" => ["Credentials invalid."]
                    ]);
                }
            } else {
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        } else {
            return \View::make("user::login", $this->data);
        }
    }

    public function logout()
    {
        \Auth::logout();
        return \Redirect::route("login");
    }


    protected function getRegisterValidator()
    {

        return \Validator::make(\Input::all(), [
            "username"  => "required|unique:user",
            "email"     => "required|email|unique:user",
            "password"  => "required|min:5"
        ]);
    }

    protected function getLoginValidator()
    {
        return \Validator::make(\Input::all(), [
            "username" => "required",
            "password" => "required|min:5"
        ]);
    }

    protected function getLoginInput()
    {
        return [
            "username" => \Input::get("username"),
            "password" => \Input::get("password"),
            "login_enabled" => 0
        ];
    }

}
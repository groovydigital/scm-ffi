-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Feb 2016 pada 01.03
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbffi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_about`
--

CREATE TABLE IF NOT EXISTS `ffi_about` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `ffi_about`
--

INSERT INTO `ffi_about` (`id`, `title`, `text`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'About Page', '&lt;p style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Harper Lee passed away at the age of 89 on February 19, 2016.&amp;nbsp;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-weight: 700; background: 0px 0px;&quot;&gt;Harper Lee, Obscurity over Fame:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Like&amp;nbsp;&lt;a href=&quot;http://fictionwriting.about.com/b/2010/01/28/j-d-salinger-dead-at-91.htm&quot; data-component=&quot;link&quot; data-source=&quot;inlineLink&quot; data-type=&quot;internalLink&quot; data-ordinal=&quot;1&quot; style=&quot;margin: 0px; padding: 0px; vertical-align: baseline; color: rgb(0, 153, 204); text-decoration: underline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;J. D. Salinger&lt;/a&gt;,&amp;nbsp;&lt;a data-inlink=&quot;FQNjork6ItZGZQiCTCiFvA==&quot; href=&quot;http://contemporarylit.about.com/cs/authors/p/lee.htm&quot; data-component=&quot;link&quot; data-source=&quot;inlineLink&quot; data-type=&quot;internalLink&quot; data-ordinal=&quot;2&quot; style=&quot;margin: 0px; padding: 0px; vertical-align: baseline; color: rgb(0, 153, 204); text-decoration: underline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Harper Lee&lt;/a&gt;&amp;nbsp;produced a classic work of American fiction and then vanished from the literary scene.&amp;nbsp;&lt;em style=&quot;margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: 0px 0px;&quot;&gt;To Kill a Mockingbird&lt;/em&gt;&amp;nbsp;was an instant success when it appeared in 1960, lingering on the best-seller list for 88 weeks and collecting a&amp;nbsp;&lt;a data-inlink=&quot;CNurd7jvXA468HesZZ_Pog==&quot; href=&quot;http://publishing.about.com/od/Books/a/The-Pulitzer-Prize-For-Letters-A-Coveted-Prize-For-Book-Authors.htm&quot; data-component=&quot;link&quot; data-source=&quot;inlineLink&quot; data-type=&quot;internalLink&quot; data-ordinal=&quot;3&quot; style=&quot;margin: 0px; padding: 0px; vertical-align: baseline; color: rgb(0, 153, 204); text-decoration: underline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Pulitzer Prize&lt;/a&gt;. But despite her professed aspiration to become the &quot;&lt;a data-inlink=&quot;It618pK5M3IUadPLLgPRuw==&quot; href=&quot;http://fictionwriting.about.com/od/interviews/a/JaneAusten.htm&quot; data-component=&quot;link&quot; data-source=&quot;inlineLink&quot; data-type=&quot;internalLink&quot; data-ordinal=&quot;4&quot; style=&quot;margin: 0px; padding: 0px; vertical-align: baseline; color: rgb(0, 153, 204); text-decoration: underline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Jane Austen&lt;/a&gt;&amp;nbsp;of Alabama,&quot; she published only a handful of articles and no other novels.&lt;/p&gt;&lt;p class=&quot;cb-split&quot; style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;&amp;nbsp;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-weight: 700; background: 0px 0px;&quot;&gt;Why Did&amp;nbsp;&lt;a data-inlink=&quot;elmIleKy5nJDrSJ5b1c7JQ==&quot; href=&quot;http://classiclit.about.com/od/tokillamockingbird/f/Why-Did-Harper-Lee-Stop-Writing-After-to-Kill-A-Mockingbird.htm&quot; data-component=&quot;link&quot; data-source=&quot;inlineLink&quot; data-type=&quot;internalLink&quot; data-ordinal=&quot;5&quot; style=&quot;margin: 0px; padding: 0px; vertical-align: baseline; color: rgb(0, 153, 204); text-decoration: underline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Harper Lee Disappear&lt;/a&gt;?:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;The author did offer some clue as to why she retreated from the limelight after the success of&amp;nbsp;&lt;em style=&quot;margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: 0px 0px;&quot;&gt;To Kill a Mockingbird.&lt;/em&gt;&amp;nbsp;In a 1964 interview, she said, &quot;I was hoping for a quick and merciful death at the hands of reviewers, but at the same time I sort of hoped that maybe someone would like it enough to give me encouragement . . . I hoped for a little, as I said, but I got rather a whole lot, and in some ways this was just about as frightening as the quick, merciful death I''d expected.&quot;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-weight: 700; background: 0px 0px;&quot;&gt;&lt;a data-inlink=&quot;-mHtNrgVNnIqqVN-P5r10w==&quot; href=&quot;http://classiclit.about.com/od/leeharper/p/aa_harperlee.htm&quot; data-component=&quot;link&quot; data-source=&quot;inlineLink&quot; data-type=&quot;internalLink&quot; data-ordinal=&quot;6&quot; style=&quot;margin: 0px; padding: 0px; vertical-align: baseline; color: rgb(0, 153, 204); text-decoration: underline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Harper Lee''s Early Years&lt;/a&gt;&amp;nbsp;and Education:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 17px; padding: 0px; border: 0px; font-size: 15px; vertical-align: baseline; color: rgb(25, 25, 25); line-height: 21px !important; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;&quot;&gt;Born Nelle Harper Lee in 1926, the future author grew up in Depression-era Alabama, the youngest of Amasa Coleman Lee and Frances Finch Lee''s four children.&lt;/p&gt;', '2016-02-21 21:43:02', '2016-02-21 14:43:02', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_blog`
--

CREATE TABLE IF NOT EXISTS `ffi_blog` (
`blog_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `porsi` varchar(255) DEFAULT NULL,
  `bahan` text,
  `text` text,
  `image` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `is_featured` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `ffi_blog`
--

INSERT INTO `ffi_blog` (`blog_id`, `title`, `slug`, `header`, `porsi`, `bahan`, `text`, `image`, `published`, `is_featured`, `created_at`, `published_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Risol', 'risol', 'makanan khas anak kosan', '5 orang', '&lt;ol&gt;&lt;li&gt;Kacang&lt;/li&gt;&lt;li&gt;Bawang&lt;/li&gt;&lt;li&gt;Brambang&lt;/li&gt;&lt;/ol&gt;', '&lt;p&gt;Goreng ayam setengah matang atau sampai bagian kulitnya kecoklatan. Sisihkan.&lt;/p&gt;&lt;p&gt;Tumis serai, lengkuas, daun salam, jahe dan daun jeruk sampai harum, lalu masukkan bumbu yg sudah dihaluskan. Masak sampai bumbu matang atau sampai bumbu tidak berbau langu, lalu masukkan ayam. Aduk sampai bumbu merata, lalu tambahkan air secukupnya.&lt;/p&gt;&lt;p&gt;Bumbui dengan garam, gula merah, merica bubuk dan kecap manis. Tambahkan irisan cabe merah.Koreksi rasanya. Masak sampai ayam empuk dan air terserap. Sajikan panas dengan taburan bawang goreng.&lt;/p&gt;', '9170720160223blog.jpg', 0, 0, '2016-02-23 21:48:46', '2016-02-18 19:23:55', '2016-02-23 14:48:46', 0),
(2, 'Bola Bola Tahu Mackerel', 'bola-bola-tahu-mackerel', 'Siang siang mumpung anak tidur, curi waktu buat makanan... \r\nDi rak makanan dapur kebetulan masih ada beberapa bahan diantaranya mackerel kalengan dan tepung roti. Ya udin deh, buka kulkas masih ada tahu. Langsung dibikin. Niatnya cuma mau bikin bola bola', '3 orang', '&lt;ol&gt;&lt;li&gt;tahu&lt;/li&gt;&lt;li&gt;daging&lt;/li&gt;&lt;li&gt;ayam&lt;/li&gt;&lt;li&gt;sayur&lt;/li&gt;&lt;/ol&gt;', '&lt;p&gt;Siapkan wadah. Campur tahu yang sudah dihancurkan dengan mackerel kaleng. Tambahkan tepung roti, telur, bawang putih yang sudah dihaluskan, merica bubuk, dan bumbu penyedap.&lt;/p&gt;&lt;p&gt;Aduk rata semua bahan.&lt;/p&gt;&lt;p&gt;Setelah adonan tercampur rata, bentuk bola bola menggunakan tangan.&lt;/p&gt;&lt;p&gt;Panaskan minyak kemudian goreng hingga kecoklatan.&lt;/p&gt;&lt;p&gt;Bola bola tahu mackerel siap dihidangkan.&lt;/p&gt;', '4208920160223blog.jpg', 0, 0, '2016-02-23 14:51:34', '0000-00-00 00:00:00', '2016-02-23 14:51:34', 0),
(3, 'Bola Bola Tahu Mackerel', 'bola-bola-tahu-mackerel-1', 'Siapkan wadah. Campur tahu yang sudah dihancurkan dengan mackerel kaleng. Tambahkan tepung roti, telur, bawang putih yang sudah dihaluskan, merica bubuk, dan bumbu penyedap.', '5 orang', '&lt;p&gt;Siapkan wadah. Campur tahu yang sudah dihancurkan dengan mackerel kaleng. Tambahkan tepung roti, telur, bawang putih yang sudah dihaluskan, merica bubuk, dan bumbu penyedap.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Aduk rata semua bahan.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Setelah adonan tercampur rata, bentuk bola bola menggunakan tangan.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Panaskan minyak kemudian goreng hingga kecoklatan.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Bola bola tahu mackerel siap dihidangkan.&lt;/p&gt;', '&lt;p&gt;Siapkan wadah. Campur tahu yang sudah dihancurkan dengan mackerel kaleng. Tambahkan tepung roti, telur, bawang putih yang sudah dihaluskan, merica bubuk, dan bumbu penyedap.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Aduk rata semua bahan.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Setelah adonan tercampur rata, bentuk bola bola menggunakan tangan.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Panaskan minyak kemudian goreng hingga kecoklatan.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Bola bola tahu mackerel siap dihidangkan.&lt;/p&gt;', '4905820160223blog.jpg', 0, 0, '2016-02-23 21:52:50', '0000-00-00 00:00:00', '2016-02-23 14:52:50', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_config`
--

CREATE TABLE IF NOT EXISTS `ffi_config` (
  `key_config` varchar(255) NOT NULL DEFAULT '',
  `value_config` varchar(255) DEFAULT NULL,
  `label_config` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_howto`
--

CREATE TABLE IF NOT EXISTS `ffi_howto` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `ffi_howto`
--

INSERT INTO `ffi_howto` (`id`, `title`, `text`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'How to Page', '&lt;p&gt;adasdad&lt;/p&gt;', '2016-02-19 11:37:32', '2016-02-19 04:37:32', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_inbox`
--

CREATE TABLE IF NOT EXISTS `ffi_inbox` (
`inbox_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` text,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `read_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `ffi_inbox`
--

INSERT INTO `ffi_inbox` (`inbox_id`, `user_id`, `message`, `status`, `created_at`, `updated_at`, `read_at`, `is_deleted`) VALUES
(1, 2, 'this is first message', 'read', '2016-02-21 22:29:01', '2016-02-15 19:27:40', '2016-02-15 19:27:42', 0),
(2, 2, 'this second message', 'send', '2016-02-21 22:29:02', '2016-02-15 20:04:27', '0000-00-00 00:00:00', 0),
(3, 21, 'asemmmmmmmmmmmmmmmmmmmmmmm', 'read', '2016-02-23 17:58:46', '2016-02-23 10:58:46', '0000-00-00 00:00:00', 0),
(4, 21, '  test', 'read', '2016-02-23 17:58:46', '2016-02-23 10:58:46', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_post`
--

CREATE TABLE IF NOT EXISTS `ffi_post` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type_post` varchar(255) DEFAULT NULL,
  `caption` text,
  `image` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `location` text,
  `published` tinyint(1) DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `ffi_post`
--

INSERT INTO `ffi_post` (`id`, `user_id`, `type_post`, `caption`, `image`, `latitude`, `longitude`, `location`, `published`, `featured`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 1, 'text', 'this is first post', NULL, NULL, NULL, NULL, 0, 0, '2016-02-16 20:13:44', '2016-02-16 13:13:44', 0),
(2, 1, 'photo', 'isyana', '4890120160221post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-21 16:02:26', '2016-02-21 16:02:26', 0),
(3, 1, 'photo', 'isyana', '9290320160221post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-21 16:03:31', '2016-02-21 16:03:31', 0),
(4, 1, 'text', 'asdasdad', NULL, NULL, NULL, NULL, 0, 0, '2016-02-21 16:21:01', '2016-02-21 16:21:01', 0),
(5, 1, 'text', 'kisah kita berakhir dijanuary', NULL, NULL, NULL, NULL, 0, 0, '2016-02-21 16:22:15', '2016-02-21 16:22:15', 0),
(6, 1, 'text', 'asdasdadad', NULL, NULL, NULL, NULL, 0, 0, '2016-02-21 16:38:26', '2016-02-21 16:38:26', 0),
(7, 1, 'text', 'asdasd', NULL, NULL, NULL, NULL, 0, 0, '2016-02-21 16:46:42', '2016-02-21 16:46:42', 0),
(8, 1, 'photo', 'asdad', '5998520160221post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-21 16:46:55', '2016-02-21 16:46:55', 0),
(9, 1, 'text', 'adsad', NULL, NULL, NULL, NULL, 0, 0, '2016-02-21 21:03:28', '2016-02-21 21:03:28', 0),
(10, 1, 'text', 'asdasdasdad', NULL, NULL, NULL, NULL, 0, 0, '2016-02-21 21:21:18', '2016-02-21 21:21:18', 0),
(11, 17, 'photo', 'isyana sarasvati', '7819220160222post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-22 09:55:20', '2016-02-22 09:55:20', 0),
(12, 17, 'photo', 'isyana lagi', '8844920160222post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-22 09:58:31', '2016-02-22 09:58:31', 0),
(13, 17, 'photo', 'ahok', '5503120160222post.png', NULL, NULL, NULL, 0, 0, '2016-02-22 10:04:49', '2016-02-22 10:04:49', 0),
(14, 17, 'photo', 'obsat tahun itu', '6669920160222post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-22 10:06:33', '2016-02-22 10:06:33', 0),
(15, 17, 'text', 'makan pagi ama kamu &quot;lili&quot;', NULL, NULL, NULL, NULL, 0, 0, '2016-02-22 10:09:48', '2016-02-22 10:09:48', 0),
(18, 17, 'location', 'ini dikontrakan', NULL, '-6.235567', '106.8419103', 'Gg. Langgar No.33-34, Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12870, Indonesia', 0, 0, '2016-02-22 10:38:01', '2016-02-22 10:38:01', 0),
(19, 17, 'location', '', NULL, '-6.836864599999999', '107.05107829999997', 'Rasamala, Warungkondang, Kabupaten Cianjur, Jawa Barat 43261, Indonesia', 0, 0, '2016-02-22 23:26:15', '2016-02-22 23:26:15', 0),
(20, 18, 'text', 'ryan asem', NULL, NULL, NULL, NULL, 0, 0, '2016-02-23 04:09:35', '2016-02-23 04:09:35', 0),
(21, 18, 'location', '', NULL, '-6.181323000000001', '106.81532659999999', 'Jl. Jati Baru No.20, Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10160, Indonesia', 0, 0, '2016-02-23 04:10:37', '2016-02-23 04:10:37', 0),
(22, 21, 'text', 'ngantuk banget', NULL, NULL, NULL, NULL, 0, 0, '2016-02-23 04:51:07', '2016-02-23 04:51:07', 0),
(23, 21, 'location', 'ngantor dot com', NULL, '-6.1809354999999995', '106.8151084', 'Jl. Jati Baru No.28, Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10160, Indonesia', 0, 0, '2016-02-23 04:51:24', '2016-02-23 04:51:24', 0),
(24, 21, 'photo', 'isyana ahahaha', '2879420160223post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-23 05:12:04', '2016-02-23 05:12:04', 0),
(25, 21, 'location', 'jalan jalan', NULL, '-6.181515', '106.8154002', 'Jl. Jati Baru No.5-7, Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10160, Indonesia', 0, 0, '2016-02-23 05:17:31', '2016-02-23 05:17:31', 0),
(26, 21, 'photo', 'ahok ahok', '6242320160223post.png', NULL, NULL, NULL, 0, 0, '2016-02-23 05:29:53', '2016-02-23 05:29:53', 0),
(27, 21, 'text', 'asdasd', NULL, NULL, NULL, NULL, 0, 0, '2016-02-23 08:13:11', '2016-02-23 08:13:11', 0),
(28, 22, 'photo', 'isyana maneh', '2201320160223post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-23 08:19:59', '2016-02-23 08:19:59', 0),
(29, 24, 'photo', 'update ye', '4821420160223post.jpg', NULL, NULL, NULL, 0, 0, '2016-02-23 10:24:25', '2016-02-23 10:24:25', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_privacy`
--

CREATE TABLE IF NOT EXISTS `ffi_privacy` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `ffi_privacy`
--

INSERT INTO `ffi_privacy` (`id`, `title`, `text`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Privacy Page', '&lt;p&gt;asdasd&lt;/p&gt;', '2016-02-19 12:03:23', '2016-02-19 05:03:23', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_roles`
--

CREATE TABLE IF NOT EXISTS `ffi_roles` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_role_user`
--

CREATE TABLE IF NOT EXISTS `ffi_role_user` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_tnc`
--

CREATE TABLE IF NOT EXISTS `ffi_tnc` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `ffi_tnc`
--

INSERT INTO `ffi_tnc` (`id`, `title`, `text`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Term & condition Page', '&lt;p&gt;asdasd&lt;/p&gt;', '2016-02-19 11:57:01', '2016-02-19 04:57:01', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_users`
--

CREATE TABLE IF NOT EXISTS `ffi_users` (
`id` int(11) NOT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `provider_avatar` varchar(255) DEFAULT NULL,
  `provider_access_token` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `spouse` varchar(255) DEFAULT NULL,
  `child` varchar(255) DEFAULT NULL,
  `shop` text,
  `image` varchar(255) DEFAULT NULL,
  `friendlist` text,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `login_enabled` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data untuk tabel `ffi_users`
--

INSERT INTO `ffi_users` (`id`, `provider`, `provider_avatar`, `provider_access_token`, `username`, `password`, `name`, `email`, `phone`, `birthdate`, `gender`, `spouse`, `child`, `shop`, `image`, `friendlist`, `last_login`, `login_enabled`, `deleted_at`, `created_at`, `updated_at`, `remember_token`, `is_deleted`) VALUES
(1, NULL, NULL, NULL, 'administrator', '$2y$10$gfgATfOso0lPbLQRR6If6.AJf1OcsEYsuP2j3qm2/6IoYqBcsID/C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-02-18 19:24:07', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-02-18 12:24:07', 'hRAgte83bHPVoQC6dlot8dtCfhR5RPsfJEmXA5s0XG0Ky1E74z4zidbfrIUf', 1),
(2, NULL, NULL, NULL, 'luluq', 'luluq', 'luluq miftakhul hudas', 'luluq.ye@gmail.com', '08563531001', NULL, NULL, '', '', NULL, NULL, NULL, '2016-02-16 20:04:12', 1, '0000-00-00 00:00:00', '2016-02-16 11:20:15', '2016-02-16 13:04:12', 'CruboKGE8GS3BT04WpWeujNbHk44fwHwSn0d5NbIjbpK5dKwK2tH9LyO1qlp', 1),
(3, NULL, NULL, NULL, 'admin', '$2y$10$mIuYCRoSJO3qqmYWfjs7WO/QmeOAh1Otqc3OiM8KrJf1l3F88OJBy', 'admin@ffi.com', 'admin@ffi.com', '08563531001', NULL, NULL, '', '', NULL, '/images/upload/photo/2786720160216photo.png', NULL, '2016-02-21 21:43:30', 0, '0000-00-00 00:00:00', '2016-02-16 11:27:27', '2016-02-21 14:43:30', 'L1oFXukr8n2f5zaB3wifJW7ydZtSUiwQCU4pPO31iq3LXfvrHRLGSYvjHaWB', 0),
(21, '1019597078112297', '/images/upload/photo/provider/8851420160223provider.jpg', NULL, 'Luluq Miftakhul Huda', NULL, 'Luluq Huda', 'luluq.ye@gmail.com', NULL, '0000-00-00 00:00:00', '1019597078', 'lisa han vimora', 'lili,lulu', NULL, '/images/upload/photo/provider/8851420160223provider.jpg', '[{"name":"Deden Sembada","id":"1125263274152800"}]', '2016-02-23 15:41:44', 0, '0000-00-00 00:00:00', '2016-02-23 04:38:33', '2016-02-23 08:41:44', NULL, 0),
(24, '1125263274152800', '/images/upload/photo/provider/4785420160223provider.jpg', NULL, 'Deden Sembada', NULL, 'Deden Sembada', 'dnsluver@gmail.com', '08563531001', '0000-00-00 00:00:00', '1125263274', 'tiaka', 'tara', 'Alfamart', '/images/upload/photo/provider/4785420160223provider.jpg', '[{"name":"Luluq Miftakhul Huda","id":"1019597078112297"}]', '2016-02-23 21:32:11', 0, '0000-00-00 00:00:00', '2016-02-23 10:23:48', '2016-02-23 14:32:11', NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_winners`
--

CREATE TABLE IF NOT EXISTS `ffi_winners` (
`winner_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `winner_category_id` int(11) DEFAULT NULL,
  `ket_date` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `ffi_winners`
--

INSERT INTO `ffi_winners` (`winner_id`, `user_id`, `winner_category_id`, `ket_date`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 1, 2, NULL, '2016-02-19 09:18:59', '2016-02-19 02:18:59', 1),
(2, 3, 2, NULL, '2016-02-23 23:45:02', '2016-02-23 16:45:02', 1),
(3, 21, 2, NULL, '2016-02-23 23:50:42', '2016-02-23 16:50:42', 1),
(4, 24, 2, NULL, '2016-02-23 23:53:29', '2016-02-23 16:53:29', 1),
(5, 21, 2, NULL, '2016-02-23 23:53:27', '2016-02-23 16:53:27', 1),
(6, 21, 2, '12 Maret 2016', '2016-02-23 16:54:40', '2016-02-23 16:54:40', 0),
(7, 24, 2, '13 Maret 2016', '2016-02-23 16:56:10', '2016-02-23 16:56:10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ffi_winner_category`
--

CREATE TABLE IF NOT EXISTS `ffi_winner_category` (
`winner_category_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `ffi_winner_category`
--

INSERT INTO `ffi_winner_category` (`winner_category_id`, `name`, `created_at`, `updated_at`, `deleted_at`, `is_active`, `is_deleted`) VALUES
(2, 'Pemenang Utama', '2016-02-23 23:26:24', '2016-02-23 16:26:24', '0000-00-00 00:00:00', 0, 0),
(3, 'Pemenang Mingguan', '2016-02-23 15:17:39', '2016-02-23 15:17:39', '0000-00-00 00:00:00', 0, 0),
(4, 'Pemenang Harian', '2016-02-23 23:26:15', '2016-02-23 16:26:15', '0000-00-00 00:00:00', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ffi_about`
--
ALTER TABLE `ffi_about`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ffi_blog`
--
ALTER TABLE `ffi_blog`
 ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `ffi_config`
--
ALTER TABLE `ffi_config`
 ADD PRIMARY KEY (`key_config`);

--
-- Indexes for table `ffi_howto`
--
ALTER TABLE `ffi_howto`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ffi_inbox`
--
ALTER TABLE `ffi_inbox`
 ADD PRIMARY KEY (`inbox_id`);

--
-- Indexes for table `ffi_post`
--
ALTER TABLE `ffi_post`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ffi_privacy`
--
ALTER TABLE `ffi_privacy`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ffi_roles`
--
ALTER TABLE `ffi_roles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ffi_tnc`
--
ALTER TABLE `ffi_tnc`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ffi_users`
--
ALTER TABLE `ffi_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ffi_winners`
--
ALTER TABLE `ffi_winners`
 ADD PRIMARY KEY (`winner_id`);

--
-- Indexes for table `ffi_winner_category`
--
ALTER TABLE `ffi_winner_category`
 ADD PRIMARY KEY (`winner_category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ffi_about`
--
ALTER TABLE `ffi_about`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ffi_blog`
--
ALTER TABLE `ffi_blog`
MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ffi_howto`
--
ALTER TABLE `ffi_howto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ffi_inbox`
--
ALTER TABLE `ffi_inbox`
MODIFY `inbox_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ffi_post`
--
ALTER TABLE `ffi_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `ffi_privacy`
--
ALTER TABLE `ffi_privacy`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ffi_roles`
--
ALTER TABLE `ffi_roles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ffi_tnc`
--
ALTER TABLE `ffi_tnc`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ffi_users`
--
ALTER TABLE `ffi_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ffi_winners`
--
ALTER TABLE `ffi_winners`
MODIFY `winner_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ffi_winner_category`
--
ALTER TABLE `ffi_winner_category`
MODIFY `winner_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
